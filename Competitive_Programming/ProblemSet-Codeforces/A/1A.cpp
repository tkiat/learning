#include <iostream>

//Result: 31 ms	3400 KB
int main(){

    int n, m, a;
    std::cin >> n >> m >> a;

    int numRow = (n + a -1)/a;
    int numCol = (m + a -1)/a;
    std::cout << numRow * (long long) numCol;

    return 0;
}
