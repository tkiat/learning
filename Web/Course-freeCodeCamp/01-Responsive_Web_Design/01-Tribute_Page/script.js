const timeline_story = {
  1867: "Scrooge was born in Glasgow, Scotland to Fergus McDuck and Downy O'Drake. He would have two younger sisters, namely Matilda McDuck (born in 1871) and Hortense McDuck (born in 1876).",
  1877: "Scrooge, age 10, goes into business with a shoeshine kit that Fergus built for him, but his first customer fools him and pays him with an American dime (which was actually a plan of his father's). He keeps the dime he cannot spend as his symbol of success.",
  1880: "Scrooge, age 13, emigrates to the United States. He first meets his uncle, Mississippi riverboater Angus \"Pothole\" McDuck, and the Beagle Boys, a family of outlaws that remain his enemies for the rest of his life.,",
  1882: "His uncle retires and leaves his riverboat, named Dilly Dollar, to Scrooge, now 15 years old. The Beagle Boys destroy the riverboat in an act of revenge. Scrooge decides to try his luck in the American West and later in the year gets hired as a cowboy by cattle baron Murdo MacKenzie (an actual historical figure, one of the many that Scrooge met).",
  1883: "Scrooge, age 16, becomes a miner searching for silver and copper.",
  1885: "Scrooge's father calls his son back to Scotland on an important family matter. Just a week before he leaves he meets and befriends the millionaire Howard Rockerduck, who had become rich in the California gold rush of 1849. He also meets Howard's seven-year-old spoiled son John D. Rockerduck, who will grow up to become one of Scrooge's main rivals.",
  "1886–1889": "Scrooge, age 19–22, searches for gold in South Africa. During his first year there he saves the life of a duck about his age named Flintheart Glomgold, though Scrooge learns his name more than half a century later. A little later they become bitter enemies, and remain as such for the rest of their lives. Glomgold later became the second richest duck in the world.",
  "1889–1893": "Scrooge, age 22–26, returns to the United States to search for gold, most notably near Pizen Bluff, Arizona. He meets many famous historical figures but his search fails.",
  "1893–1896": "Scrooge, age 26–29, goes to Australia to search for gold but his search again fails.",
  "1896–1899": "Scrooge, age 29–32, searches for gold in the Klondike. During his years there he meets the saloon owner, singer and occasional thief \"Glittering\" Goldie O'Gilt. He carries a love/hate relationship with her for the rest of his life. His search for gold succeeds.",
  1897: "Scrooge's mother Downy O'Drake dies, aged 57, in Dismal Downs. Scrooge is now 30.",
  "1899–1902": "Scrooge, at 32, becomes a millionaire and buys a bank in Whitehorse, Yukon. He starts building a small financial empire; by 1902, at 35, he has become a billionaire.",
  1902: "Scrooge, age 35, returns to Scotland intending to make Castle McDuck at Dismal Downs the heart of his empire, but he soon finds local culture too stagnant and decides to return to America. His sisters Matilda McDuck and Hortense McDuck go to America with him. Also in 1902, Scrooge's father Fergus McDuck dies, aged 67, in Dismal Downs. Scrooge, Matilda and Hortense are the last of the McDuck clan. Scrooge settles in the small village of Duckburg, Calisota, United States, which he chose as his home base.",
  "1909–1930": "While his sisters remain in Duckburg and run his empire, Scrooge, age 42–63, travels the world expanding his empire in every continent.",
  1930: "Scrooge, at 63, becomes the richest duck in the world, but a fight with his family leaves him with no contact with them for the next seventeen years. Note that during this year he met his ten-year-old nephew Donald Duck and his nephew's twin sister Della Duck for the first time. (It was possibly the only time Scrooge met his niece Della, ignoring the girl completely.)",
  1942: "Scrooge, age 75, feels depressed and tired and decides to retire.",
  1947: "Scrooge, age 80, meets his nephew Donald Duck, now age 27, as well as his grandnephews, Della's children, Huey, Dewey, and Louie Duck. He decides to become active again and soon a circle of activities whirl around him as he attracts the attention of relatives, old and new enemies and friends.",
  1955: "Scrooge, age 88, is reunited and reconciled with his sister Matilda, thanks to Donald (In the story \"A Letter From Home\").",
  1967: "According to Don Rosa's unofficial timeline, Scrooge McDuck died at the age of 100 years after a life of adventure."
}

window.onload = function (){
  console.clear();
  var timeline = document.getElementById("timeline");
  for(const [year, description] of Object.entries(timeline_story)){
    let year_elem = document.createElement("strong");
    year_elem.innerHTML = year + ":";
    
    let item = document.createElement("li");
    item.className = "timeline_item"
    item.appendChild(year_elem);
    item.innerHTML += " " + description;
    timeline.appendChild(item);
  }
}