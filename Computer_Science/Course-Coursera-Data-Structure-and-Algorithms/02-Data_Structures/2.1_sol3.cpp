#include <iostream>
#include <queue>
#include <vector>
#include <deque>

//Result: Max time used: 0.32/2.00, max memory used: 13533184/536870912
struct Request {
    Request(int arrival_time, int process_time):
        arrival_time(arrival_time),
        process_time(process_time)
    {}

    int arrival_time;
    int process_time;
};

struct Response {
    Response(bool dropped, int start_time):
        dropped(dropped),
        start_time(start_time)
    {}

    bool dropped;
    int start_time;
};

class Buffer {
public:
    Buffer(int size):
        size_(size),
        finish_time_()
    {}

    std::deque<int> deq; //this stores finish time of each packet

    Response Process(const Request &request) {
        int i = 0;
        bool dropped;
        int startTime, finishTime; //for current coming request

        Response result(true, -1);
        
        if(deq.size() > 0){
            //if newcoming pkt has to wait, start when all packets finished
            //otherwise start at it own arrival time
            startTime = std::max(deq.back(), request.arrival_time);
            //deleted all past data (< current time = request.arrival_time)
            while(deq.size() > 0 && request.arrival_time >= deq.front())
                deq.pop_front();
        }
        else{
            startTime = request.arrival_time;
        }
        if(deq.size() == size_){
            //drop packet if after removing past data buffer is still full
            return result;
        }
        finishTime = startTime + request.process_time;
        deq.push_back(finishTime);
        result.dropped = false;
        result.start_time = startTime; 

        return result;
    }
private:
    int size_;
    std::queue <int> finish_time_;
};

std::vector <Request> ReadRequests() {
    std::vector <Request> requests;
    int count;
    std::cin >> count;
    for (int i = 0; i < count; ++i) {
        int arrival_time, process_time;
        std::cin >> arrival_time >> process_time;
        requests.push_back(Request(arrival_time, process_time));
    }
    return requests;
}

std::vector <Response> ProcessRequests(const std::vector <Request> &requests, Buffer *buffer) {
    std::vector <Response> responses;
    for (int i = 0; i < requests.size(); ++i)
        responses.push_back(buffer->Process(requests[i]));
    return responses;
}

void PrintResponses(const std::vector <Response> &responses) {
    for (int i = 0; i < responses.size(); ++i)
        std::cout << (responses[i].dropped ? -1 : responses[i].start_time) << std::endl;
}

int main() {
    int size;
    std::cin >> size;
    std::vector <Request> requests = ReadRequests();

    Buffer buffer(size);
    std::vector <Response> responses = ProcessRequests(requests, &buffer);

    PrintResponses(responses);
    return 0;
}
