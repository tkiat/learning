//Ans: 2407905288

#include <iostream>
#include <vector>
#include <fstream>
#include <string>

using std::string;
using std::vector;

long long Merge(vector<int> &arr, int left, int  mid, int right){
  int temp[right-left+1];//resultant merged subarray

  int i = left; //1st index of left subarray [left ... mid]
  int j = mid + 1; //1st index of right subarray [mid+1 ... right]
  int k = 0; //1st index of resultant merged subarray

  long long inv_count = 0;
  while(i <= mid && j <=right){
    if(arr[i] <= arr[j]){
      temp[k++] = arr[i++];
    }
    else{
      temp[k++] = arr[j++];
      //ex. [1 3 5] and [2 4 6] when 2 is copied to temp 
      //the split inversions are (3,2) and (5,2)
      inv_count += (mid - i) + 1;
    }
  }
  //Copy the remaining elements of left subarray (if any)
  while(i <= mid)
      temp[k++] = arr[i++];
  //Copy the remaining elements of right subarray (if any)
  while(j <= right)
      temp[k++] = arr[j++];
  //copy the merged array to original array
  for(k = 0, i = left; i <= right; ++i, ++k)
      arr[i] = temp[k];

  return inv_count;
}

long long MergeSort(vector<int> &arr, int left, int right){
  int mid;
  long long inv_count = 0;
  if(left < right){
    mid = (left + right) >> 1;
    inv_count += MergeSort(arr, left, mid);
    inv_count += MergeSort(arr, mid+1, right);
    inv_count += Merge(arr, left, mid, right);
  }
  return inv_count;
}
 
int main() {

  vector<int> arr;
  std::ifstream myfile ("4_RefFile.txt");

  int n = 0;
	if (myfile.is_open()){
    string line;
		while (getline(myfile, line)){
			arr.push_back(stoi(line));
		}
		myfile.close();
	}
	else {
		std::cout << "Unable to open file\n";
	}

/*
  std::cout << "Before mergeSort: ";
  for(auto it = arr.begin(); it != arr.end(); it++){
    std::cout << *it << " ";
  }
  std::cout << std::endl;
*/
  std::cout << MergeSort(arr, 0, arr.size()-1);
/*
  std::cout << "\nAfter mergeSort: ";
  for(auto it = arr.begin(); it != arr.end(); it++){
    std::cout << *it << " ";
  }
  std::cout << std::endl;
*/
  return (0);
}