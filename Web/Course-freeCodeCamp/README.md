## https://learn.freecodecamp.org

### This repository includes 20 projects + 5 Javascript exercises for 5 freeCodeCamp certifications

### This was the first time I learned about web development. 

- 01 Responsive Web Design (completed on November 4, 2018)
- 02 Algorithms and Data Structures (November 10, 2018)
- 03 Front End Libraries (July 23, 2019)
- 04 Data Visualization (August 5, 2019)
- 05 APIs and Microservices (July 7, 2019)
<br />
[Demo 01-04](https://codepen.io/collection/DOxqzM)
<br />
[Demo 05](https://glitch.com/@tkiatd/free-code-camp-ap-is-and-microservices)
