#include <iostream>
#include <string>

//Result: 280 ms 3444 KB
//note: it should work for any hard-coded values
int main(){

    //hard-coded part
    int taxi_size = 4;
    int num_size = 4; //number of group sizes available
    int group_size[num_size] = {1, 2, 3, 4};
    //receive input
    int s[num_size] = {0}; //s[i] = # of group_size[i]-person groups
    int n;
    std::cin >> n;
    while(n-- > 0){
        int i;
        std::cin >> i;
        s[i - 1]++;
    }
    //compute result
    int result = 0;
    while(s[0] > 0 || s[1] > 0 || s[2] > 0 || s[3] > 0){

        int temp = taxi_size;
        for(int i = 3; i >= 0; i--){
            while(s[i] > 0 && temp >= group_size[i]){
                temp -= group_size[i];
                s[i]--;
            }
        }
        result++;
    }
    std::cout << result;

    return 0;
}