console.clear();

document.addEventListener('DOMContentLoaded', function(){
  let req = new XMLHttpRequest();
  req.open("GET", 'https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/cyclist-data.json', true);
  req.send();
  req.onload = function(){
    var dataset = JSON.parse(req.responseText);
    //d3
    d3.select("body")
      .append("h1")
      .text("Doping Doping Doping!")
      .attr("id", "title");
    
    const minYear = d3.min(dataset, d => d.Year);
    const maxYear = d3.max(dataset, d => d.Year);
    const minSeconds = d3.min(dataset, d => d.Seconds);
    const maxSeconds = d3.max(dataset, d => d.Seconds);

    const date = dataset.map(d => new Date(1970, 1, 1, 1, d.Time.split(":")[0], d.Time.split(":")[1]))
    
    const minDate = d3.min(date);
    const maxDate = d3.max(date);
    
    var date_to_mm_ss = d3.timeFormat("%M:%S");
    
    const w = 600, h = 500, padding = 40;
    const xScale = d3.scaleLinear().domain([minYear, maxYear])
                                   .range([padding, w - padding]);
    const yScale = d3.scaleTime().domain([maxDate, minDate])
                                   .range([h - padding, padding]);
    
    const svg = d3.select("body")
                  .append("svg")
                  .attr("width", w)
                  .attr("height", h);
    
    svg.selectAll("circle")
       .data(dataset)
       .enter()
       .append("circle") 
       .attr("cx", (d, i) => xScale(d.Year))
       .attr("cy", (d, i) => yScale(date[i]))
       .attr("r", 5)
       .attr("class", "dot")
       .style("fill", function(d){ return d.Year < 2000 ? "blue" : "green" })
       .attr("data-xvalue", (d, i) => d.Year)
       .attr("data-yvalue", (d, i) => date[i])

    const xAxis = d3.axisBottom(xScale).tickFormat(d3.format("d"));
    const yAxis = d3.axisLeft().scale(yScale).tickFormat((d, i) => date_to_mm_ss(d));
    
    //axis
    svg.append("g")
       .attr("id", "x-axis")
       .attr("transform", "translate\(0," + (h - padding) + "\)")
       .call(xAxis);
    svg.append("g")
       .attr("id", "y-axis")
       .attr("transform", "translate\(" + padding + ", 0\)")
       .call(yAxis);
    
    //legend
    svg.append("g")
       .attr("id", "legend")
       .attr("height", "300")
       .attr("width", "450");

    var legend = d3.select("#legend");
    
    legend.append("rect")
       .attr("x", w - 200)
       .attr("y", 50)
       .attr("width", 20)
       .attr("height", 20)
       .style("fill", "blue");
    legend.append("text")
       .attr("x", w - 170)
       .attr("y", 70)
       .text("Before 2000")
       .style("font-size", "15px")
       .attr("alignment-baseline","middle");
    
    legend.append("rect")
       .attr("x", w - 200)
       .attr("y", 80)
       .attr("width", 20)
       .attr("height", 20)
       .style("fill", "green");
    legend.append("text")
       .attr("x", w - 170)
       .attr("y", 100)
       .text("After 2000")
       .style("font-size", "15px")
       .attr("alignment-baseline","middle");
    
    legend.append("text")
       .attr("x", w - 200)
       .attr("y", 130)
       .text("(Sorry if that is obvious!)")
       .style("font-size", "15px")
       .attr("alignment-baseline","middle");

    //tooltip
    var tooltip = d3.select("body")
                    .append("div")
                    .attr("id", "tooltip")
                    .style("position", "absolute")
                    .style("visibility", "hidden");
    
    d3.selectAll("circle")
      .on("mouseover", function(d){
        tooltip.text(d.Name + " [" + d.Nationality +"]")
               .attr("data-year", d.Year)
               .style("visibility", "visible")
               .style("top", (event.pageY - 30) + "px")
               .style("left", event.pageX + "px");
               
      })
      .on("mouseout", function(){
        return tooltip.style("visibility", "hidden");
      });
  }
});
