{ pkgs ? import <nixpkgs> { } }:

let
  myGhc = pkgs.haskellPackages.ghcWithPackages (hpkgs: with hpkgs; [
  ]);
in
pkgs.mkShell {
  buildInputs = [
    myGhc
    pkgs.haskellPackages.haskell-language-server
    pkgs.haskellPackages.ghcid
    pkgs.haskellPackages.ormolu
    pkgs.haskellPackages.hlint
    pkgs.nixpkgs-fmt
  ];
}
