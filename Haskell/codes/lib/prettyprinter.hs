import Data.Text.Prettyprint.Doc

main = do
  let prettyType = align . sep . zipWith (<+>) ("::" : repeat "->")
  let prettyDecl n tys = pretty n <+> prettyType tys
  let doc = prettyDecl "example" ["Int", "Bool", "Char", "IO ()"]
  putDocW 80 doc
