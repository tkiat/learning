-- 1: parameteric polymorphism

length :: [a] -> Int

-- 2: bounded polymorphism
-- source: https://stackoverflow.com/questions/12430660/creating-polymorphic-functions-in-haskell
-- action for each type is the same, unlike the one with type families

class FunnyShow a where
  funnyshow :: a -> String

instance FunnyShow Int where
  funnyshow i = show (i + 1)

instance FunnyShow [Char] where
  funnyshow s = show (show s)

-- with type families
-- source: https://stackoverflow.com/questions/12430660/creating-polymorphic-functions-in-haskell

-- {-# LANGUAGE FlexibleInstances #-}
-- {-# LANGUAGE TypeSynonymInstances #-}
-- {-# LANGUAGE TypeFamilies #-}
--
--
class F a where
  type Out a :: *
  f :: a -> Out a

instance F String where
  type Out String = String
  f = show . length

instance F Int where
  type Out Int = String
  f = show

instance F Float where
  type Out Float = Float
  f = id

-- > f (2::Int)
-- > f "Hello"
-- > f (2::Float)
