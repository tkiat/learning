-- src: cs240h 2016

module Ref
  ( Ref, -- export type ctor, but not value ctor
    newRef,
    readOnly,
    readRef,
    writeRef,
  )
where

import Data.IORef

-- use phantom types to create both mutable and non-mutable references
newtype Ref t a = Ref (IORef a)

data ReadOnly

data ReadWrite

-- create mutable reference
newRef :: a -> IO (Ref ReadWrite a)
newRef a = Ref `fmap` newIORef a

-- create read-write reference
readRef :: Ref t a -> IO a
readRef (Ref ref) = readIORef ref

writeRef :: Ref ReadWrite a -> a -> IO ()
writeRef (Ref ref) v = writeIORef ref v

-- create read-only reference
readOnly :: Ref t a -> Ref ReadOnly a
readOnly (Ref ref) = Ref ref
