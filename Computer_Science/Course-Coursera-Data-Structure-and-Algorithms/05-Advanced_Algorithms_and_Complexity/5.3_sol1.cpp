#include <ios>
#include <iostream>
#include <vector>

using std::vector;

//Result: Max time used: 0.00/1.00, max memory used: 8998912/1073741824
struct Edge {
  int from;
  int to;
};
struct ConvertGSMNetworkProblemToSat {

  int numVertices;
  vector<Edge> edges;

  ConvertGSMNetworkProblemToSat(int n, int m): numVertices(n), edges(m){}

  void exactly_one_of(int r, int g, int y){
    //only one of {r, g, y} is true
    std::cout << r << " " << g << " " << y << " 0\n";
    std::cout << -r << " " << -g << " 0\n";
    std::cout << -g << " " << -y << " 0\n";
    std::cout << -y << " " << -r << " 0\n";
  }

  void printEquisatisfiableSatFormula() {

    //output # clauses and # variables
    int numClauses = numVertices * 4 + edges.size() * 3;
    int numVariables = numVertices * 3;
    
    std::cout << numClauses<< " " << numVariables << "\n";
    //assign node u color red/green/yellow with number 3*(u-1) + 1/2/3
    for(int u = 0; u < numVertices; u++){

      int u_r = u * 3 + 1;    //node u color red
      int u_g = u * 3 + 2;
      int u_y = u * 3 + 3;
      //make sure that a node contains only one color
      exactly_one_of(u_r, u_g, u_y);
    }
    for(int i = 0; i < edges.size(); i++){

      int u_r = (edges[i].from - 1) * 3 + 1;
      int u_g = u_r + 1;
      int u_y = u_g + 1;

      int v_r = (edges[i].to - 1) * 3 + 1;
      int v_g = v_r + 1;
      int v_y = v_g + 1;
      //adjacent nodes cannot have same color
      std::cout << -u_r << " " << -v_r << " 0\n";
      std::cout << -u_g << " " << -v_g << " 0\n";
      std::cout << -u_y << " " << -v_y << " 0\n";
    }
  }
};

int main() {

  std::ios::sync_with_stdio(false);

  int n, m;
  std::cin >> n >> m;
  ConvertGSMNetworkProblemToSat converter(n, m);
  for (int i = 0; i < m; ++i)
    std::cin >> converter.edges[i].from >> converter.edges[i].to;
  converter.printEquisatisfiableSatFormula();

  return 0;
}
