#include <algorithm>
#include <sstream>
#include <iostream>
#include <vector>
#include <string>
#include <climits>
using std::vector;
using std::string;

//Result: Max time used: 0.01/1.00, max memory used: 9601024/536870912
bool IsGreaterOrEqual(int a, int b){

  string one = std::to_string(a) + std::to_string(b);
  string two = std::to_string(b) + std::to_string(a);
  return one > two;
}

string largest_number(vector<string> a) {

  string answer = "";
  while(a.size() > 0){
    int maxDigit = -1;
   
    for (size_t i = 0; i < a.size(); i++) {
      if(IsGreaterOrEqual(stoi(a[i]), maxDigit)){//stoi(a[i]) >= maxDigit
        maxDigit = stoi(a[i]);
      }
    }
    answer.append(std::to_string(maxDigit));
    
    vector<string>::iterator pos;
    pos = std::find(std::begin(a), std::end(a), std::to_string(maxDigit));
    a.erase(pos);
  }
  return answer;
}

int main() {
  //stressTest();
  
  int n;
  std::cin >> n;
  vector<string> a(n);
  for (size_t i = 0; i < a.size(); i++) {
    std::cin >> a[i];
  }
  std::cout << largest_number(a);
  
  getchar();
  return 0;
}
