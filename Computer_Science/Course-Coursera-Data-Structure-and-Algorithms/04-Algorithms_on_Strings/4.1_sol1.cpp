#include <string>
#include <iostream>
#include <vector>
#include <map>

using std::map;
using std::vector;
using std::string;

typedef map<char, int> edges;
typedef vector<edges> Trie;

//Max time used: 0.01/0.50, max memory used: 67588096/536870912
//----------------------------------------
Trie build_trie(vector<string> &patterns) {

  Trie trie;
  
  int ind = 0;
  for(int i = 0; i < patterns.size(); i++){
    int temp = 0;
    int offset = 0;
    
    //step 1: move from the root in trie "offset" times until we find non-existing char in pattern
    for(char c : patterns[i]){

      while(temp + 1 > trie.size()) //prevent segmentation fault
        trie.push_back(map<char, int>());

      if(trie[temp].find(c) != trie[temp].end()){ //if found
        temp = trie[temp][c];
        offset++;
      }
      else
        break;
    }
    //step 2: neglect first "offset" char in pattern[i] and insert the rest
    for(int j = offset; j < patterns[i].length(); j++){
    
      while(temp + 1 > trie.size())
        trie.push_back(map<char, int>());
      
      trie[temp].insert(std::pair<char,int>(patterns[i][j], ++ind));
      temp = ind;
    }
    /*
    //delimiter of each trie (optional)
    while(temp + 1 > trie.size())
        trie.push_back(map<char, int>());
    trie[temp].insert(std::pair<char,int>('$', ++ind));
    */
  }
  return trie;
}
//----------------------------------------
int main() {
  size_t n;
  std::cin >> n;
  vector<string> patterns;
  for (size_t i = 0; i < n; i++) {
    string s;
    std::cin >> s;
    patterns.push_back(s);
  }

  Trie trie = build_trie(patterns);
  for (size_t i = 0; i < trie.size(); ++i){
    for (const auto & j : trie[i])
      std::cout << i << "->" << j.second << ":" << j.first << "\n";
  }
  return 0;
}