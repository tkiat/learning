#include <iostream>

//Result: Max time used: 0.09/1.00, max memory used: 9891840/536870912
int gcd_naive(int a, int b) {
  int current_gcd = 1;
  for (int d = 2; d <= a && d <= b; d++) {
    if (a % d == 0 && b % d == 0) {
      if (d > current_gcd) {
        current_gcd = d;
      }
    }
  }
  return current_gcd;
}
//O(log(ab))
int EuclidGCD(int a, int b){
  if (b==0)
    return a;
  a = a % b;
  return EuclidGCD(b, a);
}

int main() {
  int a, b;
  std::cin >> a >> b;
  //std::cout << gcd_naive(a, b) << std::endl;
  std::cout << EuclidGCD(a, b) << std::endl;
  return 0;
}
