let
  hostNix = import <nixpkgs> {};
  pinnedPkgsMetadata = hostNix.pkgs.lib.importJSON ./nixos-19-09.json;

  pinnedPkgs = hostNix.pkgs.fetchFromGitHub {
    owner = "NixOS";
    repo  = "nixpkgs-channels";
    inherit (pinnedPkgsMetadata) rev sha256;
  };
in
  pinnedPkgs
