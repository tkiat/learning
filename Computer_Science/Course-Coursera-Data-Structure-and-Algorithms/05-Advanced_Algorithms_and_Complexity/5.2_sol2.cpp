#include <algorithm>
#include <iostream>
#include <vector>
#include <cstdio>
#include <cmath>
#include <climits>

using std::vector;

typedef std::vector<double> Column;
typedef std::vector<double> Row;
typedef std::vector<Row> Matrix;
//Result: Max time used: 0.07/2.00, max memory used: 9916416/536870912
//----------------------------------------
struct Equation {
  Matrix a;
  Column b;
  Equation(const Matrix &a, const Column &b): a(a), b(b){}
};
struct Position {
  int column;
  int row;
  Position(int column, int row): column(column), row(row){}
};
//----------------------------------------
Column SolveEquation(Equation equation);
unsigned int num1Bits(unsigned int n);
//Goal: Maximize sum(x_i * c_i) from inequalites Ax <= b
std::pair<int, vector<double>> solve_diet_problem(
  int n, 
  int m, 
  Matrix A, 
  vector<double> b, 
  vector<double> c){
  //Step 1: add constraints to A and b
    //constraint x_i >= 0
  for(int i = 0; i < m; i++){
    vector<double> temp(m);
    temp[i] = -1;
    A.push_back(temp);
    b.push_back(0);
  }
    //constraint sum(x_i * c_i) <= 10^9
  b.push_back(1000000000);
  vector<double> temp(m);
  std::fill(temp.begin(), temp.end(), 1);
  A.push_back(temp);
  //Step 2: select each subset of m equations
  vector<int> permutation;
  for(int i = 0; i < pow(2, A.size()); i++){
    if(num1Bits(i) == m)
      permutation.push_back(i);
  }
  //Step 3: for each subset, change inequalities to equalities
  //and perform Gaussian Elimination 
  //select solution that maximizes sum(x_i * c_i)
  vector<double> ans;
  double max = std::numeric_limits<double>::lowest();
  for(int i = 0; i < permutation.size(); i++){

    //Select subset of size m
    int temp = permutation[i];
    int ind = 0;

    Matrix A_sub;
    Column b_sub;
    
    while(temp > 0){
      if(temp % 2 == 1){
        A_sub.push_back(A[ind]);
        b_sub.push_back(b[ind]);
      }
      ind++;
      temp >>= 1;
    }
    //Perform Gaussian elimination of this subset
    Equation eq(A_sub, b_sub);
    Column ans_i = SolveEquation(eq);
    //Check if solution is valid
      //check if no solution exists
    if(ans_i.size() == 0)
      continue;
      //check if satisfy all inequalities
    bool flag = false;
    for(int j = 0; j < A.size(); j++){
      double sum = 0;
      for(int k = 0; k < ans_i.size(); k++)
        sum += A[j][k] * ans_i[k];
      if(sum > b[j] + 0.001){
        flag = true;
        break;
      }
    }
    if(flag)
      continue;
    //Select solutions of each subset equations that maximize sum(x_i * c_i)
    double max_i = 0;
    for(int j = 0; j < ans_i.size(); j++)
      max_i += c[j] * ans_i[j];
    if(max_i > max){
      max = max_i;
      ans = ans_i;
    }
  }
  //Step 4: Check result
    //if sum(ans_i * c_i) = 10^9, output "Infinity"
    //because from step 3 sum(x_i * c_i) is maximized
    //this means these two have the same growth trend
  double sum = 0;
    for(int i = 0; i < ans.size(); i++)
      sum += ans[i];
  if(fabs(sum-1000000000) < 0.0001)
    return {1, vector<double>(m, 0)};
    //if no answer at all, output "No solution"
  if(ans.size() == 0)
    return {-1, vector<double>(m, 0)};
    //otherwise output "Bounded solution" and corresponding values
  else
    return {0, ans};
}
//----------------------------------------
int main(){
  int n, m;
  std::cin >> n >> m;
  Matrix A(n, vector<double>(m));
  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++)
      std::cin >> A[i][j];

  vector<double> b(n);
  for (int i = 0; i < n; i++)
    std::cin >> b[i];

  vector<double> c(m);
  for (int i = 0; i < m; i++)
    std::cin >> c[i];

  std::pair<int, vector<double>> ans = solve_diet_problem(n, m, A, b, c);

  switch (ans.first) {
    case -1: 
      printf("No solution\n");
      break;
    case 0: 
      printf("Bounded solution\n");
      for (int i = 0; i < m; i++)
        printf("%.18f%c", ans.second[i], " \n"[i + 1 == m]);
      break;
    case 1:
      printf("Infinity\n");
      break;      
  }
  return 0;
}
//----------------------------------------
//Gaussian Elimination
Position SelectPivotElement(
  const Matrix &a, 
  std::vector <bool> &used_rows, 
  std::vector <bool> &used_columns) {

  size_t size = a.size();
  Position pivot_element(0, 0);

  while (used_rows[pivot_element.row])
    pivot_element.row++;
  while (used_columns[pivot_element.column])
    pivot_element.column++;
  while(pivot_element.row < size - 1 && pivot_element.column < size 
      && fabs(a[pivot_element.row][pivot_element.column]) < 0.001)
    pivot_element.row++;
  while(pivot_element.row < size - 1 && pivot_element.column < size 
      && fabs(a[pivot_element.row][pivot_element.column]) < 0.001)
    pivot_element.column++;

  return pivot_element;
}
void SwapLines(Matrix &a, Column &b, std::vector <bool> &used_rows, Position &pivot_element) {
  std::swap(a[pivot_element.column], a[pivot_element.row]);
  std::swap(b[pivot_element.column], b[pivot_element.row]);
  std::swap(used_rows[pivot_element.column], used_rows[pivot_element.row]);
  pivot_element.row = pivot_element.column;
}
void ProcessPivotElement(Matrix &a, Column &b, const Position &pivot_element) {

  double pivot = a[pivot_element.row][pivot_element.column];

  for(int i = 0; i < a.size(); i++)
    a[pivot_element.row][i] /= pivot;
  b[pivot_element.row] /= pivot;
  for(int i = 0; i < a.size(); i++){
    if(i == pivot_element.row)
      continue;
    double val = a[i][pivot_element.column];
    if(val == 0)
      continue;
    for(int j = 0; j < a.size(); j++)
      a[i][j] -= val * a[pivot_element.row][j];
    b[i] -= val * b[pivot_element.column];
  }
}
void MarkPivotElementUsed(
  const Position &pivot_element, 
  std::vector <bool> &used_rows, 
  std::vector <bool> &used_columns) {

  used_rows[pivot_element.row] = true;
  used_columns[pivot_element.column] = true;
}
Column SolveEquation(Equation equation) {
  Matrix &a = equation.a;
  Column &b = equation.b;
  int size = a.size();

  std::vector <bool> used_columns(size, false);
  std::vector <bool> used_rows(size, false);

  for (int step = 0; step < size; ++step) {

    Position pivot_element = SelectPivotElement(a, used_rows, used_columns);
    //pivot is zero, no solution
    if(fabs(a[pivot_element.row][pivot_element.column]) < 0.001)
      return vector<double>(); 
    SwapLines(a, b, used_rows, pivot_element);
    ProcessPivotElement(a, b, pivot_element);
    MarkPivotElementUsed(pivot_element, used_rows, used_columns);
  }
  return b;
}
//output # of '1' bit in a number
unsigned int num1Bits(unsigned int n){

  unsigned int count = 0;
  while (n){
    count += n & 1;
    n >>= 1;
  }
  return count;
}
