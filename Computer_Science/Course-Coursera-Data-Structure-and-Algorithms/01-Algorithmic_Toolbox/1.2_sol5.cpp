#include <iostream>

//Result: Max time used: 0.02/1.00, max memory used: 9912320/536870912
int Fn_modm(long long n, long long m) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;

    for (long long i = 0; i < n - 1; i++) {
        long long tmp_previous = previous;
        previous = current;
        current = (tmp_previous + current) % m;
        //all Fn mod m is periodic starting with 01
        if(previous == 0 && current == 1){
            int period = i + 1;
            i += ((n - i -2)/period) * period;
        }
    }
    return current;
}

long long get_fibonacci_huge_naive(long long n, long long m) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;

    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
    }

    return current % m;
}

int main() {
    long long n, m;
    std::cin >> n >> m;
    //std::cout << get_fibonacci_huge_naive(n, m) << '\n';
    std::cout << Fn_modm(n, m) << '\n';
}