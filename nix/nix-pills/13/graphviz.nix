{ mkDerivation, gdSupport ? true, gd, fontconfig, libjpeg, bzip2 }:

mkDerivation {
  name = "graphviz";
  src = ./graphviz-2.48.0.tar.gz;
  buildInputs = if gdSupport then [ gd fontconfig libjpeg bzip2 ] else [ ];
}
# 
# let
#   pkgs = import <nixpkgs> { };
#   mkDerivation = import ./autotools.nix pkgs;
# in
# mkDerivation {
#   name = "graphviz";
#   src = ./graphviz-2.48.0.tar.gz;
#   buildInputs = with pkgs; [ gd fontconfig libjpeg bzip2 ];
# }
