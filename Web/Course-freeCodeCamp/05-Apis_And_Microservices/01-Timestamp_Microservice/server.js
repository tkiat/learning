//node app starts here
var express = require('express');
var app = express();
var mode = "";
// enable CORS so that your API is remotely testable by FCC 
var cors = require('cors');
app.use(cors({optionSuccessStatus: 200}));  // some legacy browsers choke on 204

app.use(express.static(__dirname + '/src'));

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/view/index.html");
});

app.get("/api/timestamp/:date_string?", function (req, res) {
  
  let date = new Date();
  let date_input = req.params.date_string;
  
  if(typeof date_input !== "undefined"){ 
    if(!isNaN(date_input)){
      date_input = +date_input;
    }
    date = new Date(date_input);
  }
  res.json({"unix": date.getTime(), "utc" : date.toUTCString()})
});

if(mode !== "test"){
  var listener = app.listen(process.env.PORT || 3000, function () {
    console.log('Your app is listening on port ' + listener.address().port);
  });
}

module.exports = app;
