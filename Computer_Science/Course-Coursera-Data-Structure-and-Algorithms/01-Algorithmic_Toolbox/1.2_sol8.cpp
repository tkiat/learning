#include <iostream>

//Result: Max time used: 0.00/1.00, max memory used: 9605120/536870912
int Fn_modm(long long n, long long m) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;

    for (long long i = 0; i < n - 1; i++) {
        long long tmp_previous = previous;
        previous = current;
        current = (tmp_previous + current) % m;
        //all Fn mod m is periodic starting with 01
        if(previous == 0 && current == 1){
            int period = i + 1;
            i += ((n - i -2)/period) * period;
        }
    }
    return current;
}
int fibonacci_sum_squares(long long n) {
//1. the sqaure sum of n 1 to 5 is the area of a rectangle with vertical side 
//𝐹5 = 5 and horizontal side 𝐹5 + 𝐹4 = 3 + 5 = 𝐹6
//2. (A * B) mod C = (A mod C * B mod C) mod C
    return Fn_modm(n, 10) * Fn_modm(n + 1, 10) % 10;
}

int fibonacci_sum_squares_naive(long long n) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;
    long long sum      = 1;

    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
        sum += current * current;
    }

    return sum % 10;
}

int main() {
    long long n = 0;
    std::cin >> n;
    std::cout << fibonacci_sum_squares(n);
}
