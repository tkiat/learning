#include <iostream>
#include <vector>
#include <cstdlib>

using std::vector;
using std::swap;

//Result: Max time used: 0.05/1.00, max memory used: 12275712/536870912
std::pair<int, int> partition3(vector<int> &a, int l, int r) { 

  //partition3 to handle equal elements
  int pivot = a[l];
  int m2 = l;
  int m1 = l;
//move i from l+1 to r maintaining invariants
//1. from m1 to m2 all elements = pivot
//2. left elements from m1 < pivot
//3. right elements from m2 > pivot
  for (int i = l + 1; i <= r; i++) {
    if (a[i] < pivot) {
      m2++;
      swap(a[i], a[m2]);
      swap(a[m1], a[m2]);
      m1++;
    }
    else if(a[i] == pivot){
      m2++;
      swap(a[i], a[m2]);
    }
  }
  return std::make_pair(m1, m2);
}

int partition2(vector<int> &a, int l, int r) {
  int pivot = a[l]; //pivot is initially at the most left position
  int j = l;
//move i from l+1 to r maintaining invariants
//1. left elements from j <= pivot
//2. right elements from j >= pivot
//position j will be swapped with pivot at the end
  for (int i = l + 1; i <= r; i++) {
    if (a[i] <= pivot) {
      j++;
      swap(a[i], a[j]);
    }
  }
//swap pivot a[l] with element at j
  swap(a[l], a[j]); 
  return j; //j = pivot position
}

void randomized_quick_sort(vector<int> &a, int l, int r) {
  if (l >= r) {
    return;
  }
//swap random pivot so that it is at most left position
  int k = l + rand() % (r - l + 1);
  swap(a[l], a[k]);

  int partition = 3;
  if(partition == 2){
  //after partition2 left m <= m and right m >= m, i.e a[m] is at final position
    int m = partition2(a, l, r); 
  //sort remaining parts other than a[m]
    randomized_quick_sort(a, l, m - 1);
    randomized_quick_sort(a, m + 1, r);
  }
  else if(partition == 3){
    std::pair<int,int> m = partition3(a, l, r);
    randomized_quick_sort(a, l, m.first - 1);
    randomized_quick_sort(a, m.second + 1, r);
  }
}

int main() {
  int n;
  std::cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); ++i) {
    std::cin >> a[i];
  }
  randomized_quick_sort(a, 0, a.size() - 1);
  for (size_t i = 0; i < a.size(); ++i) {
    std::cout << a[i] << ' ';
  }
}
