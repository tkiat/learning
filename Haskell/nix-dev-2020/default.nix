{ nixpkgs ? import ./nix/nixos-19-09.nix }:
let
  # shouldn't override the default haskellPackages directly, rather a copy of it
  overlay = self: super: {
    myHaskellPackages =
      super.haskell.packages.ghc865.override (old: {
        overrides = self.lib.composeExtensions (old.overrides or (_: _: {}))
          (hself: hsuper: {
            ghc = hsuper.ghc // { withPackages = hsuper.ghc.withHoogle; };
            ghcWithPackages = hself.ghc.withPackages;
            # add package from Hackage. A trick is to set sha256 to 52 zeros, then invoke nix-shell and when the build fails it should report the correct sha256.
#             req = hself.callHackageDirect
#               { pkg = "req";
#                 ver = "3.1.0";
#                 sha256 = "0000000000000000000000000000000000000000000000000000";
#               } {};
            # you can also use 'callHackage' which does not need a sha256 but it has to be in all-cabal-hashes
            # sometimes tests is slow so you may skip it
#             req = self.haskell.lib.dontCheck
#               (hself.callHackageDirect
#                 { pkg = "req";
#                   ver = "3.1.0";
#                   sha256 = "0rwbdmp2g74yq7fa77fywsgm0gaxcbpz9m6g57l5lmralg95cdxh";
#                 } {}
#               );
            # add local package
#             project = self.haskell.lib.dontCheck (hself.callCabal2nix "project" /full/path/to/project/folder {});
          });
      });
  };
  pkgs = import nixpkgs {
    overlays = [overlay];
  };

  # see #1: build from source directly, extra is missing library for ghcid
#   ghcidSrc = pkgs.fetchFromGitHub {
#     owner = "ndmitchell";
#     repo = "ghcid";
#     rev = "e3a65cd986805948687d9450717efe00ff01e3b5";
#     sha256 = "1xhyl7m3yaka552j8ipvmrbfixgb8x0i33k2166rkiv7kij0rhsz";
#   };
#   extra = pkgs.haskell.lib.dontCheck
#     (pkgs.haskellPackages.callHackageDirect
#       { pkg = "extra";
#         ver = "1.6.20";
#         sha256 = "0fy5bkn8fn3gcpxksffim912q0ifk057x0wmrvxpmjjpfi895jzj";
#       } {}
#     );

  # create derivation
  drv = pkgs.myHaskellPackages.callCabal2nix "name-of-project" ./name-of-project.cabal {};
  drvWithTools = drv.env.overrideAttrs (
    old: with pkgs.myHaskellPackages; {
      nativeBuildInputs = old.nativeBuildInputs ++ [
        ghcid
#         (pkgs.haskell.lib.justStaticExecutables (pkgs.haskellPackages.callHackage "ghcid" "0.8.2"))
        # #1
#         (pkgs.haskell.lib.justStaticExecutables
#           (pkgs.haskell.packages.ghc865.callCabal2nix "ghcid" ghcidSrc {inherit extra;}))

        # Add other development tools like ormolu here
      ];
      # shellHook e.g. 'source .config/secrets'
      shellHook = ''
      '';
    }
  );
in
  drvWithTools
