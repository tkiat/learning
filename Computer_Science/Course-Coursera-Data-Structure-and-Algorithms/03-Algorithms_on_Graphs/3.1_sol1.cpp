#include <iostream>
#include <vector>
#include <stack>

using std::vector;
using std::pair;

//Max time used: 0.01/1.00, max memory used: 8105984/536870912
int dfs_iterative(vector<vector<int> > &adj, int x, int y) {

  int n = adj.size();
  int visited[n] = {0}; //= 1 if visited

  std::stack<int> st;
  st.push(x);
  visited[x] = 1;
  while(!st.empty()){
    int v = st.top();  //Pop a vertex from stack to visit next
    st.pop();
    //visit all neighbor nodes and mark "visited"
    for(auto it = adj[v].begin(); it != adj[v].end(); it++){
      if(visited[*it] == 0){
        st.push(*it);
        visited[*it] = 1;
        if(*it == y)//if x and y are connected
          return 1;
      }
    }
  }
  return 0;
}

int main() {
  size_t n, m;
  std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  
  for (size_t i = 0; i < m; i++) {
    int x, y;
    std::cin >> x >> y;
    adj[x - 1].push_back(y - 1);
    adj[y - 1].push_back(x - 1);
  }
  int x, y;
  std::cin >> x >> y;
  std::cout << dfs_iterative(adj, x - 1, y - 1);

  getchar();
  return 0;
}