console.clear();

const SESSION = "session";
const BREAK = "break";
const millisecondsPerTick = 1000;
//in minutes
const defaultSessionLength = 25;
const defaultBreakLength = 5;
const maxSessionLength = 60;
const maxBreakLength = 60;

class Pomodoro extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      running: false,
      currentPhase: SESSION,
      currentTimeLeft: defaultSessionLength * 60, //in seconds
      sessionLength: defaultSessionLength, //in minutes
      breakLength: defaultBreakLength //in minutes
    }
    this.resetClick = this.resetClick.bind(this);
    this.adjustmentClick = this.adjustmentClick.bind(this);
    this.timerToggle = this.timerToggle.bind(this);
  }
  
  resetClick(){
    let beep = document.getElementById("beep");
    beep.pause();
    beep.currentTime = 0;
    clearInterval(this.pomodoroTimer);
    document.getElementById("break-length").innerHTML = defaultBreakLength;
    document.getElementById("session-length").innerHTML = defaultSessionLength;

    this.setState({
      running: false,
      currentPhase: SESSION,
      currentTimeLeft: defaultSessionLength * 60,
      sessionLength: defaultSessionLength,
      breakLength: defaultBreakLength
    }, () => {
      document.getElementById("timer-label").innerHTML = this.state.currentPhase.charAt(0).toUpperCase() + this.state.currentPhase.slice(1);  
      this.updateTimeLeftDisplay(this.state.currentTimeLeft);
    });
  }
  
  adjustmentClick(event){
    //console.log(event.target) 
    let step = +event.target.dataset.step, maxLength, phase;
    switch(event.target.id){
      case "session-increment": case "session-decrement":
        maxLength = maxSessionLength; phase = SESSION;
        break;
      case "break-increment": case "break-decrement":
        maxLength = maxBreakLength; phase = BREAK;
        break;
      default:
        console.error("assign this event to a wrong HTML element");
    }

    if(this.state[phase+"Length"] + step <= 0 || this.state[phase+"Length"] + step > maxLength) return;
    
    this.setState(state => {
      let newLength = state[phase+"Length"] + step;
      document.getElementById(phase+"-length").innerHTML = newLength;
      let updateState = {};
      if(!state.running && state.currentPhase == phase){
        this.updateTimeLeftDisplay(newLength * 60);
        updateState[phase+"Length"] = newLength;
        updateState["currentTimeLeft"] = newLength * 60;
        return updateState;
      }
      updateState[phase+"Length"] = newLength;
      return updateState;
    });
  }
  
  tick(){
    this.setState(state => {
      if(state.currentTimeLeft < 1){
        document.getElementById("beep").play(); 
        let newPhase = state.currentPhase == SESSION ? BREAK : SESSION;
        let newPhaseTimeLeft = state.currentPhase == SESSION ? state.breakLength : state.sessionLength;
        document.getElementById("timer-label").innerHTML = newPhase.charAt(0).toUpperCase() + newPhase.slice(1);
        this.updateTimeLeftDisplay(newPhaseTimeLeft * 60);
        return ({currentTimeLeft: newPhaseTimeLeft * 60, currentPhase: newPhase});
      }
      else{
        this.updateTimeLeftDisplay(this.state.currentTimeLeft - 1)
        return ({currentTimeLeft: state.currentTimeLeft - 1});
      }
    })
  }
  
  timerToggle(){
    if(!this.state.running){
      this.pomodoroTimer = setInterval(
        () => this.tick(),
        millisecondsPerTick
      );
    }
    else{
      clearInterval(this.pomodoroTimer);
    }
    
    this.setState(state => ({
      running: !state.running
    }));
  }

  updateTimeLeftDisplay(timeLeft){
    document.getElementById("time-left").innerHTML = 
      ("0" + Math.floor(timeLeft / 60)).substr(-2) + ":" + ("0" + timeLeft % 60).substr(-2);
  }
  
  componentDidMount(){
    this.updateTimeLeftDisplay(this.state.currentTimeLeft);
    document.getElementById("timer-label").innerHTML = this.state.currentPhase.charAt(0).toUpperCase() + this.state.currentPhase.slice(1);
  }
  
  render(){
    return (
      <div>
        <header>Pomodoro Clock</header>
        <div className="adjustment-container">
         {/*This doesnt work with FCC test but fine overall
              <svg width="30" height="30" viewBox="0 0 60 60" id="test">
                <rect width="100%" height="100%" fill="none"/>
                <path
                  fill="red"
                  stroke="#000000"
                  strokeLinecap="butt"
                  strokeLinejoin="miter"
                  strokeOpacity="1"
                  strokeWidth="3"
                  d="m 23.181548,59.645833 v -30 H 7.1815476 L 31.181548,4.6458333 l 24,24.9999997 h -16 v 30 z" 
                  data-step={1}
                  id="session-increment"
                  onClick={this.adjustmentClick}/>
              </svg>
          */}
            <div id="session-label">Session Length</div>
            <button id="session-increment" data-step={1} onClick={this.adjustmentClick}>Up</button>
            <button id="session-decrement" data-step={-1} onClick={this.adjustmentClick}>Down</button>
            <div id="session-length">{defaultSessionLength}</div>
          
            <div id="break-label">Break Length</div>
            <button id="break-increment" data-step={1} onClick={this.adjustmentClick}>Up</button>
            <button id="break-decrement" data-step={-1} onClick={this.adjustmentClick}>Down</button>
            <div id="break-length">{defaultBreakLength}</div>
        </div>
        
        <div id="timer-label"></div>
        <div id="time-left"></div>
        <button id="start_stop" onClick={this.timerToggle}>Start/Stop</button>
        <button id="reset" onClick={this.resetClick}>Reset</button>
      </div>
    );
  }
}

ReactDOM.render(<Pomodoro />, document.getElementById("root"));
