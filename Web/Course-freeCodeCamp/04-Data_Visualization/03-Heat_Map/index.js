"use strict";
console.clear();

const monthList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const colorList = ["#3f0ef1", "#1843e7", "#189fe7", "#1de2c8", "#1be46e", "#a6e41b", "#e5b41a", "#eb6914", "#e91d16", "#e7185c", "#eb14aa"];
const cutoffList = [2.8, 3.9, 5.0, 6.1, 7.2, 8.3, 9.5, 10.6, 11.7, 12.8];

document.addEventListener("DOMContentLoaded", function(){
  
  d3.select("body").append("h1")
    .attr("id", "title")
    .text("Heat Map")
  
  var req = new XMLHttpRequest();
  req.open("GET", "https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/global-temperature.json", true);
  req.send();
  req.onload = function(){
    var dataset = JSON.parse(req.responseText);

    const baseTemp = dataset.baseTemperature;
    const monthlyTemp = dataset.monthlyVariance;
    
//d3
    d3.select("body").append("h2")
      .attr("id", "description")
      .html("Base Temperature: " + baseTemp + "&deg;C")
    
    const w = 800, h = 500, padding = 80, rectWidth = 3, rectHeight = 15;

    const svg = d3.select("body").append("svg")
                                 .attr("width", w)
                                 .attr("height", h)
    
    //scale
    const xScale = d3.scaleTime().domain(
      [d3.min(monthlyTemp, d =>  new Date(d.year, 0)), 
       d3.max(monthlyTemp, d =>  new Date(d.year, 0))])
                                 .range([padding, w - padding]);
    const yScale = d3.scaleLinear().domain(
      [d3.min(monthlyTemp, d => d.month), 
       d3.max(monthlyTemp, d => d.month)])
                                   .range([padding, h - padding]);
    
    const cellHeight = 30;
    svg.selectAll("rect")
       .data(monthlyTemp)
       .enter()
       .append("rect")
       .attr("class", "cell")
       .attr("x", d => xScale(new Date(d.year, 0)))
       .attr("y", d => yScale(d.month))
       .attr("width", rectWidth)
       .attr("height", cellHeight)
       .attr("data-month", d => d.month - 1)
       .attr("data-year", d => d.year)
       .attr("data-temp", d => baseTemp + d.variance)
       .style("fill", function(d){
          for(let i = 0; i < cutoffList.length; ++i){
            if(baseTemp + d.variance < cutoffList[i])  return colorList[i];
          }
          return colorList[colorList.length - 1];
        })
    //axis
    const xAxis = d3.axisBottom(xScale); 
    svg.append("g")
       .attr("id", "x-axis")
       .attr("transform", "translate\(0," + (h - padding + cellHeight) + "\)")
       .call(xAxis)
    console.log([...Array(12).keys()])
    const yAxis = d3.axisLeft(yScale)
                    .tickFormat(d => monthList[d-1])

    svg.append("g")
       .attr("id", "y-axis")
       .attr("transform", "translate\(" + padding + ", " + cellHeight/2 + "\)")
       .call(yAxis)
    
    //d3 - legend
    svg.append("g")
       .attr("id", "legend")
       .attr("height", "300")
       .attr("width", "450");
    
    var legend = d3.select("#legend");
    
    const legendRectWidth = 30;
    const legendX = w - 410; //beginning (x, y) coordinate
    const legendY = 30;
  
    colorList.forEach((d, i) => {
      legend.append("rect")
       .attr("x", legendX + legendRectWidth * i)
       .attr("y", legendY)
       .attr("width", legendRectWidth)
       .attr("height", 20)
       .style("fill", d)
       .style("stroke", "black")
      
      if(i < cutoffList.length){
        legend.append("text")
              .attr("x", legendX + legendRectWidth * (i+1) - 5)
              .attr("y", legendY + 40)
              .text(cutoffList[i].toFixed(1))
              .style("font-size", "10px")
              .attr("alignment-baseline","middle")
      
        legend.append("line")
              .attr("x1", legendX + legendRectWidth * (i+1))
              .attr("y1", legendY)
              .attr("x2", legendX + legendRectWidth * (i+1))
              .attr("y2", legendY + 30)
              .attr("stroke-width", 1)
              .attr("stroke", "black")
      }
    })
    //d3 - tooltip
    var tooltip = d3.select("body")
                    .append("div")
                    .attr("id", "tooltip")
                    .style("position", "absolute")
                    .style("visibility", "hidden")
    
    d3.selectAll(".cell")
      .on("mouseover", function(d){
        var rect = event.target.getBoundingClientRect();
        tooltip.attr("data-year", d.year)
               .html(monthList[d.month - 1] + " " + d.year + " Temp: " + (baseTemp + d.variance).toFixed(1) + "&deg;C")
               .style("visibility", "visible")
               .style("top", (rect.top + window.scrollY - 30) + "px")
               .style("left",  (event.pageX > document.body.clientWidth - 200 ? event.pageX - 200 : event.pageX) + "px")
      })
      .on("mouseout", function(){
          return tooltip.style("visibility", "hidden");
      });
  }
});
  