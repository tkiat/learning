module Functor where

class Functor' f where
  fmap' :: (a -> b) -> f a -> f b

-------------------------------------------------------------------------------
-- Instances

instance Functor' [] where
  fmap' _ [] = []
  fmap' g (x : xs) = g x : fmap g xs

myList :: [Int]
myList = fmap' (+ 1) [1, 2, 3]

instance Functor' Maybe where
  fmap' _ Nothing = Nothing
  fmap' g (Just a) = Just (g a)

myMaybe1 :: Maybe Int
myMaybe1 = fmap' (+ 1) (Just 7)

myMaybe2 :: Maybe Int
myMaybe2 = fmap' id Nothing

-- ex 1.1

instance Functor' (Either e) where
  fmap' _ (Left a) = Left a
  fmap' g (Right a) = Right (g a)

myEither1 :: Either Int Int
myEither1 = fmap' (+ 1) (Left 7)

myEither2 :: Either Int Int
myEither2 = fmap' (+ 1) (Right 7)

-- ex 1.2

instance Functor' ((->) e) where
  fmap' g f = g . f

myArrow :: Int
myArrow = fmap' (+ 1) (* 2) 3

-- ex 2.1

instance Functor' ((,) e) where
  fmap' g (a, b) = (a, g b)

myTuple :: (String, Int)
myTuple = fmap' (+ 1) ("gg", 1)

-- ex2.2

data Pair a = Pair a a deriving (Show)

instance Functor Pair where
  fmap g (Pair a b) = Pair (g a) (g b)

myPair :: Pair Int
myPair = fmap (+ 1) (Pair 2 3)

-- ex3
data ITree a = Leaf (Int -> a) | Node [ITree a]

instance Functor ITree where
  fmap g (Leaf x) = Leaf (g . x)
  fmap f (Node xs) = Node (map (fmap f) xs)

myITree1 :: ITree Int
myITree1 = fmap' id Leaf (+ 2)

myITree2 :: ITree Int
myITree2 = fmap' id Node [Leaf (+ 2), Leaf (+ 3), Node [Leaf (+ 4)]]

-- ex4

-- From: https://stackoverflow.com/a/16118529
newtype Ex4 a = Ex4 (a -> Int)

-- cannot satisfy fmap' :: (a -> b) -> f a -> f b
-- or (a -> b) -> (a -> Int) -> (b -> Int)
-- i.e. cannot produce (b -> Int) from (a -> b) and (a -> Int)

-- ex5
-- True. The intuition is that a functor doesn't change the structure of the
-- container, so do the composition of two functors.

-------------------------------------------------------------------------------
-- Laws
-- ex2:
-- break 1st law: fmap id [1,2,3] = [1,1,2,2,3,3] \= [1,2,3]
-- don't break 2nd law: fmap ((+1) . (*2)) == fmap (+1) . fmap (*2)

-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStr "myList: " >> print myList
  putStr "myMaybe1: " >> print myMaybe1
  putStr "myMaybe2: " >> print myMaybe2
  putStr "myEither1: " >> print myEither1
  putStr "myEither2: " >> print myEither2
  putStr "myArrow: " >> print myArrow
  putStr "myTuple: " >> print myTuple
  putStr "myPair: " >> print myPair
