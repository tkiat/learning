#include <iostream>
#include <vector>
#include <climits>
#include <queue>
#include <tuple>

using std::vector;

//Result: Max time used: 0.01/1.00, max memory used: 9142272/536870912
//----------------------------------------
/* This problem can be reduced to 38_Q1*/
class FlowGraph {

public:

  struct Edge {
    int from, to, capacity, flow;
  };

private:
    
  vector<Edge> edges; // List of all forward and backward edges
  vector<vector<size_t> > graph; //adjacency lists, store only indices of edges in the edges list
  unsigned int num_left;

public:

  explicit FlowGraph(size_t n): graph(n) {} //explicit forgoes implicit conv

  void add_edge(int from, int to, int capacity) {

    Edge forward_edge = {from, to, capacity, 0};
    Edge backward_edge = {to, from, 0, 0};

    graph[from].push_back(edges.size());
    edges.push_back(forward_edge);

    graph[to].push_back(edges.size());
    edges.push_back(backward_edge);

  }

  size_t size() const {
    return graph.size();
  }

  const vector<size_t>& get_ids(int from) const {
    return graph[from];
  }

  const Edge& get_edge(size_t id) const {
    return edges[id];
  }

  void add_num_left(int num) {
    num_left = num;
  }

  unsigned int get_num_left() {
    return num_left;
  }

  void add_flow(size_t id, int flow) {

    edges[id].flow += flow;
    edges[id ^ 1].flow -= flow;
  }
};

std::pair<int, vector<int>> bfs(FlowGraph &graph, int from, int to){

  int n = graph.size();

  vector<int> parent(n);
  std::fill(parent.begin(), parent.end(), -1);
  parent[from] = -2;//make sure src is not rediscovered

  int M[n]; //current path capacity
  M[from] = INT_MAX;

  std::queue<int> q;
  q.push(from);

  while(!q.empty()){
    //u = from, v = to
    int u = q.front();
    q.pop();
    for(auto it = graph.get_ids(u).begin(); it != graph.get_ids(u).end(); it++){

      int v = graph.get_edge(*it).to;

      if(graph.get_edge(*it).capacity - graph.get_edge(*it).flow  > 0 
        && parent[v] == -1){

        parent[v] = u;
        M[v] = std::min(M[u], graph.get_edge(*it).capacity - graph.get_edge(*it).flow);
        
        if(v != to)
          q.push(v);
        else
          return std::make_pair(M[to], parent);
      }
    }
  }
  return std::make_pair(0, parent);
}

vector<int> edmondsKarp(FlowGraph& graph, int from, int to) {

  int maxFlow = 0;
  int n = graph.size();

  vector<int> result;

  while(1){
    
    int flow;
    vector<int> parent;
    std::tie(flow, parent) = bfs(graph, from, to);

    if (flow == 0)
      break;

    maxFlow += flow;
    int cur = to;

    while(cur != from){

      int prev = parent[cur];

      for(auto it = graph.get_ids(prev).begin(); it != graph.get_ids(prev).end(); it++){
        if(graph.get_edge(*it).to == cur 
          && graph.get_edge(*it).capacity - graph.get_edge(*it).flow >= flow){
          graph.add_flow(*it, flow);
          break;
        }
      }
      cur = prev;
    }
  }
  //Push all flows to result
  for(int u = 0; u <= graph.get_num_left() - 1; u++){
    bool flag = true;
    for(auto it = graph.get_ids(u).begin(); it != graph.get_ids(u).end(); it++){
      if(graph.get_edge(*it).flow == 1){
        result.push_back(graph.get_edge(*it).to - (graph.get_num_left()-1));
        flag = false;
        break;
      }
   }
    if(flag)
      result.push_back(-1);
  }

  return result;
}
//Main
FlowGraph read_data();
int main() {

    std::ios_base::sync_with_stdio(false);
    FlowGraph graph = read_data();

    vector<int> result = edmondsKarp(graph, graph.size() - 2, graph.size() - 1);
    
    for(auto it = result.begin(); it != result.end(); it++)
      std::cout << *it << " ";
    
    return 0;
}
//Util Functions
FlowGraph read_data() {

  int capacity = 1;
  int num_left, num_right;
  std::cin >> num_left >> num_right;
  FlowGraph graph(num_left + num_right + 2); //+2 for source and sink
  graph.add_num_left(num_left);

  for(int u = 1; u <= num_left; u++){
    for(int v = num_left + 1; v <= num_left + num_right; v++){
      int temp;
      std::cin >> temp;
      if(temp == 1)
        graph.add_edge(u - 1, v - 1, capacity);
    }
  }
  //add sources and sink
  int s = num_left + num_right + 1;
  for (int u = 1; u <= num_left; u++)
    graph.add_edge(s - 1, u - 1, capacity);

  int t = s + 1;
  for (int v = num_left + 1; v <= num_left + num_right; v++)
    graph.add_edge(v - 1, t - 1, capacity);

  return graph;
}
