#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <list>

using std::string;
using std::vector;
using std::cin;

//Result: Max time used: 0.20/1.00, max memory used: 26472448/536870912
struct query{
    string type, value;
    size_t key;
};
class hash{

    int numBucket;
    std::list<string> *table;
    static const int p = 1000000007; //before this size_t but have prob with neg mod p
    static const int x = 263;

public:

    hash(int b){
        this->numBucket = b;
        table = new std::list<string>[numBucket];
    }
    //my code
    void add(string value){
        //ignore query if there is already such string in the hash table
        if(find(value) == "yes")
            return;
        int index = hashFunction(value);
        table[index].push_back(value);
    }
    int hashFunction(const string& value) {   //map value to key
        unsigned long long h_S = 0;
        //feed last char first
        for (int i = static_cast<int> (value.size()) - 1; i >= 0; i--){
            h_S = (h_S * x + value[i]) % p;
        }
        return h_S % numBucket;
    }
    string find(string value) {   //map value to key
        int index = hashFunction(value);
        std::list <string>::iterator it;
        for (it = table[index].begin(); it != table[index].end(); it++) {
            if (*it == value)
                return "yes";
        }
        return "no";
    }
    void del(string value){
        int index = hashFunction(value);
        //find and delete
        std::list <string> :: iterator it;
        for (it = table[index].begin(); it != table[index].end(); it++) {
            if (*it == value){//if found
                table[index].erase(it);
                break;
            }
        }
    }
    void check(int index) {  
        //output the content of the 𝑖-th list in the table in reverse order
        std::list <string> :: reverse_iterator it;
        for (it = table[index].rbegin(); it != table[index].rend(); it++)
            std::cout << *it << " ";
        std::cout << std::endl;
    }
    void displayHash() {
        for (int i = 0; i < numBucket; i++) {
            std::cout << i;
            for (string x : table[i])
                std::cout << " --> " << x;
            std::cout << std::endl;
        }
    }
};
query readQuery(){
    query _query;
    cin >> _query.type;
    if (_query.type != "check")
        cin >> _query.value;
    else
        cin >> _query.key;
    return _query;
}
void processQuery(const query& _query, hash &h) {
    //my code
    if (_query.type == "check") 
        h.check(_query.key);
    else if (_query.type == "find")
        std::cout << h.find(_query.value) << "\n";
    else if (_query.type == "add") 
        h.add(_query.value);
    else if (_query.type == "del")
        h.del(_query.value);
}

int main() {

    std::ios_base::sync_with_stdio(false);
    int bucket_count;
    cin >> bucket_count;

    hash h(bucket_count);
    int n;
    cin >> n;
    for (int i = 0; i < n; i++){
        query _query = readQuery();
        processQuery(_query, h);
    }
    return 0;
}
//refFile
/*
struct Query {
    string type, s;
    size_t ind;
};
*/

/*
class QueryProcessor {
    int bucket_count;
    // store all strings in one vector
    vector<string> elems;
    size_t hash_func(const string& s) const {
        static const size_t multiplier = 263;
        static const size_t prime = 1000000007;
        unsigned long long hash = 0;
        for (int i = static_cast<int> (s.size()) - 1; i >= 0; --i)
            hash = (hash * multiplier + s[i]) % prime;
        return hash % bucket_count;
    }

public:

    explicit QueryProcessor(int bucket_count): bucket_count(bucket_count) {}

    Query readQuery() const {
        Query query;
        cin >> query.type;
        if (query.type != "check")
            cin >> query.s;
        else
            cin >> query.ind;
        return query;
    }

    void writeSearchResult(bool was_found) const {
        std::cout << (was_found ? "yes\n" : "no\n");
    }

    void processQuery(const Query& query) {
        if (query.type == "check") {
            // use reverse order, because we append strings to the end
            for (int i = static_cast<int>(elems.size()) - 1; i >= 0; --i)
                if (hash_func(elems[i]) == query.ind)
                    std::cout << elems[i] << " ";
            std::cout << "\n";
        } else {
            vector<string>::iterator it = std::find(elems.begin(), elems.end(), query.s);
            if (query.type == "find")
                writeSearchResult(it != elems.end());
            else if (query.type == "add") {
                if (it == elems.end())
                    elems.push_back(query.s);
            } else if (query.type == "del") {
                if (it != elems.end())
                    elems.erase(it);
            }
        }
    }

    void processQueries() {
        int n;
        cin >> n;
        for (int i = 0; i < n; ++i)
            processQuery(readQuery());
    }
};

*/