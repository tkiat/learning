#include <iostream>
#include <vector>

using std::vector;

//Result: Max time used: 0.05/2.00, max memory used: 13156352/536870912
//TODO: trace back list of items that give most value
int knapsack_01(int W, const vector<int> &w) {

  int num_items = w.size();
  vector<vector<int>> value(W+1, vector<int>(num_items));

	for (int i = 0; i <= W; i++){
    value[i][0] = 0;
  }
  for(int i = 0; i < num_items; i++){
    value[0][i] = 0;
  }
  for(int i = 0; i < num_items; i++){
    for(int j = 1; j <= W; j++){

      value[j][i] = value[j][i-1];
      if(w[i] <= j){
        int temp = value[(j-w[i])][i-1] + w[i]; //replace this with v[i] for general case
        if(temp > value[j][i])
          value[j][i] = temp;
      }
    }
  }
  return value[W][num_items-1];
}
int main() {

  int n, W;
  std::cin >> W >> n;
  vector<int> w(n);
  for (int i = 0; i < n; i++)
    std::cin >> w[i];
  std::cout << knapsack_01(W, w) << '\n';
}
