"use strict";
console.clear();

const colorList = ["#3f0ef1", "#189fe7", "#1de2c8", "#1be46e", "#a6e41b", "#e5b41a", "#eb6914", "#e91d16", "#eb14aa"];
const cutoffList = [0, 3, 12, 21, 30, 39, 48, 57, 66];

document.addEventListener('DOMContentLoaded', function(){
  d3.select("body").append("h1")
    .attr("id", "title")
    .text("Heat Map")
  
  d3.select("body").append("h3")
    .attr("id", "description")
    .text("Percentage of adults age 25 and older with a bachelor's degree or higher (2010-2014)")
  
  var req_edu = new XMLHttpRequest();
  req_edu.open("GET", "https://raw.githubusercontent.com/no-stack-dub-sack/testable-projects-fcc/master/src/data/choropleth_map/for_user_education.json", true);
  req_edu.send();
  req_edu.onload = function(){
    var req_counties = new XMLHttpRequest();
    req_counties.open("GET", "https://raw.githubusercontent.com/no-stack-dub-sack/testable-projects-fcc/master/src/data/choropleth_map/counties.json", true);
    req_counties.send();
    req_counties.onload = function(){
      
      //http requests' response
      var dataset_edu = {};
      JSON.parse(req_edu.responseText).forEach(d => {
        dataset_edu[d.fips] = {area_name: d.area_name, 
                       bachelorsOrHigher: d.bachelorsOrHigher,
                                   state: d.state}
      });
      var dataset_counties = JSON.parse(req_counties.responseText);
      
      //d3 - const
      const w = 960, h = 600;
      const svg = d3.select("body")
                    .append("svg")
                    .attr("width", w)
                    .attr("height", h) 
      var path = d3.geoPath();
      //d3 - convert topojson to geojson
      const geojson = topojson.feature(dataset_counties, dataset_counties.objects.counties);
      const features = geojson.features;
      //d3 - shape
      svg.append("g")
         .selectAll("path")
         .data(features)
         .enter()
         .append("path")
         .attr("class", "county")
         .attr("d", path)
         .attr("data-fips", d => d.id)
         .attr("data-education", d => dataset_edu[d.id].bachelorsOrHigher)
         .attr("data-area_name", d => dataset_edu[d.id].area_name)
         .attr("data-state", d => dataset_edu[d.id].state)
         .style("fill", function(d){
          for(let i = cutoffList.length - 1; i >= 0 ; --i){
            if(dataset_edu[d.id].bachelorsOrHigher > cutoffList[i])  
              return colorList[i];
          }
          return colorList[colorList.length - 1];
        })
       
      //d3 - legend
      svg.append("g")
       .attr("id", "legend")
       .attr("height", "300")
       .attr("width", "450");
    
      var legend = d3.select("#legend");

      const legendRectWidth = 30;
      const legendX = w - 410, legendY = 30; //beginning coordinate
      
      colorList.forEach((d, i) => {
        legend.append("rect")
         .attr("x", legendX + legendRectWidth * i)
         .attr("y", legendY)
         .attr("width", legendRectWidth)
         .attr("height", 20)
         .style("fill", d)
         .style("stroke", "black")

          legend.append("text")
                .attr("x", legendX + legendRectWidth * i - 5)
                .attr("y", legendY + 40)
                .text(cutoffList[i]+"%")
                .style("font-size", "10px")
                .attr("alignment-baseline","middle")

          legend.append("line")
                .attr("x1", legendX + legendRectWidth * i)
                .attr("y1", legendY)
                .attr("x2", legendX + legendRectWidth * i)
                .attr("y2", legendY + 30)
                .attr("stroke-width", 1)
                .attr("stroke", "black")
      })
      
      //d3 - tooltip
      var tooltip = d3.select("body")
                      .append("div")
                      .attr("id", "tooltip")
                      .style("position", "absolute")
                      .style("visibility", "hidden")

      d3.selectAll(".county")
        .on("mouseover", function(){
          let county_data = event.target.dataset;
          let rect = event.target.getBoundingClientRect();
          tooltip.attr("data-education", county_data.education)
                 .text(county_data.area_name + ", " + county_data.state + " " + county_data.education + "%")
                 .style("visibility", "visible")
                 .style("top", (rect.top + window.scrollY - 30) + "px")
                 .style("left",  (event.pageX > document.body.clientWidth - 200 ? event.pageX - 200 : event.pageX) + "px")
        
        })
        .on("mouseout", function(){
            return tooltip.style("visibility", "hidden");
        });
      
    }
  }
});
