#include <algorithm>
#include <iostream>
#include <vector>
#include <queue>
#if defined(__unix__) || defined(__APPLE__)
#include <sys/resource.h>
#endif

//Result: Max time used: 0.04/1.00, max memory used: 15851520/536870912
class Node;

class Node {
public:
    int key;
    Node *parent;
    std::vector<Node *> children;

    Node() {
      this->parent = NULL;
    }
    //children.setParent(theirParent)
    void setParent(Node *theParent) {
      parent = theParent;
      parent->children.push_back(this);
    }
};
//Input: 5 4 -1 4 1 1 means for these 5 nodes 
//nodes[0].setParent(node 4): node 0 becomes child of node 4
//nodes[1].setParent(node -1): node 1 becomes root
//nodes[2].setParent(node 4): node 2 becomes child of node 4 ...
//0  1 2 3 4
//4 -1 4 1 1
//     1
//  3      4
//       0    2
int main_with_large_stack_space() {

  std::ios_base::sync_with_stdio(0);
  int n;
  std::cin >> n;

  //my code
  std::vector<Node> nodes;
  nodes.resize(n);
  int root;
  for (int child_index = 0; child_index < n; child_index++) {
    int parent_index;
    std::cin >> parent_index;
    if (parent_index >= 0)
      nodes[child_index].setParent(&nodes[parent_index]);
    nodes[child_index].key = child_index;
    if(parent_index == -1)  
      root = child_index;
  }
  //find height (iterative)
  std::queue <Node*> q;
  int height = 0;
  q.push(&nodes[root]);
  while(1){
  
    int nodeCount = q.size();
    if(nodeCount == 0)
      break;
    height++;
    
    while(nodeCount > 0){

      if(q.size() == 0)
        break;
      Node *temp = q.front();
      q.pop();
      for(auto it = (temp->children).begin(); it != (temp->children).end(); it++)
        q.push(&nodes[(*it)->key]);
      nodeCount--;
    }
  }
  std::cout << height;
  /*
  //refFile code (slow)
  int maxHeight = 0;
  for (int leaf_index = 0; leaf_index < n; leaf_index++) {
    int height = 0;
    for (Node *v = &nodes[leaf_index]; v != NULL; v = v->parent)
      height++;
    maxHeight = std::max(maxHeight, height);
  }
  //std::cout << maxHeight << std::endl;
  */
  return 0;
}

int main (int argc, char **argv){

#if defined(__unix__) || defined(__APPLE__)
  // Allow larger stack space
  const rlim_t kStackSize = 16 * 1024 * 1024;   // min stack size = 16 MB
  struct rlimit rl;
  int result;

  result = getrlimit(RLIMIT_STACK, &rl);
  if (result == 0)
  {
      if (rl.rlim_cur < kStackSize)
      {
          rl.rlim_cur = kStackSize;
          result = setrlimit(RLIMIT_STACK, &rl);
          if (result != 0)
          {
              std::cerr << "setrlimit returned result = " << result << std::endl;
          }
      }
  }

#endif
  return main_with_large_stack_space();
}
