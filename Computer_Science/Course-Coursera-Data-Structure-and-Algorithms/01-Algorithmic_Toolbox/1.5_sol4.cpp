#include <iostream>
#include <vector>

using std::vector;

//Result: Max time used: 0.00/1.00, max memory used: 9719808/536870912
/* 
DP[i+1][j+1] = |LCS| of a[0]...a[i] and b[0]...b[j]
e.g. a = {2, 7, 8, 3}, b = {5, 2, 8, 7}
DP becomes
             [5][5 2][5 2 8][5 2 8 7]
           0  0   0     0       0
[2]        0  0   1     1       1
[2 7]      0  0   1     1       2
[2 7 8]    0  0   1     2       2
[2 7 8 3]  0  0   1     2       2
*/
int lcs2(vector<int> &a, vector<int> &b){

  int m = a.size();
  int n = b.size();
  vector<vector<int>> DP(m+1, vector<int>(n+1));

  for (int i = 0; i <= m; i++){
    for (int j = 0; j <= n; j++){

      if (i == 0 || j == 0)
        DP[i][j] = 0;

      else if (a[i-1] == b[j-1])
        DP[i][j] = DP[i-1][j-1] + 1;

      else
        DP[i][j] = std::max(DP[i-1][j], DP[i][j-1]);
    }
  }
  /*
  for(int i = 0; i <= m; i++){
    for(int j = 0; j <= n; j++){
      std::cout << DP[i][j] << " ";
    }
    std::cout << std::endl;
  }
  */
  return DP[m][n];
}
int main() {

  size_t n;
  std::cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < n; i++) 
    std::cin >> a[i];

  size_t m;
  std::cin >> m;
  vector<int> b(m);
  for (size_t i = 0; i < m; i++)
    std::cin >> b[i];

  std::cout << lcs2(a, b) << std::endl;
}