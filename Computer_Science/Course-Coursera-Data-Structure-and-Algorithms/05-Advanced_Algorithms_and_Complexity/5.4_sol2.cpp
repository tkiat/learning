#include <iostream>
#include <vector>
#include <sys/resource.h>
#include <algorithm>
#include <climits>

using std::vector;

//Result: Max time used: 0.13/1.00, max memory used: 22716416/536870912
struct Vertex {
    int weight;
    std::vector <int> children;
};
typedef std::vector<Vertex> Graph;

Graph ReadTree() {
  int vertices_count;
  std::cin >> vertices_count;

  Graph tree(vertices_count);

  for (int i = 0; i < vertices_count; ++i)
    std::cin >> tree[i].weight;

  for (int i = 1; i < vertices_count; ++i) {
    int from, to, weight;
    std::cin >> from >> to;
    //undirectional
    tree[from - 1].children.push_back(to - 1);
    tree[to - 1].children.push_back(from - 1);
  }
  return tree;
}

int dfs(const Graph &tree, int vertex, int parent, vector<int> &D) {

  if(D[vertex] == INT_MAX){

    if(tree[vertex].children.size() == 0)
      D[vertex] = tree[vertex].weight;
    else{
      int m_1 = tree[vertex].weight;
      for (int child : tree[vertex].children){
        if(child != parent){
          for (int grandchild : tree[child].children){
            if (grandchild != vertex)
              m_1 += dfs(tree, grandchild, child, D);
          }
        }
      }
      int m_0 = 0;
      for (int child : tree[vertex].children){
        if(child != parent)
          m_0 += dfs(tree, child, vertex, D);
      }
      D[vertex] = std::max(m_1, m_0);
    }
  }
  return D[vertex];
}

int MaxWeightIndependentTreeSubset(const Graph &tree) {
  size_t size = tree.size();
  if (size == 0)
    return 0;
  vector<int> D(tree.size());
  std::fill(D.begin(), D.end(), INT_MAX);

  int max = INT_MIN;
  for(int i = 0; i < size; i++){
    if(D[i] == INT_MAX){
      int temp = dfs(tree, i, -1, D); //-1 since we don't care what is i's parent
    if(temp > max)
      max = temp;
    }
  }
  return max;
}

int main() {
  // This code is here to increase the stack size to avoid stack overflow
  // in depth-first search.
  const rlim_t kStackSize = 64L * 1024L * 1024L;  // min stack size = 64 Mb
  struct rlimit rl;
  int result;
  result = getrlimit(RLIMIT_STACK, &rl);
  if (result == 0)
  {
    if (rl.rlim_cur < kStackSize)
    {
      rl.rlim_cur = kStackSize;
      result = setrlimit(RLIMIT_STACK, &rl);
      if (result != 0)
      {
          fprintf(stderr, "setrlimit returned result = %d\n", result);
      }
    }
  }

  // Here begins the solution
  Graph tree = ReadTree();
  int weight = MaxWeightIndependentTreeSubset(tree);
  std::cout << weight << std::endl;
  return 0;
}
