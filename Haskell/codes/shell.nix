{ pkgs ? import <nixpkgs> { } }:

let
  myGhc = pkgs.haskellPackages.ghcWithPackages (hpkgs: with hpkgs; [
    lens
    mtl
    pretty
    prettyprinter
    transformers
  ]);
in
pkgs.mkShell {
  buildInputs = [
    myGhc
    #     pkgs.haskellPackages.haskell-language-server
    pkgs.haskellPackages.ormolu
  ];
}
