#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
#include <unordered_set>

using std::vector;
using std::unordered_set;
//Result: Max time used: 0.27/1.00, max memory used: 44433408/1073741824
//----------------------------------------
vector<vector<int>> getReverse(vector<vector<int> > &G);
vector<int> toposort(vector<vector<int> > adj);
void dfs_sub_recursive(vector<vector<int> > &adj, vector<int> &visited, vector<int> &order, int u);
void dfs_sub_recursive_2(vector<vector<int> > &adj, vector<int> &visited, unordered_set<int> &scc_elements, int u);
void dfs_iterative(vector<vector<int> > &adj, vector<int> &visited, unordered_set<int> &scc_elements, int u);

struct TwoSatisfiability {

  int offset;
  vector<vector<int> > clauses;

  TwoSatisfiability(int n): offset((n-1)/2), clauses(n){}

  bool isSatisfiable(vector<int>& result) {

    fill(result.begin(), result.end(), -1);

    vector<int> visited(clauses.size());
    fill(visited.begin(), visited.end(), 0);
    //Kosaraju Step 1: Create reverse graph
    vector<vector<int> > clauses_rev = getReverse(clauses);
    //Kosaraju Step 2: DFS-Loop on reverse graph, get order of all nodes
    vector<int> order = toposort(clauses_rev); //highest postorder is at index 0 of order
    //Kosaraju Step 3: DFS-Loop on original graph in reversing order
    for(auto it = order.rbegin(); it != order.rend(); it++){

      int u = *it;
      
      if(visited[u] == 0){
        //contain all elements in scc
        unordered_set<int> scc_elements;
        
        dfs_iterative(clauses, visited, scc_elements, u);
        //dfs_sub_recursive_2(clauses, visited, scc_elements, u);

        for (auto it = scc_elements.begin(); it != scc_elements.end(); it++){
          //update result, if we get X set X to 1 if we get X' set X to 0
          if(result[abs(*it - offset)-1] == -1)
            result[abs(*it - offset)-1] = (*it < offset) ? 0:1;
          //check if x and -x lie in the same SCC
          if(scc_elements.size() > 1 &&
            scc_elements.find(-(*it - offset) + offset) != scc_elements.end())
              return false;
        }
      }
    }
    return true;
  }
};
//SCCs function form previous assignment
//Step 1
vector<vector<int>> getReverse(vector<vector<int> > &G){

	int G_size = G.size();
	vector<vector<int>> G_rev(G_size);

	for (int i = 0; i < G_size; i++){
		// Recur for all the vertices adjacent to this vertex
		for (auto it = G[i].begin(); it != G[i].end(); it++){
			G_rev[*it].push_back(i);
		}
	}
	return G_rev;
}
//Step 2
vector<int> toposort(vector<vector<int> > adj) {

  int n = adj.size();
  vector<int> visited(n, 0);
  vector<int> order;
  //DFS
  for (int u = 0; u < n; u++)
    if (visited[u] == 0)
      dfs_sub_recursive(adj, visited, order, u);

  return order;
}
void dfs_sub_recursive(vector<vector<int> > &adj, vector<int> &visited, 
  vector<int> &order, int u) {
  
  visited[u] = 1;
  for (vector <int>::iterator v = adj[u].begin(); v != adj[u].end(); v++){
    if (visited[*v] == 0)
      //recursive
      dfs_sub_recursive(adj, visited, order, *v);
  }
  //if all child nodes of u are visited, insert u last
  order.push_back(u);
}
//Step 3
void dfs_iterative(vector<vector<int> > &adj, vector<int> &visited, unordered_set<int> &scc_elements, int u) {

  std::stack<int> st;
  st.push(u);
  visited[u] = 1;
  scc_elements.insert(u);
  int numVertices = 1;
  while(!st.empty()){
    int v = st.top();  //Pop a vertex from stack to visit next
    st.pop();
    //visit all neighbor nodes and mark "visited"
    for(auto it = adj[v].begin(); it != adj[v].end(); it++){
      if(visited[*it] == 0){
        scc_elements.insert(*it);
        st.push(*it);
        visited[*it] = 1;
        numVertices++;
      }
    }
  }
}
void dfs_sub_recursive_2(vector<vector<int> > &adj, vector<int> &visited, 
  unordered_set<int> &scc_elements, int u) {
  
  visited[u] = 1;
  for (vector <int>::iterator v = adj[u].begin(); v != adj[u].end(); v++){
    if (visited[*v] == 0)
      dfs_sub_recursive_2(adj, visited, scc_elements, *v);
  }
  scc_elements.insert(u);
}
//----------------------------------------
int main() {

  std::ios::sync_with_stdio(false);

  int n, m;
  std::cin >> n >> m;
  int offset = n; //to prevent negative input
  TwoSatisfiability twoSat(n + offset + 1);

  for (int i = 0; i < m; ++i) {
    //for each 2-clause l_1 and l_2 introduces
    //two directed edges ~l_1 → l_2 and ~l_2 → l_1
    int l_1, l_2;
    std::cin >> l_1 >> l_2;
    twoSat.clauses[-l_1 + offset].push_back(l_2 + offset);
    twoSat.clauses[-l_2 + offset].push_back(l_1 + offset);
  }

  vector<int> result(n);
  if (twoSat.isSatisfiable(result)) {
    std::cout << "SATISFIABLE" << std::endl;
    for (int i = 1; i <= n; ++i) {

      if (result[i-1]) {
        std::cout << i;
      } else {
        std::cout << -i;
      }
      if (i < n) {
        std::cout << " ";
      } else {
        std::cout << std::endl;
      }
    }
  } else {
    std::cout << "UNSATISFIABLE" << std::endl;
  }
  return 0;
}
 /*
// This solution tries all possible 2^n variable assignments (too slow)
struct Clause {
    int firstVar;
    int secondVar;
};
  for (int mask = 0; mask < (1 << numVars); ++mask) {
    for (int i = 0; i < numVars; ++i) {
      result[i] = (mask >> i) & 1;
    }

    bool formulaIsSatisfied = true;

    for (const Clause& clause: clauses) {
      bool clauseIsSatisfied = false;
      if (result[abs(clause.firstVar) - 1] == (clause.firstVar < 0)) {
        clauseIsSatisfied = true;
      }
      if (result[abs(clause.secondVar) - 1] == (clause.secondVar < 0)) {
        clauseIsSatisfied = true;
      }
      if (!clauseIsSatisfied) {
        formulaIsSatisfied = false;
        break;
      }
    }

    if (formulaIsSatisfied) {
      return true;
    }
  }
  return false;
  */
