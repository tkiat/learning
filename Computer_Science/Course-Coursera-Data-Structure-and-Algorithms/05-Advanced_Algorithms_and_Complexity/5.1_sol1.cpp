#include <iostream>
#include <vector>
#include <climits>
#include <queue>
#include <tuple>

using std::vector;

//Result: Max time used: 0.01/1.00, max memory used: 9228288/536870912
//----------------------------------------
/* This class implements a bit unusual scheme for storing edges of the graph,
 * in order to retrieve the backward edge for a given edge quickly. */
class FlowGraph {

public:

  struct Edge {
    int from, to, capacity, flow;
  };

private:
    
  vector<Edge> edges; // List of all forward and backward edges
  vector<vector<size_t> > graph; //adjacency lists, store only indices of edges in the edges list

public:

  explicit FlowGraph(size_t n): graph(n) {} //explicit forgoes implicit conv

  void add_edge(int from, int to, int capacity) {
    /* Note that we first append a forward edge and then a backward edge,
      * so all forward edges are stored at even indices (starting from 0),
      * whereas backward edges are stored at odd indices in the list edges */
    Edge forward_edge = {from, to, capacity, 0};
    Edge backward_edge = {to, from, 0, 0};

    graph[from].push_back(edges.size());
    edges.push_back(forward_edge);

    graph[to].push_back(edges.size());
    edges.push_back(backward_edge);

  }

  size_t size() const {
    return graph.size();
  }

  const vector<size_t>& get_ids(int from) const {
    return graph[from];
  }

  const Edge& get_edge(size_t id) const {
    return edges[id];
  }

  void add_capacity(size_t id, int capacity) {
    edges[id].capacity += capacity;
  }

  void add_flow(size_t id, int flow) {
    /* To get a backward edge for a true forward edge (i.e id is even), we should get id + 1
      * due to the described above scheme. On the other hand, when we have to get a "backward"
      * edge for a backward edge (i.e. get a forward edge for backward - id is odd), id - 1
      * should be taken.
      *
      * It turns out that id ^ 1 works for both cases. Think this through! */
    edges[id].flow += flow;
    edges[id ^ 1].flow -= flow;
  }
};

std::pair<int, vector<int>> bfs(FlowGraph &graph, int from, int to){

  int n = graph.size();

  vector<int> parent(n);
  std::fill(parent.begin(), parent.end(), -1);
  parent[from] = -2;//make sure src is not rediscovered

  int M[n]; //current path capacity
  M[from] = INT_MAX;

  std::queue<int> q;
  q.push(from);

  while(!q.empty()){
    //u = from, v = to
    int u = q.front();
    q.pop();
    for(auto it = graph.get_ids(u).begin(); it != graph.get_ids(u).end(); it++){

      int v = graph.get_edge(*it).to;

      if(graph.get_edge(*it).capacity - graph.get_edge(*it).flow  > 0 
        && parent[v] == -1){

        parent[v] = u;
        M[v] = std::min(M[u], graph.get_edge(*it).capacity - graph.get_edge(*it).flow);
        
        if(v != to)
          q.push(v);
        else
          return std::make_pair(M[to], parent);
      }
    }
  }
  return std::make_pair(0, parent);
}

int edmondsKarp(FlowGraph& graph, int from, int to) {

  int maxFlow = 0;
  int n = graph.size();

  while(1){
    
    int flow;
    vector<int> parent;
    std::tie(flow, parent) = bfs(graph, from, to);

    if (flow == 0)
      break;

    maxFlow += flow;

    int cur = to;

    while(cur != from){

      int prev = parent[cur];

      for(auto it = graph.get_ids(prev).begin(); it != graph.get_ids(prev).end(); it++){
        if(graph.get_edge(*it).to == cur 
          && graph.get_edge(*it).capacity - graph.get_edge(*it).flow >= flow){
          graph.add_flow(*it, flow);
          break;
        }
      }
      cur = prev;
    }
  }
  return maxFlow;
}
//Main
FlowGraph read_data();
int main() {

    std::ios_base::sync_with_stdio(false);
    FlowGraph graph = read_data();

    std::cout << edmondsKarp(graph, 0, graph.size() - 1) << "\n";
    return 0;
}
//Util Functions
FlowGraph read_data() {

  int vertex_count, edge_count;
  std::cin >> vertex_count >> edge_count;
  FlowGraph graph(vertex_count);
  
  for (int i = 0; i < edge_count; ++i) {

    int u, v, capacity;
    std::cin >> u >> v >> capacity;

    graph.add_edge(u - 1, v - 1, capacity);
  }
  return graph;
}
