#include <iostream>
#include <string>

//Result: 31 ms	3500 KB
int main(){

    int n;
    std::cin >> n;
    std::string inp[n];

    int i = 0;
    while(i < n)
        std::cin >> inp[i++];
        
    i = 0;
    while(i < n){
        int len = inp[i].length();
        if(len > 10)
            std::cout << inp[i][0] << len - 2 << inp[i][len - 1];
        else
            std::cout << inp[i];
        std::cout << "\n";
        i++;
    }
    getchar();
    return 0;
}