#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using std::vector;
using std::string;

//Result: Max time used: 0.01/10.00, max memory used: 9519104/536870912
//Note: This code is not correct in every case (e.g. top and down of nodes 5, 10, 15 are all blues)
//5 x 5 squares
/*
0  1  2  3  4
5  6  7  8  9
10 11 12 13 14
15 16 17 18 19
20 21 22 23 24
*/
#define numPieces 25

struct piece{
  string up;
  string left;
  string down;
  string right;
  piece(string a, string b, string c, string d): up(a),left(b),down(c),right(d) {}
};
//--------------------------------------------------
vector<piece> getPieces();;
vector<piece*> arrangePieces(vector<piece> &pieces);
string pieceToString(piece* &piece);

int main() {

  std::ios_base::sync_with_stdio(false);

  vector<piece> pieces = getPieces();
  
  vector<piece*> newPieces = arrangePieces(pieces);

  int edgeLen = (int) pow(numPieces, 0.5);
  for(int i = 0; i < numPieces; i++){
    std::cout << pieceToString(newPieces[i]);
    if((i+1) % edgeLen == 0)
      std::cout << "\n";
    else
      std::cout << ";";
  }
  return 0;
}
//--------------------------------------------------
//Arrange blocks so that all common edge has the same color
vector<piece*> arrangePieces(vector<piece> &pieces){

  vector<piece*> newPieces(numPieces);
  vector<int> unused_ver1;
  for(int i = 0; i < numPieces; i++){
    //pos 0, 4, 20, 24
    if(pieces[i].up == "black" && pieces[i].left == "black")
      newPieces[0] = &pieces[i];
    else if(pieces[i].up == "black" && pieces[i].right == "black")
      newPieces[4] = &pieces[i];
    else if(pieces[i].left == "black" && pieces[i].down == "black")
      newPieces[20] = &pieces[i];
    else if(pieces[i].down == "black" && pieces[i].right == "black")
      newPieces[24] = &pieces[i];
    else
      unused_ver1.push_back(i);
  }
  vector<int> unused_ver2;
  for(int i: unused_ver1){
    //pos 1, 2, 3
    if(pieces[i].up == "black"){
      if(newPieces[1] == nullptr && pieces[i].left == newPieces[0]->right)
        newPieces[1] = &pieces[i];
      else if(newPieces[3] == nullptr && pieces[i].right == newPieces[4]->left)
        newPieces[3] = &pieces[i];
      else
        newPieces[2] = &pieces[i];
    }
    //pos 5, 10, 15
    else if(pieces[i].left == "black"){
      if(newPieces[5] == nullptr && pieces[i].up == newPieces[0]->down)
        newPieces[5] = &pieces[i];
      else if(newPieces[15] == nullptr && pieces[i].down == newPieces[20]->up)
        newPieces[15] = &pieces[i];
      else
        newPieces[10] = &pieces[i];
    }
    //pos 21, 22, 23
    else if(pieces[i].down == "black"){
      if(newPieces[21] == nullptr && pieces[i].left == newPieces[20]->right)
        newPieces[21] = &pieces[i];
      else if(newPieces[23] == nullptr && pieces[i].right == newPieces[24]->left)
        newPieces[23] = &pieces[i];
      else
        newPieces[22] = &pieces[i];
    }
    //pos 9, 14, 19
    else if(pieces[i].right == "black"){
      if(newPieces[9] == nullptr && pieces[i].up == newPieces[4]->down)
        newPieces[9] = &pieces[i];
      else if(newPieces[19] == nullptr && pieces[i].down == newPieces[24]->up)
        newPieces[19] = &pieces[i];
      else
        newPieces[14] = &pieces[i];
    }
    else
      unused_ver2.push_back(i);
  }
  vector<int> unused_ver3;
  for(int i: unused_ver2){
    //pos 6, 8, 16, 18
    if(newPieces[6] == nullptr && pieces[i].up == newPieces[1]->down && pieces[i].left == newPieces[5]->right)
      newPieces[6] = &pieces[i];
    else if(newPieces[8] == nullptr && pieces[i].up == newPieces[3]->down && pieces[i].right == newPieces[9]->left)
      newPieces[8] = &pieces[i];
    else if(newPieces[16] == nullptr && pieces[i].left == newPieces[15]->right && pieces[i].down == newPieces[21]->up)
      newPieces[16] = &pieces[i];
    else if(newPieces[18] == nullptr && pieces[i].down == newPieces[23]->up && pieces[i].right == newPieces[19]->left)
      newPieces[18] = &pieces[i];
    else
      unused_ver3.push_back(i);
  }
  for(int i: unused_ver3){
    //pos 7, 11, 12, 13, 17
    if(newPieces[7] == nullptr && pieces[i].up == newPieces[2]->down && pieces[i].left == newPieces[6]->right && pieces[i].right == newPieces[8]->left)
      newPieces[7] = &pieces[i];
    else if(newPieces[11] == nullptr && pieces[i].up == newPieces[6]->down && pieces[i].left == newPieces[10]->right && pieces[i].down == newPieces[16]->up)
      newPieces[11] = &pieces[i];
    else if(newPieces[13] == nullptr && pieces[i].up == newPieces[8]->down && pieces[i].down == newPieces[18]->up && pieces[i].right == newPieces[14]->left)
      newPieces[13] = &pieces[i];
    else if(newPieces[17] == nullptr && pieces[i].left == newPieces[16]->right && pieces[i].down == newPieces[22]->up && pieces[i].right == newPieces[18]->left)
      newPieces[17] = &pieces[i];
    else
      newPieces[12] = &pieces[i];
  }
  return newPieces;
}
//Conversion
piece stringToPiece(const string &s){

  int ind[5];
  int k = 0;
  for(int i = 0; i < s.length(); i++)
    if(s[i] == '(' || s[i] == ',' || s[i] == ')')
      ind[k++] = i;

  return piece(s.substr(ind[0]+1, ind[1]-ind[0]-1), s.substr(ind[1]+1, ind[2]-ind[1]-1), 
    s.substr(ind[2]+1, ind[3]-ind[2]-1), s.substr(ind[3]+1, ind[4]-ind[3]-1));
}
//Conversion
string pieceToString(piece* &piece){
  return "(" + piece->up + "," + piece->left + "," + piece->down + "," + piece->right + ")";
}
//Read input
vector<piece> getPieces(){

  vector<piece> pieces;
  for(int i = 0; i < numPieces; i++){
    string s;
    std::cin >> s;
    pieces.push_back(stringToPiece(s));
  }
  return pieces;
}
