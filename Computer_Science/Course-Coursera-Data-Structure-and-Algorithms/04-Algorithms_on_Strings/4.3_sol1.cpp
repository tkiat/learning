#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::string;
using std::vector;

//Result: Max time used: 0.28/1.00, max memory used: 77656064/536870912
//Compute prefix for Knuth Morris Pratt alg.
void KMP_prefix(string S, int s[]){

  int S_len = S.length();

  s[0] = 0;
  int border = 0;
  
  for(int i = 1; i < S_len; i++){

    while(border > 0 && S[i] != S[border])
      border = s[border - 1];

    if(S[i] == S[border])
      border++;
    else
      border = 0;
    s[i] = border;
  }
}
//Knuth Morris Pratt alg.
//Exploit prefix/suffix matches between pattern and text
vector<int> KMP(const string& pattern, const string& text){

  vector<int> result;

  int p_len = pattern.length();
  int t_len = text.length();

  string S = pattern + '$' + text;
  int S_len = p_len + 1 + t_len;
  int s[S_len];

  KMP_prefix(S, s);

  for(int i = p_len + 1; i < S_len; i++){
    if(s[i] == p_len)
      result.push_back(i - 2 * p_len);
  }
  return result;
}

int main() {

  string pattern, text;

  cin >> pattern;
  cin >> text;
  vector<int> result = KMP(pattern, text);

  for (int i = 0; i < result.size(); ++i) {
    printf("%d ", result[i]);
  }
  printf("\n");
  return 0;
}
