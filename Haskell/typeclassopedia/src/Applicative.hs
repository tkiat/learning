{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE InstanceSigs #-}

module Applicative where

class Functor f => Applicative' f where
  pure' :: a -> f a
  infixl 4 <@>
  (<@>) :: f (a -> b) -> f a -> f b

-------------------------------------------------------------------------------
-- Instances

-- ex1

instance Applicative' Maybe where
  pure' :: a -> Maybe a
  pure' a = Just a

  (<@>) :: Maybe (a -> b) -> Maybe a -> Maybe b
  (Just g) <@> (Just a) = Just (g a)
  _ <@> Nothing = Nothing
  Nothing <@> _ = Nothing

myMaybe1 :: Maybe Int
myMaybe1 = Just (+ 1) <@> Just 1

myMaybe2 :: Maybe Int
myMaybe2 = Just (+ 1) <@> Nothing

-- ex2

newtype ZipList' a = ZipList' {getZipList' :: [a]} deriving (Show)

instance Functor ZipList' where
  fmap g (ZipList' a) = ZipList' (fmap g a)

instance Applicative' ZipList' where
  pure' :: a -> ZipList' a
  pure' a = ZipList' (repeat a) -- exercise

  (<@>) :: ZipList' (a -> b) -> ZipList' a -> ZipList' b
  (ZipList' gs) <@> (ZipList' xs) = ZipList' (zipWith ($) gs xs)

myZipList :: ZipList' Int
myZipList = pure' (+ 1) <@> ZipList' [4, 5, 6]

-------------------------------------------------------------------------------
-- Utility functions

-- ex1
-- this should be equivalent to converting Just <$> [5,6] to Just [5,6]
sequenceAL :: Applicative f => [f a] -> f [a]
sequenceAL (x : xs) = (:) <$> x <*> sequenceAL xs
sequenceAL [] = pure []

mySequenceAL :: Maybe [Int]
mySequenceAL = sequenceAL [Just 1, Just 2, Just 3]

-------------------------------------------------------------------------------
-- Alternative formulation

-- class Functor f => Applicative' f where
--   pure' :: a -> f a
--   infixl 4 <@>
--   (<@>) :: f (a -> b) -> f a -> f b
-- class Functor f => Monoidal f where
--   unit :: f ()
--   (**) :: f a -> f b -> f (a, b)

-- instance Monoidal Maybe where
--   unit = pure ()
-- TODO

-------------------------------------------------------------------------------

main :: IO ()
main = do
  putStr "myMaybe1: " >> print myMaybe1
  putStr "myMaybe2: " >> print myMaybe2
  putStr "myZipList: " >> print myZipList
  putStr "mySequenceAL: " >> print mySequenceAL
