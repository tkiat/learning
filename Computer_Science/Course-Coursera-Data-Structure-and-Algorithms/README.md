## [Coursera: Data Structures and Algorithms Specialization](https://www.coursera.org/specializations/data-structures-algorithms)

### This repository includes almost assignments from 6 courses (not all are mandatory). Unfortunately I cannot upload question paper as it may violate copyrights. I used C++ for all assignments except the final one for which I used Python.

Below is the list of courses and assignment topics.

1. Algorithmic Toolbox - [Course Link](https://www.coursera.org/learn/algorithmic-toolbox?specialization=data-structure-and-algorithm)
    * 1.1 Sum and Max Pairwise Product
    * 1.2 Algorithmic Warm-up
    * 1.3 Greedy Algorithms
    * 1.4 Divide-and-Conquer
    * 1.5 Dynamic Programming 1
    * 1.6 Dynamic Programming 2

2. Data Structures - [Course Link](https://www.coursera.org/learn/data-structures?specialization=data-structure-and-algorithm)
    * 2.1 Basic Data Structures
    * 2.2 Priority Queues and Disjoint Sets
    * 2.3 Hash Tables and Hash Functions
    * 2.4 Binary Search Trees

3. Algorithms on Graphs - [Course Link](https://www.coursera.org/learn/algorithms-on-graphs?specialization=data-structure-and-algorithm)
    * 3.1 Decomposition of Graphs
    * 3.2 Decomposition of Graphs
    * 3.3 Paths in Graphs
    * 3.4 Paths in Graphs
    * 3.5 Minimum Spanning Trees
    * 3.6 Advanced Shortest Paths

4. Algorithms on Strings - [Course Link](https://www.coursera.org/learn/algorithms-on-strings?specialization=data-structure-and-algorithm)
    * 4.1 Suffix Trees
    * 4.2 Burrows–Wheeler Transform and Suffix Arrays
    * 4.3 Algorithmic Challenges

5. Advanced Algorithms and Complexity - [Course Link](https://www.coursera.org/learn/advanced-algorithms-and-complexity)
    * 5.1 Flows in Networks
    * 5.2 Linear Programming
    * 5.3 NP-completeness
    * 5.4 Coping with NP-completeness
    
6. Genome Assembly Programming Challenge - [Course Link](https://www.coursera.org/learn/assembling-genomes)
    * 6.1 Assembling phi X174 Using Overlap Graphs
    * 6.2 Assembling Genomes Using de Bruijn Graphs
    * 6.3 Genome Assembly Faces Real Sequencing Data
