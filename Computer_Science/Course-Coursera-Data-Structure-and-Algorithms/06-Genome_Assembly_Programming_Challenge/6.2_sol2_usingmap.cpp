#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stack>
#include <unordered_map>

using std::vector;
using std::string;
//Result: Max time used: 0.08/1.00, max memory used: 53637120/536870912
//--------------------------------------------------
std::unordered_multimap<int,int> getDirectedGraph(int numEdges);
bool isBalancedGraph(std::unordered_multimap<int,int> &graph);
vector<int> hierholzer(std::unordered_multimap<int,int> graph);

int main() {

  std::ios_base::sync_with_stdio(false);
  //get input
  int V, E;
  std::cin >> V >> E;
  std::unordered_multimap<int,int> graph = getDirectedGraph(E);
  
  //check if balanced
  if(!isBalancedGraph(graph)){
    std::cout << "0";
    return 0;
  }
  else 
    std::cout << "1\n";
  //find path
  vector<int> path = hierholzer(graph);
  //print result
  for(auto it = path.begin(); it != path.end() - 1; it++)
    std::cout << *it + 1 << " ";

  return 0;
}
//--------------------------------------------------
//Hierholzer algorihtm: find Eulerian path
vector<int> hierholzer(std::unordered_multimap<int,int> graph){

  vector<int> path;
  if(graph.size() == 0)
    return vector<int>();
    
  //outdegree of each node
  std::unordered_map<int,int> outdegree;
  for(std::pair<int,int> e : graph)
    outdegree[e.first] = graph.count(e.first);

  std::stack<int> st; //store travered nodes
  
  //Hierholzer Step 1: Choose any vertex and begin traversing
  int u = graph.begin()->first;
  st.push(u);
  
  while (!st.empty()){
    //Hierholzer Step 2: keep traversing while there is still unexplored edge
    if (outdegree[u] > 0){
      st.push(u);
        //return pair iterator: first/second = lower/upper bound of the range
      auto ret = graph.equal_range(u);
      int next = ret.first->second;
      graph.erase(ret.first);
      outdegree[u]--; //remove explored edge
      u = next;
    }
    //Hierholzer Step 2: backtrack if there is no unexplored edge
    else{
      path.push_back(u);
      u = st.top();
      st.pop();
    }
  }
  //Hierholzer Step 3: reverse path
  std::reverse(path.begin(), path.end());
  
  return path;
}
//A graph is balanced if indegree = outdegree for each node
bool isBalancedGraph(std::unordered_multimap<int,int> &graph){

  std::unordered_map<int,int> outdegree;
  std::unordered_map<int,int> indegree;

  for(std::pair<int,int> e : graph){
    outdegree[e.first]++;
    indegree[e.second]++;
  }
  for(auto e : graph)
    if(indegree[e.first] != outdegree[e.first])
      return false;
  return true;
}
//avoid inserting duplicate edge
bool isDuplicate(std::unordered_multimap<int,int> &multimap, std::pair<int,int> pair){

  auto val = multimap.find(pair.first);
  int num = multimap.count(pair.first);
  while(num--)
    if((val++)->second == pair.second)
      return true;
  return false;
}
//read input
std::unordered_multimap<int,int> getDirectedGraph(int numEdges){

  std::unordered_multimap<int,int> adj;

  for(int i = 0; i < numEdges; i++){
    int u, v;
    std::cin >> u >> v;
    //if(!isDuplicate(adj, std::pair<int,int>(u-1, v-1)))
    adj.insert(std::make_pair(u-1, v-1));
  }
  return adj;
}