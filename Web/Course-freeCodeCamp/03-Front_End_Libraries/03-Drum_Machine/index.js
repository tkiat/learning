console.clear();
//buttonText:[src, description]
const preset = {
  Q: ["https://raw.githubusercontent.com/theerawatK/raw-data/master/audio/drums/bassdrum01.mp3", "bassdrum01"],
  W: ["https://raw.githubusercontent.com/theerawatK/raw-data/master/audio/drums/bassdrum04.mp3", "bassdrum04"],
  E: ["https://raw.githubusercontent.com/theerawatK/raw-data/master/audio/drums/bassdrum_acoustic01.mp3", "bassdrum_acoustic01"],
  A: ["https://raw.githubusercontent.com/theerawatK/raw-data/master/audio/drums/clap03.mp3", "clap03"],
  S: ["https://raw.githubusercontent.com/theerawatK/raw-data/master/audio/drums/clap04.mp3", "clap04"],
  D: ["https://raw.githubusercontent.com/theerawatK/raw-data/master/audio/drums/clav01.mp3", "clav01"],
  Z: ["https://raw.githubusercontent.com/theerawatK/raw-data/master/audio/drums/hihat_closed01.mp3", "hihat_closed01"],
  X: ["https://raw.githubusercontent.com/theerawatK/raw-data/master/audio/drums/kick_distorted01.mp3", "kick_distorted01"],
  C: ["https://raw.githubusercontent.com/theerawatK/raw-data/master/audio/drums/kick_hard01.mp3", "kick_hard01"],
};
const caption = {
  Q: "This is Q",
  W: "This is w",
  E: "eee",
  A: "aaa",
  S: "ss",
  D: "dd",
  Z: "z",
  X: "x",
  C: "ccc",
};
//classes
class DrumMachine extends React.Component {
  //constructor
  constructor(props){
    super(props);
    this.trigger = this.trigger.bind(this);
    this.changeVolume = this.changeVolume.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }
  //helper functions
  trigger(j){
    let btn = event.target.dataset.sound ? event.target.dataset.sound : j;
    //console.log(event.target.dataset.sound && "test")
    document.getElementById("caption").innerHTML = preset[btn][1];
    let piece = document.getElementById(btn);
    piece.volume = document.getElementById("sliderMain").value;
    piece.play();
  }
  changeVolume(){
    let elems = document.getElementsByTagName("audio");
    for(let elem of elems){
      elem.volume = event.target.value;
    }
  }
  createDrumPads(){
    let drumPads = [];
    for(let [soundName, src] of Object.entries(preset)){
      drumPads.push(
        <div key={soundName} id={soundName + "Pad"} data-sound={soundName} className="drum-pad" onClick={this.trigger}>
          <audio className="clip" id={soundName} preload="auto" src={src[0]}>
            <code>audio</code> element not supported.
          </audio>
          {soundName}
        </div>
      )
    }
    return drumPads;
  }
  handleKeyDown(e){
    let key = String.fromCharCode(e.keyCode);
    if(Object.keys(preset).includes(key)){
      this.trigger(key)
    }
  }
  //lifecycles
  componentDidMount(){
    document.addEventListener("keydown", this.handleKeyDown);  
  }
  //render
  render() {
    return (
      <div id="drum-machine">
        <div id="display">
          <div className="drum-pad-container">
            {this.createDrumPads()}
          </div>
          <div id="caption"></div>
        </div>
        <div className="slider-container">
          <input id="sliderMain" type="range" onInput={this.changeVolume} 
            min="0" max="1" step="0.05" defaultValue="0.5"/>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<DrumMachine />, document.getElementById('root'));  