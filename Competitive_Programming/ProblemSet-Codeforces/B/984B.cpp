#include <iostream>
#include <vector>
#include <algorithm>

//Result: 30 ms	3400 KB
int main(){

    std::ios::sync_with_stdio(false);

    int n, m;
    std::cin >> n >> m;

    char mines[n][m];

    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++){
            std::cin >> mines[i][j];
        }
    }
    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++){
            //skip check for mines
            if(mines[i][j] == '*')
                continue;
            int count = 0;
            //clockwise dir
            if(i-1 >= 0 && j-1 >= 0 && mines[i-1][j-1] == '*')
                count++;
            if(j-1 >= 0 && mines[i][j-1] == '*')
                count++;
            if(i+1 < n && j-1 >= 0 && mines[i+1][j-1] == '*')
                count++;
            if(i+1 < n && mines[i+1][j] == '*')
                count++;
            if(i+1 < n && j+1 < m && mines[i+1][j+1] == '*')
                count++;
            if(j+1 < m && mines[i][j+1] == '*')
                count++;
            if(i-1 >= 0 && j+1 < m && mines[i-1][j+1] == '*')
                count++;
            if(i-1 >= 0 && mines[i-1][j] == '*')
                count++;
            //validity check
            if((mines[i][j] != '.' && mines[i][j] != '0' + count) || (mines[i][j] == '.' && count > 0)){
                std::cout << "NO";
                return 0;
            }
        }
    }
    std::cout << "YES";

    return 0;
}
/*
Example
---1---
3 3
111
1*1
111
YES
---2---
2 4
*.*.
1211
NO
*/