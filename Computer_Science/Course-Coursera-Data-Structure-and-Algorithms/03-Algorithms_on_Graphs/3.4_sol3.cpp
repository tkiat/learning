#include <iostream>
#include <limits>
#include <climits>
#include <vector>
#include <queue>

using std::vector;
using std::queue;
using std::pair;
using std::priority_queue;
//Result: Max time used: 0.22/2.00, max memory used: 12193792/536870912
vector<int> bfs(vector<vector<int> > &a, int b);
vector<long long> bellmanFord(vector<vector<int> > &adj, vector<vector<int> > &cost, int src) {

  int n = adj.size();
  vector<long long> dist_update(n, LLONG_MAX);
  dist_update[src] = 0;
  //|V-1| iterations
  for(int i = 0; i < n - 1; i++){
    for(int u = 0; u < n; u++){
      int u_col = 0;
      for(auto v = adj[u].begin(); v != adj[u].end(); v++){
        if(dist_update[u] != LLONG_MAX && dist_update[u] + cost[u][u_col] < dist_update[*v]){
          dist_update[*v] = dist_update[u] + cost[u][u_col];
        }
        u_col++;
      }
    }
  }
  //store at least 1 vertice in every negative cycle in vertex_neg_cycle
  vector<int> vertex_neg_cycle;
  for(int u = 0; u < n; u++){
    int u_col = 0;
    for(auto v = adj[u].begin(); v != adj[u].end(); v++){
      if(dist_update[u] != LLONG_MAX && dist_update[u] + cost[u][u_col] < dist_update[*v]){
        vertex_neg_cycle.push_back(u);
      }
      u_col++;
    }
  }
  //display "-" for all reachable nodes from all vertices in vertex_neg_cycle
  for(auto u = vertex_neg_cycle.begin(); u != vertex_neg_cycle.end(); u++){
    vector<int> reachable = bfs(adj, *u);
    for(auto v = reachable.begin(); v != reachable.end(); v++){
      dist_update[*v] = LLONG_MIN; //will display "-"
    }
  }
  return dist_update;
}
//return all reachable vertices from src
vector<int> bfs(vector<vector<int> > &adj, int src) {

  vector<int> reachable;
  reachable.push_back(src);

  queue<int> q;
  q.push(src);

  int n = adj.size();
  bool visited[n];
  std::fill_n(visited, n, false);
  visited[src] = true;

  while(!q.empty()){
    int u = q.front();
    q.pop();
    for(auto v = adj[u].begin(); v != adj[u].end(); v++){

      if(visited[*v] == false){
        visited[*v] = true;
        q.push(*v);
        reachable.push_back(*v);
      }
    }
  }
  return reachable;
}

int main() {
  //fetch data
  int n, m, s;
  std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  vector<vector<int> > cost(n, vector<int>());
  for (int i = 0; i < m; i++) {
    int x, y, w;
    std::cin >> x >> y >> w;
    adj[x - 1].push_back(y - 1);
    cost[x - 1].push_back(w);
  }
  std::cin >> s;
  s--;
  //display result
  vector<long long> dist = bellmanFord(adj, cost, s);
  for(auto it = dist.begin(); it != dist.end(); it++){
    if(*it == LLONG_MAX)
      std::cout << "*\n";
    else if(*it == LLONG_MIN)
      std::cout << "-\n";
    else
      std::cout << *it << "\n";
  }
}