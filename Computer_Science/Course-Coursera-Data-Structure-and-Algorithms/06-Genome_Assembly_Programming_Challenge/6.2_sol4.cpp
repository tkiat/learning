#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stack>
#include <unordered_map>

using std::vector;
using std::string;
using std::unordered_map;

#define k_mer_count 5396 //default 5396
#define k_mer_length 10
//Result: Max time used: 0.03/3.00, max memory used: 14512128/536870912
//--------------------------------------------------
//IMPROVEMENT: can use vector pair to save memory
vector<vector<int>> getDirectedGraph();
bool isBalancedGraph(vector<vector<int>> &adj);
vector<int> hierholzer(vector< vector<int> > adj);

int main() {

  std::ios_base::sync_with_stdio(false);
  //convert input string to numeric-value nodes
  vector<vector<int>> graph = getDirectedGraph();
  //find path
  vector<int> path = hierholzer(graph);
  //print result
  char nucleotide[4] = {'A', 'T', 'C', 'G'};
  for(auto it = path.begin(); it != path.end() - 1; it++)
    std::cout << nucleotide[*it % 4];

  return 0;
}
//--------------------------------------------------
//Hierholzer algorihtm: find Eulerian path
vector<int> hierholzer(vector<vector<int> > adj){

  int n = adj.size();
  if (adj.size() == 0)
    return vector<int>();
  //outdegree of each node
  unordered_map<int,int> outdegree;
  for (int i = 0; i < adj.size(); i++)
    if(adj[i].size() > 0)
      outdegree[i] = adj[i].size();

  std::stack<int> st; //store travered nodes
  vector<int> path;
  //Hierholzer Step 1: Choose any vertex and begin traversing
  int u = outdegree.begin()->first;
  st.push(u);
  while (!st.empty()){
    //Hierholzer Step 2: keep traversing while there is still unexplored edge
    if (outdegree[u] > 0){
      st.push(u);
      int next = adj[u].back();
      adj[u].pop_back();
      outdegree[u]--; //remove explored edge
      u = next;
    }
    //Hierholzer Step 2: backtrack if there is no unexplored edge
    else{
      path.push_back(u);
      u = st.top();
      st.pop();
    }
  }
  //Hierholzer Step 3: reverse path
  std::reverse(path.begin(), path.end());
  return path;
}

int stringToNum(const string& str){

  int num = 0, mul = 1;
  unordered_map<char, int> nucleotide = {{'A', 0}, {'T', 1}, {'C', 2}, {'G', 3}};

  for(int i = str.length() - 1; i >= 0; i--){
    num += nucleotide[str[i]] * mul;
    mul *= 4;
  }
  return num;
}
//Read input
vector<vector<int>> getDirectedGraph(){

  int n = pow(4, k_mer_length - 1);//n = # possible values of 10-nucleotide long
  vector<vector<int>> adj(n);
  for(int i = 0; i < k_mer_count; i++){
    string k_mer;
    std::cin >> k_mer;

    string u = k_mer.substr(0, k_mer_length-1);
    string v = k_mer.substr(1, k_mer_length-1);
    adj[stringToNum(u)].push_back(stringToNum(v));
  }
  return adj;
}
