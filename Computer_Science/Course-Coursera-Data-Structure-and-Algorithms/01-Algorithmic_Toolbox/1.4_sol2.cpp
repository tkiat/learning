#include <algorithm>
#include <iostream>
#include <vector>

using std::vector;

//Result: Max time used: 0.05/1.00, max memory used: 22228992/536870912
//keep dividing array until size two if each two elements are same they are 
//majority and returns that value, if they are not same return special character
//then move up and find majority using two steps below
int get_majority_element(vector<int> &a, int left, int right) {
  if (left == right) //contain 1 element
    return a[left];
  if (left + 1 == right){ //contain 2 elements
    if(a[left] == a[right])
      return a[left];
    else
      return -1; //no majority
  } 
  //step 1: return element if it is majority of both halves, no need to manually check
  int mid = left + (right-left)/2;

  int element_left = get_majority_element(a, left, mid);
  int element_right = get_majority_element(a, mid + 1, right);
  if(element_left == element_right){
    return element_left;
  }
  
  //step 2: if not, perform manual check and return the true majority (-1 if not found)
  int element_left_count = 0;
  int element_right_count = 0;
  for (size_t i = left; i <= right; i++) {
    if(a[i] == element_left)
      element_left_count++;
    if(a[i] == element_right)
      element_right_count++;
  }

  if(element_left_count > (right-left+1)/2){
    return element_left;
  }
  else if(element_right_count > (right-left+1)/2){
    return element_right;
  }
  else{
    return -1;
  }

}

int main() {
  int n;
  std::cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); ++i) {
    std::cin >> a[i];
  }
  std::cout << (get_majority_element(a, 0, a.size()-1) != -1) << '\n';
}
