#include <iostream>
#include <vector>
using std::vector;

//Result: Max time used: 0.01/1.00, max memory used: 9609216/536870912
int fibonacci_sum(long long n) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;
    long long sum      = 1;

    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = (tmp_previous + current) %10;
        sum += current % 10;

        if(previous == 0 && current == 1){
            int period = i + 1;
            i += ((n - i -2)/period) * period;
        }
    }
    return sum % 10;
}
long long get_fibonacci_partial_sum(long long from, long long to) {

   if(from - 1 < 0)
        from = 1;
   return (fibonacci_sum(to) - fibonacci_sum(from-1) + 10) % 10;
}

long long get_fibonacci_partial_sum_naive(long long from, long long to) {
    long long sum = 0;

    long long current = 0;
    long long next  = 1;

    for (long long i = 0; i <= to; ++i) {
        if (i >= from) {
            sum += current;
        }

        long long new_current = next;
        next = next + current;
        current = new_current;
    }

    return sum % 10;
}

int main() {
    long long from, to;
    std::cin >> from >> to;
    std::cout << get_fibonacci_partial_sum(from, to) << '\n';
}
