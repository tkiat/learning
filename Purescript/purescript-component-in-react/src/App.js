// App.js
import React from 'react'
// import Counter from './components/Counter'
// import { jsCounter as Counter } from './MyApp/Components/Counter.purs'
import { jsCounter as Counter } from './MyApp/Components/Counter/Interop.purs'
import './App.css'

const App = () => {
  return (
    <div className="App">
      {/*<Counter label="Click me!" />*/}
      <Counter onClick={(n) => console.log('clicked: ', n)} />
      <Counter counterType="decrementer" label="Click me!" />
      {/*<Counter />*/}
    </div>
  )
}

export default App
