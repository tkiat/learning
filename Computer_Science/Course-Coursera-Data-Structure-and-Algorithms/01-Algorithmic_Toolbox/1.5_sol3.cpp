#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;

//Result: Max time used: 0.00/1.00, max memory used: 9662464/536870912
//optional function: reconstruct alignment
/*
string reconstr_txt1 = "";
string reconstr_txt2 = "";
void reconstr_alignment(vector<vector<int>> D, const string &str1, const string &str2, int i, int j){
  
  if(i == 0 && j == 0)
    return;
  
  if(i > 0 && D[i][j] == D[i-1][j] + 1){
    reconstr_alignment(D, str1, str2, i - 1, j);

    reconstr_txt1 += str1[i - 1];
    reconstr_txt2 += "-";
  }
  else if(j > 0 && D[i][j] == D[i][j-1] + 1){
    reconstr_alignment(D, str1, str2, i, j - 1);

    reconstr_txt1 += "-";
    reconstr_txt2 += str2[j-1];
  }
  else{
    reconstr_alignment(D, str1, str2, i - 1, j - 1);

    reconstr_txt1 += str1[i-1];
    reconstr_txt2 += str2[j-1];
  }
}
*/
int edit_distance(const string &str1, const string &str2) {

  int n = str1.length();
  int m = str2.length();

  vector<vector<int>> D(n+1);
  for(int i = 0; i <= n; i++)
    D[i].resize(m+1);

  for(int i = 0; i <= n; i++)
    D[i][0] = i;
  for(int i = 0; i <= m; i++)
    D[0][i] = i;
  
  for(int j = 1; j <= m; j++){
    for(int i = 1; i <= n; i++){

      //find min among four choices: insertion, deletion, match, mismatch
      int insertion = D[i][j-1] + 1;
      int deletion = D[i-1][j] + 1;
      int min = (insertion > deletion) ? deletion : insertion;

      if(str1[i-1] == str2[j-1]){

        int match = D[i-1][j-1];
        if(match < min)
          min = match;
      }
      else{

        int mismatch = D[i-1][j-1] + 1;
        if(mismatch < min)
          min = mismatch;
      }
      D[i][j] = min;
    }
  }
  /*
  reconstr_alignment(D, str1, str2, n, m);
  std::cout << reconstr_txt1 << "\n";
  std::cout << reconstr_txt2 << "\n";
  */
  /*
  for(int i = 0; i <= n; i++){
    for(int j = 0; j <= m; j++){
      std::cout << D[i][j] << " ";
    }
    std::cout << std::endl;
  }
  */
  return D[n][m];
}

int main() {
  string str1;
  string str2;
  std::cin >> str1 >> str2;
  std::cout << edit_distance(str1, str2) << std::endl;
  return 0;
}
