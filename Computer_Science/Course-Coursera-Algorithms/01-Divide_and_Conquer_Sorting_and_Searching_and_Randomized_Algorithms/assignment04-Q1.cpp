//Ans: 17 this code is not good (to improve)

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <time.h>

using namespace std;

void display2DVector(vector<vector<int>> vektor) {
	int vektor_size = vektor.size();
	for (int i = 1; i < vektor_size; i++) {
		for (auto it = vektor[i].begin(); it != vektor[i].end(); it++) {
			cout << *it << " ";
		}
		cout << endl;
	}
}
void display1DVector(vector<int> vektor) {
	int vektor_size = vektor.size();
	for (auto it = vektor.begin(); it != vektor.end(); it++) {
		cout << *it << " ";
	}
	cout << endl;
}
vector<int> randomSeedGenerator(int length) {
	//generate seed array
	vector<int> seed(length + 1);
	for (int i = 0; i < length; i++) {
		seed[i] = i + 1;
	}
	srand(time(0));
	random_shuffle(&seed[0], &seed[length]);
	seed.pop_back(); //since random_shuffle does not include last
	//cout << "Original Seed: ";for (int i = 0; i < seed.size(); i++) cout << seed[i] << " ";cout << endl;
	return seed;
}
int main() {

	//Get a[a vertice] = {set of all connected vertices to this vertice}
	const int numVertice = 200;
	string line;
	vector<vector<int>> a(numVertice + 1); //a[1] = vertice 1
	int m, n;

	ifstream myfile("text.txt");

	if (myfile.is_open()) {

		while (getline(myfile, line)) {
			size_t pos = 0;
			string token;
			if ((pos = line.find('\t')) != string::npos) {
				m = stoi(line.substr(0, pos));
				line.erase(0, pos + 1);
			}
			while ((pos = line.find('\t')) != string::npos) {
				n = stoi(line.substr(0, pos));
				a[m].push_back(n);
				a[n].push_back(m);	//important!
				line.erase(0, pos + 1); // 1 = delimiter length	
			}
		}
		myfile.close();
	}
	else {
		cout << "Unable to open file\n";
	}

	//display2DVector(a);
	vector <int> vertices;
	vertices = randomSeedGenerator(numVertice);

	//Karger Contraction Algorithm
	int u;
	while (vertices.size() > 2) {
		//select randomly different u and v vertices in seed, merge them and delete v
		auto v = vertices.end() - 1;
		u = vertices[rand() % (vertices.size() - 1)];
		//int c;
		//cin >> u >> c;
		//auto v = &c;

				//must select remaining edge u-v
		while (1) {
			auto p1 = find(a[*v].begin(), a[*v].end(), u);
			auto p2 = find(a[u].begin(), a[u].end(), *v);
			if (p1 != a[*v].end() || p2 != a[u].end()) { //if found
				break;
			}
			else {
//cout << "Edge: " << u << "-" << *v << " not found. Select new one!\n";
//cout << "a[" << *v << "]: ";
//display1DVector(a[*v]);
			u = vertices[rand() % (vertices.size() - 1)];
//std::chrono::seconds dura(1);
//std::this_thread::sleep_for(dura);
			}
		}

		//cout << "a[" << u << "]: ";
		//display1DVector(a[u]);

		//cout << "a[" << *v << "]: ";
		//display1DVector(a[*v]);

		a[u].insert(a[u].end(), a[*v].begin(), a[*v].end());

		//cout << "a[" << u << "] after insert: ";
		//display1DVector(a[u]);

				//replace removed vertice to new vertice
		for (int i = 0; i < vertices.size(); i++) {
			replace(a[vertices[i]].begin(), a[vertices[i]].end(), *v, u);
		}

		//cout << "a[" << u << "] after replace: ";
		//display1DVector(a[u]);

				//remove self loop = remove all u destination nodes in u
		while (1) {
			auto p = find(a[u].begin(), a[u].end(), u);
			if (p != a[u].end()) {
				a[u].erase(p);
			}
			else {//not found
				break;
			}
		}

		//cout << "a[" << u << "] after remove self loop: ";
		//display1DVector(a[u]);

		vertices.pop_back();
	}

	int rest = vertices[0] == u ? vertices[1] : vertices[0]; //rest = another vertice left

//cout << "Final a[" << u << "]: ";
//display1DVector(a[u]);

//cout << "Final a[" << rest << "]: ";
//display1DVector(a[rest]);

	//if (a[u].size() < numCut)
	//int numCut = a[u].size();
	cout << "numCut: " << a[u].size() << " " << a[rest].size() << endl;

	getchar();
	getchar();
    return 0;
}
