#uses python2

from collections import deque
import sys
#Result: Max time used: 22.16/100.00, max memory used: 15757312/536870912
#----------------------------------------
#Main
def main():
  
  #get input
  input = list()
  k, t = map(int, sys.stdin.readline().split())
  try:
    while True:
      read = raw_input()
      if read != "":
        input.append(read)
      else:
        break
  except EOFError:
    pass
  #build graph
  graph = buildDirectedGraph(input, k)
  #brute force search each node for all bubbles
  numBubbles = 0
  for k, v in graph.items():
    if(len(filter(bool, v)) > 1):
      numBubbles += bfs(graph, k, t)
  #print result
  print(numBubbles)
#----------------------------------------
#Find all paths and count number of bubbles
def bfs_allPaths(graph, src, des, t):
  
  numBubbles = 0
  allPaths = list()

  path = list()
  path.append(src)
  q = deque()
  q.append(path)

  while q:
        
    path = q.popleft()
    if len(path) > t + 1: #avoid potential infinity loop
      continue
    last = path[-1]
    if last == des:
      allPaths.append(path[1:-1]) #except src and des
    elif last in graph:
      for new in graph[last]:
        if(new not in path): #if not visited
          newpath = list(path)
          newpath.append(new)
          q.append(newpath)
  #count number of bubbles
  if(len(allPaths) > 1):
    for i in range(len(allPaths)):
      for j in range(i+1, len(allPaths)):
        overlapped = True
        for temp in allPaths[i]:
          if temp in allPaths[j]:
            overlapped = False
            break
        if overlapped:
          numBubbles += 1

  return numBubbles
#Find all potential nodes for "bfs_allPaths" function
def bfs(graph, src, t):
  
  numBubbles = 0
  #1st BFS to find all nodes visited more than once from source
  #without potential overlapping path and dist from source > t
  nodes = dict()
  q = deque()
  q.append((src, 0))
  while q: #if not empty

    u = q.popleft()
    if u[0] in graph and u[1] <= t-1 and not(u[0] == src and u[1] > 0):
      for new in graph[u[0]]:
        q.append((new, u[1]+1))
    if not u[0] in nodes:
      nodes[u[0]] = 1
    else:
      nodes[u[0]] += 1
  #2nd BFS to find all paths from src to all nodes visited more than once
  #and return all possible bubbles
  for k,v in nodes.items():
    if(v > 1):
      numBubbles += bfs_allPaths(graph, src, k, t)

  return numBubbles
#Build graph using k_mer nodes
def buildDirectedGraph(input, k_mer):
    
  adj = dict()

  for read in input:
    j = 0
    while(j + k_mer <= len(read)):
      u = read[j : j+k_mer-1]
      v = read[j+1 : j+k_mer]
      if not(u in adj and v in adj[u]): #add u != v here can speed up a bit more
        adj.setdefault(u, []).append(v)
      j += 1
      
  return adj
#----------------------------------------
#Main
if __name__ == '__main__':
  main()

'''
//----------------------------------------
Testcase
3 4
AAABBBA
AABCCBA
AAABBCBA
Ans: 6
AA(self-loop) -> AB           ->       BC -> CC -> CB -> BA
                 AB -> BB(self-loop)-> BC   
                                       BC       -> CB
                       BB(self-loop)            ->       BA
The bubbles are
(AB -> BC) and (AB -> BB -> BC)
(AB -> BB -> BA) and (AB -> BC -> CB -> BA)
(AB -> BB -> BA) and (AB -> BC -> CC -> CB -> BA)
(BC -> CB) and (BC -> CC -> CB)
(BB -> BA) and (BB -> BC -> CB -> BA)
(BB -> BA) and (BB -> BC -> CC -> CB -> BA)
'''