#include <iostream>
#include <vector>

using std::vector;

//Result: Max time used: 0.07/1.00, max memory used: 10752000/536870912
vector<int> optimal_summands(int n) {
  vector<int> summands_list;
  //write your code here
  int summand = 1;
  while(n >= 2*summand+1){
    summands_list.push_back(summand);
    n -= summand;
    summand++;
  }
  if(n > 0){
    summands_list.push_back(n);
  }
  return summands_list;
}

int main() {
  int n;
  std::cin >> n;
  vector<int> summands = optimal_summands(n);
  std::cout << summands.size() << '\n';
  for (size_t i = 0; i < summands.size(); ++i) {
    std::cout << summands[i] << ' ';
  }
}
