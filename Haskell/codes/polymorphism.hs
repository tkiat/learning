{-# LANGUAGE TypeFamilies #-}

-- ref: https://stackoverflow.com/a/9546236
-- Hack: should be avoided
class Add r where
  add' :: (Integer -> Integer) -> r

instance Add Integer where
  add' k = k 0

instance (n ~ Integer, Add r) => Add (n -> r) where
  add' k m = add' (\n -> k (m + n))

add :: (Add r) => r
add = add' id

main :: IO ()
main = do
  print $ (add 1 2 :: Integer)
  print $ (add 1 2 3 4 :: Integer)
