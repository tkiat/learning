import Control.Monad.Cont

main = do
  putStr $ whatsYourName "h"

--   putStrLn "Example 1" >> runCont (calculateLength "123" >>= double) print
--   putStrLn "Example 2" >> putStr $ whatsYourName ""

calculateLength :: [a] -> Cont r Int
calculateLength l = return (length l)

double :: Int -> Cont r Int
double n = return (n * 2)

-- Returns a string depending on the length of the name parameter.
-- If the provided string is empty, returns an error.
-- Otherwise, returns a welcome message.
whatsYourName :: String -> String
whatsYourName name =
  (`runCont` id) $ do
    -- 1
    response <- callCC $ \exit -> do
      -- 2
      validateName name exit -- 3
      return $ "Welcome, " ++ name ++ "!" -- 4
    return response -- 5

validateName name exit = do
  when (null name) (exit "You forgot to tell me your name!")
