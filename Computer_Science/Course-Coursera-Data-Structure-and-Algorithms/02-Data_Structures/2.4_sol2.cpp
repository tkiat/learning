#include <algorithm>
#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

//Result: Max time used: 0.29/2.00, max memory used: 21639168/536870912
struct Node {
  long long key;
  int left;
  int right;

  Node() : key(0), left(-1), right(-1) {}
  Node(long long key_, int left_, int right_) : key(key_), left(left_), right(right_) {}
};
/*been given bad signal so gave up
vector<long long> inOrder(Node root, const vector<Node> &tree) {
  vector<long long> result;
  
  vector<long long> temp;

  if(root.left != -1)
    temp = inOrder(tree[root.left], tree);
  if(temp.size() > 0)
    result.insert(result.end(), temp.begin(), temp.end());
  //temp = std::vector<int>();
  vector<long long>().swap(temp); //free memory
  //std::cout << key[root] << " ";
  result.push_back(root.key);
  vector<long long> temp2;
  if(root.right != -1)
    temp2 = inOrder(tree[root.right], tree);
  if(temp2.size() > 0)
    result.insert(result.end(), temp2.begin(), temp2.end());
  //temp2 = std::vector<int>();
  vector<long long>().swap(temp2); //free memory
  return result;
}
*/
//my code
vector<long long> result;
void inOrder(Node root, const vector<Node> &tree) {

  if(root.left != -1)
    inOrder(tree[root.left], tree);
  result.push_back(root.key);
  if(root.right != -1)
    inOrder(tree[root.right], tree);
}
bool IsBinarySearchTree(const vector<Node>& tree) {
  //Step 1: Do inorder traversal
  if(tree.size() == 0)
    return true;
  inOrder(tree[0], tree);
  if(result.size() == 0)
    return true;
  long long temp = result[0];
  //Step 2: For BST, inorder traversal must be always increasing
  for(auto it = result.begin() + 1; it != result.end(); it++){
    if(temp > *it)
      return false;
    temp = *it;
  }
  return true;
}

int main() {
  int nodes;
  cin >> nodes;
  vector<Node> tree;
  for (int i = 0; i < nodes; ++i) {
    long long key;
    int left, right;
    cin >> key >> left >> right;
    tree.push_back(Node(key, left, right));
  }
  if (IsBinarySearchTree(tree)) {
    cout << "CORRECT" << endl;
  } else {
    cout << "INCORRECT" << endl;
  }
  return 0;
}