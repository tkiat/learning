console.clear();
const operators = ["+", "-", "*", "/"];

class Calculator extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      displayValue: "0",
      expression: ""
    };
    this.clearDisplayClick = this.clearDisplayClick.bind(this);
    this.digitClick = this.digitClick.bind(this);
    this.operatorClick = this.operatorClick.bind(this);
    this.equalClick = this.equalClick.bind(this);
    this.decimalPointClick = this.decimalPointClick.bind(this);
  }
  
  updateStatesAndDisplay(newVal, newExp){
    this.setState({
      displayValue: newVal,
      expression: newExp
    }, () => {
      document.getElementById("display").innerHTML = this.state.displayValue;
      document.getElementById("expression").innerHTML = this.state.expression;
    });
  }
  clearDisplayClick(){
    let newDisplayValue = "0";
    let newExp = "";
    this.updateStatesAndDisplay(newDisplayValue, newExp);
  }
  decimalPointClick(event){
    if(!this.state.displayValue.includes(".")){
      let newDisplayValue = this.state.displayValue + ".";
      let newExp = this.state.expression + ".";
      this.updateStatesAndDisplay(newDisplayValue, newExp);
    }
  }
  digitClick(event){
    let isPrevValoperator = operators.includes(this.state.expression.slice(-1));
    let isPrevValZero = this.state.displayValue == "0";
    
    let newDisplayValue = (isPrevValoperator || isPrevValZero) ? event.target.innerHTML : this.state.displayValue + event.target.innerHTML;
    let newExp = this.state.expression + event.target.innerHTML; ;
    this.updateStatesAndDisplay(newDisplayValue, newExp);
  }
  operatorClick(event){
    let exp = this.state.expression;
    let isPrevValoperators = operators.includes(exp.slice(-1));
    let isCurrentValSubtract = event.target.id == "subtract";
    
    let newDisplayValue = event.target.innerHTML;
    let newExp = (isPrevValoperators && !isCurrentValSubtract ? exp.slice(0, -1) : exp) + event.target.innerHTML;
    this.updateStatesAndDisplay(newDisplayValue, newExp);
  }
  equalClick(){
    //make 5*-5 = 25 and 5*-+5 = 10 (reduce to only last operators except -)
    let exp = this.state.expression;
    exp = exp.replace(/[\+\-\*\/](?=[\+\-\*\/]{2})/g, "");
    exp = exp.replace(/[\+\-\*\/](?=[\+\*\/])/g, "");
    exp = exp.replace(/\-\-/g, "\-");
    let result = eval(exp).toString();
    
    let newDisplayValue = result;
    let newExp = result;
    this.updateStatesAndDisplay(newDisplayValue, newExp);
  }
  
  render (){
    return (
      <div>
        <div className="presentation">
          <div id="expression"></div>
          <div id="display">0</div>
        </div>
        <div className="calcButton">
          <div id="clear" onClick={this.clearDisplayClick}>AC</div>
          <div id="divide" onClick={this.operatorClick}>/</div>
          <div id="multiply" onClick={this.operatorClick}>*</div>

          <div id="seven" onClick={this.digitClick}>7</div>
          <div id="eight" onClick={this.digitClick}>8</div>
          <div id="nine" onClick={this.digitClick}>9</div>
          <div id="subtract" onClick={this.operatorClick}>-</div>
          
          <div id="four" onClick={this.digitClick}>4</div>
          <div id="five" onClick={this.digitClick}>5</div>
          <div id="six" onClick={this.digitClick}>6</div>
          <div id="add" onClick={this.operatorClick}>+</div>
          
          <div id="one" onClick={this.digitClick}>1</div>
          <div id="two" onClick={this.digitClick}>2</div>
          <div id="three" onClick={this.digitClick}>3</div>
          <div id="equals" onClick={this.equalClick}>=</div>

          <div id="zero" onClick={this.digitClick}>0</div>
          <div id="decimal" onClick={this.decimalPointClick}>.</div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Calculator />, document.getElementById("root"));