#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>

using std::string;
using std::vector;
//Result: Max time used: 1.66/2.00, max memory used: 113848320/536870912
//in this case exploit the fact that ATCG 4 chars otherwise define count[256]
string InverseBWT(const string& bwt) {

  string text = "";
  int len = bwt.length();
  std::unordered_map <string, string> mymap;
  //find n-th occurence of original string
  int suffix_r[len];
  char possible_char[] = {'$', 'A', 'C', 'G', 'T'};
  int count[5];
  for(int i = 0; i < 5; i++)
    count[i] = 0;
  
  size_t ind = 0;
  for(char c : bwt){
    for(int i = 4; i >= 0; i--){ //check for '$' last
      if(c == possible_char[i]){
        count[i]++;
        suffix_r[ind++] = count[i];
        break;
      }
    }
  }
  /*
  //sort bwt (general case)
  string bwt_sorted = "";
  for(int i = 0; i < 5; i++)
    bwt_sorted.insert(bwt_sorted.end(), count[i], possible_char[i]);
    */
  //construct map
  int suffix_l[len];
  int index = 0;
  for(int i = 0; i < 5; i++){
    int temp = count[i];
    while(temp > 0){
      mymap.insert(std::pair<string, string>(possible_char[i] + std::to_string(count[i] - temp + 1), bwt[index] + std::to_string(suffix_r[index])));
      index++;
      temp--;
    }
  }
  //recover text
  string s = "$1";//general case: bwt_sorted[0] + std::to_string(suffix_l[0]);
  for(int i = 0; i < len; i++){
    //std::cout << s << " and " << mymap[s] << "\n"; //visualize like the lecture
    text += s[0];
    s = mymap[s];
  }
  reverse(text.begin(), text.end());
  /*General case (256 chars)
  int suffix_l[len];
  string bwt_sorted = "";
  int index = 0;
  for(int i = 0; i < 256; i++){

    if(count[i] > 0){
      suffix_l[index] = 0;
      int a = 1;
      while(count[i] > 0){
        bwt_sorted += (char) (i-128);
        suffix_l[index] = a++;
        //append n-th occurence of both string to its char, put them in map
        mymap.insert(std::pair<string, string>(bwt_sorted[index] + std::to_string(suffix_l[index]), bwt[index] + std::to_string(suffix_r[index])));
        index++;
        count[i]--;
      }
    }
  }
  */
  return text;
}

int main() {

  string bwt;
  std::cin >> bwt;
  std::cout << InverseBWT(bwt) << std::endl;
  return 0;
}

