#include <cmath>
#include <iostream>
#include <vector>

const double EPS = 1e-6;
const int PRECISION = 20;

typedef std::vector<double> Column;
typedef std::vector<double> Row;
typedef std::vector<Row> Matrix;

//Result: Max time used: 0.00/1.00, max memory used: 9703424/536870912
//----------------------------------------
struct Equation {
  Matrix a;
  Column b;
  Equation(const Matrix &a, const Column &b): a(a), b(b) {}
};
struct Position {
  int column;
  int row;
  Position(int column, int row): column(column), row(row) {}
};
//----------------------------------------
//2D vec a is matrix of everything except last column
//1D vec b is last column which will be solution after gaussian elimination
Equation ReadEquation() {
  int size;
  std::cin >> size;
  Matrix a(size, std::vector <double> (size, 0.0));
  Column b(size, 0.0);
  for (int row = 0; row < size; ++row) {
    for (int column = 0; column < size; ++column)
      std::cin >> a[row][column];
    std::cin >> b[row];
  }
  return Equation(a, b);
}

Position SelectPivotElement(
  const Matrix &a, 
  std::vector <bool> &used_rows, 
  std::vector <bool> &used_columns) {

  size_t size = a.size();
  Position pivot_element(0, 0);

  while (used_rows[pivot_element.row])
    pivot_element.row++;
  while (used_columns[pivot_element.column])
    pivot_element.column++;
  while(pivot_element.row < size - 1 && pivot_element.column < size 
      && fabs(a[pivot_element.row][pivot_element.column]) < 0.001)
    pivot_element.row++;
  while(pivot_element.row < size - 1 && pivot_element.column < size 
      && fabs(a[pivot_element.row][pivot_element.column]) < 0.001)
    pivot_element.column++;

  return pivot_element;
}

void SwapLines(Matrix &a, Column &b, std::vector <bool> &used_rows, Position &pivot_element) {
  std::swap(a[pivot_element.column], a[pivot_element.row]);
  std::swap(b[pivot_element.column], b[pivot_element.row]);
  std::swap(used_rows[pivot_element.column], used_rows[pivot_element.row]);
  pivot_element.row = pivot_element.column;
}

void ProcessPivotElement(Matrix &a, Column &b, const Position &pivot_element) {
  //value of pivot
  double pivot = a[pivot_element.row][pivot_element.column];
  //rescale pivot's row to make pivot 1
  for(int i = 0; i < a.size(); i++)
    a[pivot_element.row][i] /= pivot;
  b[pivot_element.row] /= pivot;
  //subtract pivot's row from others to make other entries in pivot's column 0
  for(int i = 0; i < a.size(); i++){
    if(i == pivot_element.row)
      continue;
    double val = a[i][pivot_element.column];
    if(val == 0)
      continue;
      //std::cout << val << " " << pivot << "\n";
    for(int j = 0; j < a.size(); j++)
      a[i][j] -= val * a[pivot_element.row][j];
    b[i] -= val * b[pivot_element.column];
  }
}

void MarkPivotElementUsed(
  const Position &pivot_element, 
  std::vector <bool> &used_rows, 
  std::vector <bool> &used_columns) {

  used_rows[pivot_element.row] = true;
  used_columns[pivot_element.column] = true;
}

Column SolveEquation(Equation equation) {
  Matrix &a = equation.a;
  Column &b = equation.b;
  int size = a.size();

  std::vector <bool> used_columns(size, false);
  std::vector <bool> used_rows(size, false);
  for (int step = 0; step < size; ++step) {
    //pivot = first left-most, top-most, unused row & column, non-zero element
    Position pivot_element = SelectPivotElement(a, used_rows, used_columns);
    //pivot is zero, no solution
    if(fabs(a[pivot_element.row][pivot_element.column]) < 0.001)
      return vector<double>(); 
    //if pivot.row != pivot.column then swap pivot's row to top of non-pivot row
    //after this pivot.row = pivot.column
    SwapLines(a, b, used_rows, pivot_element);
    //rescale pivot's row to make pivot 1
    //subtract pivot's row from others to make other entries in pivot's column 0
    ProcessPivotElement(a, b, pivot_element);
    //mark pivot as used
    MarkPivotElementUsed(pivot_element, used_rows, used_columns);
  }
  return b;
}

void PrintColumn(const Column &column) {
  int size = column.size();
  std::cout.precision(PRECISION);
  for (int row = 0; row < size; ++row)
    std::cout << column[row] << std::endl;
}
//----------------------------------------
int main() {

  Equation equation = ReadEquation();
  Column solution = SolveEquation(equation);
  PrintColumn(solution);
  return 0;
}
