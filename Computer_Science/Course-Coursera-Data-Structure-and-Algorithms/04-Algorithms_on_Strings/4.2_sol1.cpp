#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

//Result: Max time used: 0.01/0.50, max memory used: 8994816/536870912
//not very time efficient
string BWT(const string& text) {

  int len = text.length();
  string result = "";

  vector<string> vec(len);
  for(int i = 0; i < len; i++)
    vec[i] = text.substr(i) + text.substr(0, i);

  std::sort(vec.begin(), vec.end());

  for(int i = 0; i < len; i++)
    result += vec[i][len-1];

  return result;
}

int main() {

  string text;
  cin >> text;
  cout << BWT(text) << endl;
  return 0;
}