class App extends React.Component {
    constructor(props){
      //console.log("constructor()")
      super(props);
      this.state = {
        currentQuote: {},
        isFetched: false
      };
      this.quotes = {};
      this.getNewQuote = this.getNewQuote.bind(this);
    }
    
    componentDidMount(){
      //console.log("componentDidMount()")
      fetch('https://raw.githubusercontent.com/theerawatK/raw-data/master/quotes.json')
      .then(response => response.json())
      .then(
        (data) => {
          this.setState({
            isFetched: true
          })
          this.quotes = data.quotes;
          this.getNewQuote();
        },
        (error) => {
          this.setState({
            isfetched: false
          })
        }
      );
    }
    
    getNewQuote(){
      //console.log("getNewQuote()");
      let index = Math.floor(Math.random() * (this.quotes.length - 1));
      while(this.state.currentQuote === this.quotes[index]){
        index = Math.floor(Math.random() * (this.quotes.length - 1));
      }
      this.setState({
        currentQuote: this.quotes[index]
      })
    }
    
    render(){
      //console.log("render()");
      let item =  this.state.isFetched ? this.state.currentQuote : {quote: "default...", author: "loading..."};
      let twitter_link = "https://www.twitter.com/intent/tweet?text=" + "\"" + encodeURIComponent(item.quote) + "\" " + item.author + "\&hashtags=quotes"
      return (
        <div id="quote-box">
          <h1 id="text">{item.quote}</h1>
          <h2 id="author">{item.author}</h2>
          <button id="new-quote" className="fa" onClick={this.getNewQuote} >New Quote</button>
          <a href={twitter_link} target="_blank" id="tweet-quote" className="fa fa-twitter"></a>
        </div>
      );
    }
}
  
ReactDOM.render(<App />, document.getElementById('root'));