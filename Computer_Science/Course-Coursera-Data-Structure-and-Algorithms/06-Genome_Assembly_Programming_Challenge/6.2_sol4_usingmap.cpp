#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stack>
#include <unordered_map>

using std::vector;
using std::string;

typedef std::unordered_multimap<string,string> Graph;
#define k_mer_count 5396 //default 5396
#define k_mer_length 10

//Result: Max time used: 0.02/3.00, max memory used: 10027008/536870912
//--------------------------------------------------
Graph getDirectedGraph();
bool isBalancedGraph(Graph &graph);
vector<string> hierholzer(Graph graph);

int main() {

  std::ios_base::sync_with_stdio(false);
  //get input
  Graph graph = getDirectedGraph();
  if(!isBalancedGraph(graph))
    return 0;
  //find path
  vector<string> path = hierholzer(graph);
  //print result
  for(int i = 0; i < path.size() - 1; i++)
    std::cout << path[i][path[i].length()-1];

  return 0;
}
//--------------------------------------------------
//Hierholzer algorihtm: find Eulerian path
vector<string> hierholzer(Graph graph){

  vector<string> path;
  if(graph.size() == 0)
    return vector<string>();
    
  //outdegree of each node
  std::unordered_map<string,int> outdegree;
  for(std::pair<string,string> e : graph)
    outdegree[e.first] = graph.count(e.first);

  std::stack<string> st; //store travered nodes
  
  //Hierholzer Step 1: Choose any vertex and begin traversing
  string u = graph.begin()->first;
  st.push(u);
  
  while (!st.empty()){
    //Hierholzer Step 2: keep traversing while there is still unexplored edge
    if (outdegree[u] > 0){
      st.push(u);
        //return pair iterator: first/second = lower/upper bound of the range
      auto ret = graph.equal_range(u);
      string next = ret.first->second;
      graph.erase(ret.first);
      outdegree[u]--; //remove explored edge
      u = next;
    }
    //Hierholzer Step 2: backtrack if there is no unexplored edge
    else{
      path.push_back(u);
      u = st.top();
      st.pop();
    }
  }
  //Hierholzer Step 3: reverse path
  std::reverse(path.begin(), path.end());
  
  return path;
}
//A graph is balanced if indegree = outdegree for each node
bool isBalancedGraph(Graph &graph){

  std::unordered_map<string,int> outdegree;
  std::unordered_map<string,int> indegree;

  for(std::pair<string,string> e : graph){
    outdegree[e.first]++;
    indegree[e.second]++;
  }
  for(std::pair<string,string> e : graph)
    if(indegree[e.first] != outdegree[e.first])
      return false;
  return true;
}
//read input
Graph getDirectedGraph(){

  Graph adj;
  for(int i = 0; i < k_mer_count; i++){
    string k_mer;
    std::cin >> k_mer;
    string u = k_mer.substr(0, k_mer_length-1);
    string v = k_mer.substr(1, k_mer_length-1);
    adj.insert(std::make_pair(u, v));
  }
  return adj;
}