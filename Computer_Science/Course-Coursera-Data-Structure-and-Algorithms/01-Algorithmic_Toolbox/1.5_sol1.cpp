#include <iostream>
#include <climits>

//Result: Max time used: 0.00/1.00, max memory used: 9605120/536870912
int get_change(int m) {
  //write your code here
  int coins[] = {4, 3, 1};
  int numCoinValues = sizeof(coins)/sizeof(coins[0]);

  int minNumCoins[m+1];
  minNumCoins[0] = 0;
  
  for(int i = 1; i <= m; i++){
    minNumCoins[i] = INT_MAX;
    for(int j = 0; j < numCoinValues; j++){
      if(i >= coins[j]){
        int numCoins = minNumCoins[(i-coins[j])] + 1;
        if(numCoins < minNumCoins[i]){ //select lowest # coins
          minNumCoins[i] = numCoins;
        }
      }
    }
  }
  return minNumCoins[m];
}

int main() {
  int m;
  std::cin >> m;
  std::cout << get_change(m) << '\n';
}
