#include <iostream>
#include <string>
#include <vector>
#include <map>

using std::map;
using std::vector;
using std::string;

typedef map<char, int> edges;
typedef vector<edges> Trie;

//Result (Q2): Max time used: 0.20/1.00, max memory used: 48091136/536870912
//Result (Q3): Max time used: 0.21/1.00, max memory used: 48091136/536870912
//----------------------------------------
Trie build_trie(const vector<string> &patterns) {

  Trie trie;
  
  int ind = 0;
  for(int i = 0; i < patterns.size(); i++){
    int temp = 0;
    int offset = 0;
    //step 1: move from the root in trie "offset" times 
	//until we find non-existing char in pattern
    for(char c : patterns[i]){

      while(temp + 1 > trie.size()) //prevent segmentation fault
        trie.push_back(map<char, int>());

      if(trie[temp].find(c) != trie[temp].end()){ //if found
        temp = trie[temp][c];
        offset++;
      }
      else
        break;
    }
    //step 2: neglect first "offset" char in pattern[i] and insert the rest
    for(int j = offset; j < patterns[i].length(); j++){
    
      while(temp + 1 > trie.size())
        trie.push_back(map<char, int>());
      
      trie[temp].insert(std::pair<char,int>(patterns[i][j], ++ind));
      temp = ind;
    }
    while(temp + 1 > trie.size())
      trie.push_back(map<char, int>());
    //delimiter of each trie
    trie[temp].insert(std::pair<char,int>('$', ++ind));
  }
  return trie;
}

int prefixTrieMatching(const string& text, Trie &trie){

	int ind = 0;
	int temp = 0; //begin at root of trie
	char c = text[ind++];

	while(1){
		//put $ cond. before since AAA can be in trie and AA can be pattern
		if(trie[temp].find('$') != trie[temp].end()) //if c is a leaf in trie
			return 1;
		else if(trie[temp].find(c) != trie[temp].end()){ //found c, move on to next char
			temp = trie[temp][c];
			c = text[ind++];
		}
		else //no match
			return -1; 
	}
}

vector <int> trieMatching(const string& text, int n, const vector <string>& patterns){
	
  vector <int> result;
	Trie trie = build_trie(patterns);

	int ind = 0;
	while(ind < text.length()){
		if(prefixTrieMatching(text.substr(ind), trie) == 1)
			result.push_back(ind);
		ind++;
	}
	return result;
}
//----------------------------------------
int main (void){

	string t;
	std::cin >> t;

	int n;
	std::cin >> n;

	vector <string> patterns (n);
	for (int i = 0; i < n; i++)
		std::cin >> patterns[i];
	
	vector <int> ans;
	ans = trieMatching(t, n, patterns);

	for (int i = 0; i < (int) ans.size (); i++){

		std::cout << ans[i];
		if (i + 1 < (int) ans.size ())
			std::cout << " ";
		else
			std::cout << std::endl;
	}
	return 0;
}
/*
//Reffile
int const Letters =    4;
int const NA      =   -1;

struct Node{
	int next [Letters];

	Node (){
		std::fill (next, next + Letters, NA);
	}

	bool isLeaf () const{
	  return (next[0] == NA && next[1] == NA && next[2] == NA && next[3] == NA);
	}
};

int letterToIndex (char letter){
	switch (letter){
		case 'A': return 0; break;
		case 'C': return 1; break;
		case 'G': return 2; break;
		case 'T': return 3; break;
		default: assert (false); return -1;
	}
}
*/
