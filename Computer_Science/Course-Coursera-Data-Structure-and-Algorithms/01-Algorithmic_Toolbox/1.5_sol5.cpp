#include <iostream>
#include <vector>

using std::vector;

//Result: Max time used: 0.02/1.00, max memory used: 17993728/536870912
int lcs3(vector<int> &a, vector<int> &b, vector<int> &c) {
    
  int m = a.size();
  int n = b.size();
  int p = c.size();
  vector<vector<vector<int>>> DP(m+1, vector<vector<int>>(n+1, vector<int>(p+1)));

  //DP[i][j][k] = length of LCS of a[0]...a[i], b[0]...b[j], and c[0]...c[k]
  for (int i = 0; i <= m; i++){
    for (int j = 0; j <= n; j++){
      for (int k = 0; k <= p; k++){

        if (i == 0 || j == 0 || k == 0)
          DP[i][j][k] = 0;

        else if (a[i-1] == b[j-1] && b[j-1] == c[k-1])
          DP[i][j][k] = DP[i-1][j-1][k-1] + 1;

        else
          DP[i][j][k] = std::max(std::max(DP[i-1][j][k], DP[i][j-1][k]), DP[i][j][k-1]);
      }
    }
  }
  return DP[m][n][p];
}

int main() {
  size_t an;
  std::cin >> an;
  vector<int> a(an);
  for (size_t i = 0; i < an; i++) {
    std::cin >> a[i];
  }
  size_t bn;
  std::cin >> bn;
  vector<int> b(bn);
  for (size_t i = 0; i < bn; i++) {
    std::cin >> b[i];
  }
  size_t cn;
  std::cin >> cn;
  vector<int> c(cn);
  for (size_t i = 0; i < cn; i++) {
    std::cin >> c[i];
  }
  std::cout << lcs3(a, b, c) << std::endl;
}
