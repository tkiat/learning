//Ans: 26442

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>

//copy from https://www.youtube.com/watch?time_continue=1&v=JE0JE8ce1V0

#define INT_MAX 999999

using namespace std;

int n = 4;

int dist[10][10] = {
	{0,20,42,25},
	{20,0,30,34},
	{42,30,0,10},
	{25,34,10,0}
};

//int dist[25][25];

int VISITED_ALL = (1<<n) - 1;

int TSP(int mask, int pos){

	if(mask == VISITED_ALL){
		return dist[pos][0];
	}

	int ans = INT_MAX;

	for(int city = 0; city < n; city++){

		if((mask & (1 << city)) == 0){

			int newAns = dist[pos][city] + TSP(mask|(1<<city), city);
			ans = min(ans, newAns);
		}
	}
	return ans;
}

int main(){

	//copy from

	for(int i = 0; i < (1 << n); i++){
		for(int j = 0; j < n; j++){
			
		}
	}
	

	//my code
	int N;

	ifstream myfile("real.txt");
	string line;
	getline(myfile, line);

	N = stoi(line);
	vector<vector<double>> c(N);
	for(int i = 0; i < N ; i++)
		c[i].resize(2);

	double x, y;

	cout.precision(9);
	if (myfile.is_open()) {
		int i = 0;
		while (getline(myfile, line)) {

			size_t pos;
			pos = line.find_first_of(" ", 0);
			x = stod(line.substr(0, pos));
			y = stod(line.substr(pos + 1, string::npos));
			c[i][0] = x;
			c[i][1] = y;
			//cout << c[i][0] << " " << c[i][1] << endl;
			i++;
		}
		myfile.close();
	}
	else {
		cout << "Unable to open file\n";
	}

	//construct Euclidean distance table
	vector<vector<double>> a(N);
	for(int i = 0; i < N ; i++)
		a[i].resize(N);

	//int minDist = 9999999;
	for(int i = 0; i < N; i++){
		for(int j = 0; j < N; j++){
			a[i][j] = sqrt(pow(c[i][0] - c[j][0], 2) + pow(c[i][1] - c[j][1], 2));
		}
		//cout << endl;
	}
	
	cout << TSP(1,0) << endl;
	
	//by looking at plot and trying to take the circular-like path
	double minDist = a[0][1] + a[1][5] + a[5][9] + a[9][10] + a[10][11]
	+ a[11][14] + a[14][18] + a[18][17] + a[17][21] + a[21][22] 
	+ a[22][20] + a[20][16] + a[16][19] + a[19][24] + a[24][23] 
	+ a[23][15] + a[15][13] + a[13][12] + a[12][8] + a[8][6] 
	+ a[6][2] + a[2][3] + a[3][7] + a[7][4] + a[4][0];
	cout << "minDist " << minDist << endl;

	cout << "End of File" << endl;
	getchar();
	return 0;
}