#include <iostream>

//Result: Max time used: 0.47/1.00, max memory used: 9924608/536870912
long long lcm_naive(int a, int b) {
  for (long l = 1; l <= (long long) a * b; ++l)
    if (l % a == 0 && l % b == 0)
      return l;

  return (long long) a * b;
}

long long EuclidGCD(int a, int b){
  if (b==0)
    return a;
  a = a % b;
  return (long long) EuclidGCD(b, a);
}

int main() {
  long long a, b;
  std::cin >> a >> b;
  //std::cout << lcm_naive(a, b) << std::endl;
  std::cout << (a/EuclidGCD(a,b))*b << std::endl;
  
  return 0;
}
