#include <iostream>
#include <stack>
#include <string>

//Result: Max time used: 0.00/1.00, max memory used: 9867264/536870912
struct Bracket {
    Bracket(char type, int position):
        type(type),
        position(position)
    {}

    bool Matchc(char c) {
        if (type == '[' && c == ']')
            return true;
        if (type == '{' && c == '}')
            return true;
        if (type == '(' && c == ')')
            return true;
        return false;
    }

    char type;
    int position;
};

int main() {
    std::string text;
    getline(std::cin, text);

    std::stack <Bracket> opening_brackets_stack;
    for (int position = 0; position < text.length(); ++position) {
        char next = text[position];
        Bracket temp(next, position);

        if (next == '(' || next == '[' || next == '{') {
            // Process opening bracket, write your code here
            opening_brackets_stack.push(temp);
        }
        else if (next == ')' || next == ']' || next == '}') {
            // Process closing bracket, write your code here
            if(!opening_brackets_stack.empty() && (opening_brackets_stack.top()).Matchc(next)){
                opening_brackets_stack.pop();
            }
            else{
                opening_brackets_stack.push(temp);
                break;
            }
        }
    }

    // Printing answer, write your code here
    if(opening_brackets_stack.empty()){
        std::cout << "Success";
    }
    else{
       std::cout << (opening_brackets_stack.top()).position + 1;
    }
    
    return 0;
}
