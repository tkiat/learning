#include <iostream>
#include <vector>
#include <algorithm>

//Result: 280 ms 8120 KB
struct custom{
    int val;
    int seq;
    int pos;
    custom(int a, int b, int c){
        val = a;
        seq = b;
        pos = c;
    }
};
bool customSort(custom lhs, custom rhs) { 
    return lhs.val < rhs.val; 
}

int main(){

    //turn off synchronization of all the iostream
    //to prevent cin from wasting time synchronizing itself with C stdio buffer
    std::ios::sync_with_stdio(false);

    int k;
    std::cin >> k;
    std::vector<custom> info;

    int n;
    int ind = 0;
    int seq = 0;
    for(int i = 0; i < k; i++){
        std::cin >> n;
        seq++;
        int sum = 0;
        for(int j = 0; j < n; j++){

            int val;
            std::cin >> val;
            sum += val;
            custom temp(val, seq, j + 1);
            info.push_back(temp);
        }
        for(int j = ind; j < ind + n; j++){
            info[j].val = sum - info[j].val;
        }
        ind += n;
    }
    std::sort(info.begin(), info.end(), customSort);
    for(int i = 1; i < info.size(); i++){
       if(info[i].val == info[i-1].val && info[i].seq != info[i-1].seq){
           std::cout << "YES\n" << info[i].seq << " " << info[i].pos << "\n";
           std::cout << info[i-1].seq << " " << info[i-1].pos << "\n";
           return 0;
       }
    }
    std::cout << "NO";

    getchar();
    return 0;
}