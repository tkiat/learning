var coll = document.getElementsByClassName("collapsible-btn");
for(let i = 0; i < coll.length; ++i){
  coll[i].addEventListener("click", () => {
    var content = event.target.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
      content.style.border = "0";
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
      content.style.border = "2px solid black";
    }
  })
}