#! /usr/bin/env nix-shell
#! nix-shell -i runghc -p "haskellPackages.ghcWithPackages (p: [p.turtle])"

{-# LANGUAGE OverloadedStrings #-}

import Turtle

main :: IO ()
main = sh $ do
  echo "Hello World"
