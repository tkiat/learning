"use strict";
console.clear();

const [GAME, MOVIE, KICKSTARTER] = [0, 1, 2];

const GAME_TITLE = "Video Game Sales";
const MOVIE_TITLE = "Movie Sales";
const KICKSTARTER_TITLE = "Kickstarter Pledges";

const GAME_DESC = "Top 100 Most Sold Video Games Grouped by Platform";
const MOVIE_DESC = "Top 100 Highest Grossing Movies Grouped By Genres";  
const KICKSTARTER_DESC = "Top 100 Most Pledged Kickstarter Campaigns Grouped By Category";

const GAME_URL = "https://cdn.rawgit.com/freeCodeCamp/testable-projects-fcc/a80ce8f9/src/data/tree_map/video-game-sales-data.json";
const MOVIE_URL = "https://cdn.rawgit.com/freeCodeCamp/testable-projects-fcc/a80ce8f9/src/data/tree_map/movie-data.json";
const KICKSTARTER_URL = "https://cdn.rawgit.com/freeCodeCamp/testable-projects-fcc/a80ce8f9/src/data/tree_map/kickstarter-funding-data.json";

const titleList = [GAME_TITLE, MOVIE_TITLE, KICKSTARTER_TITLE];
const descList = [GAME_DESC, MOVIE_DESC, KICKSTARTER_DESC];
const urlList = [GAME_URL, MOVIE_URL, KICKSTARTER_URL]

const colorList = ["purple", "brown", "olive", "teal", "navy", "black", "red", "sienna", "mediumseagreen", "darkgreen", "green", "cadetblue", "blue", "maroon", "mediumvioletred", "grey", "deeppink", "chocolate", "tomato"];

var datasetList = [];

var title = d3.select("body")
              .append("h1")
              .attr("id", "title")
              .text(titleList[GAME])
  
var titleDesc = d3.select("body")
                  .append("h3")
                  .attr("id", "description")
                  .text(descList[GAME])

function get(url) {
  return new Promise(function(resolve, reject) {
    var req = new XMLHttpRequest();
    req.open('GET', url, false);
    req.onload = function() {
      req.status == 200 ? resolve(JSON.parse(req.responseText)) : reject(new Error(req.statusText));
    };
    req.send();
  });
}

document.addEventListener('DOMContentLoaded', function(){
  //resolves: same order as async functions
  Promise.all([get(urlList[GAME]), get(urlList[MOVIE]), get(urlList[KICKSTARTER])])
         .then(results => {
            datasetList = results;
            treemapGenerator(GAME);
          });  
})   

function treemapGenerator(set){
  title.text(titleList[set])
  titleDesc.text(titleList[set])
  
  //main svg
  d3.select("#mainsvg").remove();
  const w = 850, h = 1050, treemap_h = 650, legend_h = h - treemap_h;
  var svg = d3.select("body")
              .append("svg")
              .attr("id", "mainsvg")
              .attr("width", w)
              .attr("height", h) 

  //d3 - treemap
  var rootNode = d3.hierarchy(datasetList[set])
  rootNode.sum(function(d) {
    return d.value;
  });

  var treemapLayout = d3.treemap()
                        .size([w, treemap_h])
                        .tile(d3.treemapSquarify)//treemapBinary-treemapSquarify
  treemapLayout(rootNode);

  //color-mapping
  const categories = rootNode.descendants()[0].data.children.map(d => d.name); //categories = direct children of root node

  var color_mapping = {};
  categories.forEach((d, i) => color_mapping[d] = colorList[i]);

  //draw treemap
  var treemapGroup = svg.append('g')
  treemapGroup.selectAll("rect")
              .data(rootNode.leaves())
              .enter()
              .append("rect")
              .attr('transform', d => "translate(" + [d.x0, d.y0] + ")")
              .attr("class", "tile")
              .attr("width", d => d.x1 - d.x0)
              .attr("height", d => d.y1 - d.y0)
              .attr("data-name", d => d.data.name)
              .attr("data-category", d => d.data.category)
              .attr("data-value", d => d.data.value)
              .attr("fill", d => color_mapping[d.data.category])

  //d3 - text
  var textGroup = svg.append('g');
  textGroup.selectAll("foreignObject")
           .data(rootNode.leaves())
           .enter()
           .append('foreignObject')
           .attr('transform', d => "translate(" + [d.x0, d.y0] + ")")
           .attr("class", "text")
           .attr('x', 4)
           .attr('y', 4)
           .attr('width', d => d.x1 - d.x0)
           .attr('height', d => d.y1 - d.y0)
           .attr("fill", "orange")
           .html( d => {
               let width = d.x1 - d.x0 - 8, height = d.y1 - d.y0;
               return `<div class="bar" style="width: ${width}px; height: ${height}px;">${d.data.name}</div>`;
            })
  
  //d3 - legend
  var legendGroup = svg.append('g')
                       .attr("id", "legend")
                       .attr("width", w)
                       .attr("height", legend_h)
  var legend = d3.select("#legend");

  const legendRectWidth = 30, numLegendCols = 8;
  const legendX = 20, legendY = treemap_h + 20; //beginning coordinate

  categories.forEach((d, i) => {
    let x = legendX + legendRectWidth + 100 * (i%numLegendCols);
    let y = legendY + 80 * Math.floor(i/numLegendCols);
    legend.append("rect")
      .attr("class", "legend-item")
      .attr("x", x)
      .attr("y",  y)
      .attr("width", legendRectWidth)
      .attr("height", 20)
      .style("fill", colorList[i])
      .style("stroke", "black")

    legend.append("text")
      .attr("x", x)
      .attr("y", y + 40)
      .text(categories[i])
      .style("font-size", "10px")
      .attr("alignment-baseline","middle")
  })

  //d3 - tooltip
  var tooltip = d3.select("body")
  .append("div")
  .attr("id", "tooltip")
  .style("position", "absolute")
  .style("visibility", "hidden");

  d3.selectAll(".tile, .text")
    .on("mouseover", function(d){
    tooltip.attr("data-value", d.data.value)
           .style("visibility", "visible")
           .text("Name: " + d.data.name + " Category: " + d.data.category + " Value: " + d.data.value)
  })
    .on("mousemove", function(d){
    var vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    tooltip.style("top", (event.pageY - 30) + "px")
           .style("left", event.pageX > vw/2 ? event.pageX - 300 : event.pageX + "px");

  })
    .on("mouseout", function(){
    return tooltip.style("visibility", "hidden");
  });
}