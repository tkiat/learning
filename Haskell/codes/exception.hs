-- src: https://stackoverflow.com/questions/8853896/throwing-exceptions-in-haskell-and-xderivedatatypeable
import Control.Exception
import System.Exit (exitFailure)

data MyException = ThisException | ThatException deriving (Show)

instance Exception MyException

data CellPos = CellPos Int Int deriving (Show, Read)

test :: String -> IO CellPos
test str = do
  if length str == 0
    then throwIO ThisException
    else return (CellPos 0 0)

main :: IO ()
main = do
  result <- test "hello" `catch` handler
  result <- test "" `catch` handler
  pure ()

handler ThisException = putStrLn "Oh no!" >> exitFailure
handler ThatException = putStrLn "Yikes!" >> exitFailure
