#include <iostream>
#include <vector>
#include <climits>
#include <stack>

using std::vector;

//Result: Max time used: 0.07/2.00, max memory used: 10383360/536870912
int bellmanFord_neg_cycle(vector<vector<int> > &adj, vector<vector<int> > &cost) {

  int n = adj.size();
  vector<int> dist_update(n, INT_MAX);
    //pointer to previous vertex (to create a path later) [optional]
  //vector<int> prev(n, INT_MIN); 
  int src = n - 1;
  dist_update[src] = 0;

  //|V-1| times is enough to compute shortest path (given no negative cycle)
  for(int i = 0; i < n - 1; i++){
    for(int u = 0; u < n; u++){
      int u_col = 0;
      for(auto v = adj[u].begin(); v != adj[u].end(); v++){
        //add dist_update[u] != INT_MAX to avoid integer overflow
        if(dist_update[u] != INT_MAX && dist_update[u] + cost[u][u_col] < dist_update[*v]){
          dist_update[*v] = dist_update[u] + cost[u][u_col];
          //prev[*v] = u;
        }
        u_col++;
      }
    }
  }
  //neg weight cycle iff |V|-th (i.e. 1 more) iteration updates some dist value
  for(int u = 0; u < n; u++){
    int u_col = 0;
    for(auto v = adj[u].begin(); v != adj[u].end(); v++){
      if(dist_update[u] != INT_MAX && dist_update[u] + cost[u][u_col] < dist_update[*v])
        return 1;
      u_col++;
    }
  }
  return 0;
}

int main() {
  
  int n, m;
  std::cin >> n >> m;
  vector<vector<int> > adj(n + 1, vector<int>());
  vector<vector<int> > cost(n + 1, vector<int>());
  for (int i = 0; i < m; i++) {
    int x, y, w;
    std::cin >> x >> y >> w;
    adj[x - 1].push_back(y - 1);
    cost[x - 1].push_back(w);
  }
  //add one virtual node linking all nodes to cope with unconnected clusters
  for (int i = 0; i < n; i++) {
    adj[n].push_back(i);
    cost[n].push_back(0);
  }
  std::cout << bellmanFord_neg_cycle(adj, cost);
}
