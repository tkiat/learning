{ pkgs ? import <nixpkgs> {} }:

let
  myGhc = pkgs.haskellPackages.ghcWithPackages (hpkgs: with hpkgs; [
    random
  ]);
in
pkgs.mkShell {
  buildInputs = [ myGhc ];
}
