#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

using std::vector;
using std::string;
using std::unordered_map;

#define input_count 400 //default 400
//Result: Max time used: 0.37/3.00, max memory used: 9715712/536870912
//----------------------------------------
unordered_map<string,string> buildDirectedGraph(vector<string> &input, int k_mer_length);

int main() {

  std::ios_base::sync_with_stdio(false);
  
  vector<string> input;
  for(int i = 0; i < input_count; i++){
    string temp;
    std::cin >> temp;
    input.push_back(temp);
  }
  //*there is one possible Eulerian cycle iff the whole graph is a cycle
  for(int k_mer = 100; k_mer >= 1; k_mer--){

    unordered_map<string,string> graph = buildDirectedGraph(input, k_mer);
    
    int numVertices = graph.size();
    if(numVertices == 0)
      continue;
    string start = graph.begin()->first;
    string cur = graph.begin()->second;
    
    //Valid result iff number of travered edges + 1 = numVertices of whole graph
    int numEdges = 0;
    while(cur != start){
      cur = graph[cur];
      numEdges++;
      if(numEdges > 400)
        break;
    }
    if(numEdges + 1 == numVertices){
      std::cout << k_mer;
      return 0;
    }
  }
  std::cout << "No solution";

  return 0;
}
//----------------------------------------
//Read input
unordered_map<string,string> buildDirectedGraph(vector<string> &input, int k_mer_length){

  unordered_map<string,string> adj;

  for(int i = 0; i < input.size(); i++){

    if(k_mer_length > input[i].length())
      return adj;
    for(int j = 0; j + k_mer_length - 1 <= input[i].length() - 1; j++){
      string from = input[i].substr(j, k_mer_length-1);
      string to = input[i].substr(j+1, k_mer_length-1);
      adj[from] = to;
    }
  }
  return adj;
}