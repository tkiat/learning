#include <iostream>
#include <cassert>
#include <string>
#include <vector>
#include <climits>
#include <algorithm>

using std::vector;
using std::string;
using std::max;
using std::min;

//Result: Max time used: 0.01/1.00, max memory used: 9609216/536870912
long long eval(long long a, long long b, char op) {
  if (op == '*') {
    return a * b;
  } else if (op == '+') {
    return a + b;
  } else if (op == '-') {
    return a - b;
  } else {
    assert(0);
  }
}

std::pair<long long, long long> minAndMax(int i, int j, vector<vector<long long>> &m, vector<vector<long long>> &M, vector<char> &op){
  long long min = LLONG_MAX;
  long long max = LLONG_MIN;

  for(int k = i; k <= j-1; k++){

    long long a = eval(M[i][k], M[k+1][j], op[k]);
    long long b = eval(M[i][k], m[k+1][j], op[k]);
    long long c = eval(m[i][k], M[k+1][j], op[k]);
    long long d = eval(m[i][k], m[k+1][j], op[k]);
    //std::cout << a << " "<< b<< " "<<c<< " "<<d<<" "<<op[k]<<std::endl;
    if(a < min) min = a; if(a > max) max = a;
    if(b < min) min = b; if(b > max) max = b;
    if(c < min) min = c; if(c > max) max = c;
    if(d < min) min = d; if(d > max) max = d;
  }
  return std::make_pair(min, max);
}

long long Parentheses(string &exp){

//step 1: store operations and operands to variables
  size_t num_op = 0;
  num_op += std::count(exp.begin(), exp.end(), '+');
  num_op += std::count(exp.begin(), exp.end(), '-');
  num_op += std::count(exp.begin(), exp.end(), '*');
  vector<char> op(num_op);
  //n operations imply n+1 operands
  size_t n = num_op + 1;
  vector<vector<long long>> m(n), M(n);
  for(int i = 0; i < n; i++){
    m[i].resize(n);
    M[i].resize(n);
  }

  for(int i = 0; i < n; i++){
    size_t pos = exp.find_first_of("+-*", 0);
    if(pos == string::npos){
      break;
    }
    m[i][i] = stoi(exp.substr(0, pos)); 
    M[i][i] = m[i][i]; 
    if(i < n){
      op[i] = exp[pos];
    }
    exp.erase(0, pos+1);
  }
  m[n-1][n-1] = stoi(exp);  
  M[n-1][n-1] = m[n-1][n-1]; 
  //step 2: do real stuff
  for(int s = 1; s < n; s++){
    for(int i = 0; i < n-s; i++){
      int j = i + s;
      std::pair<long long, long long> temp = minAndMax(i, j, m, M, op);
      m[i][j] = temp.first;
      M[i][j] = temp.second;
    }
  }
  /*
  for(int i = 0; i < n; i++){
    for(int j = 0; j < n; j++){
      std::cout << M[i][j] << " ";
    }
    std::cout << std::endl;
  }
   for(int i = 0; i < n; i++){
    for(int j = 0; j < n; j++){
      std::cout << m[i][j] << " ";
    }
    std::cout << std::endl;
  }
  */
  return M[0][n-1];
}

long long get_maximum_value(const string &exp) {
  //write your code here
  return 0;
}

int main() {
  string s;
  std::cin >> s;
  //std::cout << get_maximum_value(s) << '\n';
  std::cout << Parentheses(s);
}
