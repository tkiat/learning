#include <iostream>
#include <string>
#include <unordered_map>

using std::string;
//Result: 31 ms	3444 KB
int main(){

    string gem[6] = { "Power", "Time", "Space", "Soul", "Reality", "Mind" };
    string gem_color[6] = {"purple", "green", "blue", "orange", "red", "yellow"};
    std::unordered_map <string, string> mymap;

    for(int i = 0; i < 6; i++){
        mymap[gem_color[i]] = gem[i]; 
    }
    int n;
    string s;
    std::cin >> n;
    while(n-- > 0){
        std::cin >> s;
        mymap.erase(s);
    }
    std::cout << mymap.size() << "\n";
    for(auto m : mymap)
        std::cout << m.second << "\n";

    return 0;
}