//-----require
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const shortid = require('shortid');
//-----dotenv
const dotenv = require('dotenv');
dotenv.config();

//-----cors
app.use(cors())

//-----body parser
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

//-----express
app.use(express.static('public'))
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html')
});
var router = express.Router();
//create new user
app.post('/api/exercise/new-user', (req, res) => {
  let username = req.body.username;

  User.findOne({username: username}, function(err, user){
    if(user){
      res.send("username already taken");
    }
    else{
      let id = shortid.generate();
      var newUser = new User({username: username, _id: id});
      newUser.save(function (err, fluffy) {
        if (err) return console.error(err);
      });
      res.json({username: username, _id: id});
    }
  });
});
//display all users
app.get('/api/exercise/users', (req, res) => {
  User.find(function (err, users) {
    if (err) return console.error(err);
    res.json(users);
  }).select('-log')
});
//display a user log
app.get('/api/exercise/log', (req, res) => {
  let qry = req.query;
  let userId = qry.userId;
  let from = new Date(qry.from);
  let to = new Date(qry.to);
  let limit = +qry.limit;
  User.findOne({_id: userId}/*, {log: {$slice: limit}}*/, function (err, user) {
    if (!user){
      res.send("unknown userId")
    }
    else{
      let newLog = (qry.from && qry.to) ? user.log.filter(elem => (elem.date >= from && elem.date <= to)) : user.log;
      newLog = isNaN(limit) ? newLog : newLog.slice(0, limit);
      res.json({_id: user._id, username: user.username, count: newLog.length, log: newLog});
    }
  })
});
//add execise
app.post('/api/exercise/add', (req, res) => {
  let userId = req.body.userId;
  let description = req.body.description;
  let duration = req.body.duration;
  let date = req.body.date ? new Date(req.body.date) : new Date();

  let exercise = {description: description, duration: duration, date: date}
  User.findOneAndUpdate({_id: userId}, {"$push": {"log": exercise}}, function(err, user){
    if(err){
      res.send(err.reason ? err.reason.message: err.message)
    }
    else if(!user){
      res.send("unknown _id");
    }
    else{
      let date_beautify = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
      res.json({username: user.username, description: description, duration: duration, _id: userId, date: date_beautify});
    }
  });
});
//middleware: Not found
app.use((req, res, next) => {
  return next({status: 404, message: 'not found'})
})
//middleware: Error Handling
app.use((err, req, res, next) => {
  let errCode, errMessage
  if (err.errors) {
    // mongoose validation error
    errCode = 400 // bad request
    const keys = Object.keys(err.errors)
    // report the first validation error
    errMessage = err.errors[keys[0]].message
  } else {
    // generic or custom error
    errCode = err.status || 500
    errMessage = err.message || 'Internal Server Error'
  }
  res.status(errCode).type('txt')
    .send(errMessage)
})
const listener = app.listen(process.env.PORT || 3000, () => {
  console.log('Your app is listening on port ' + listener.address().port)
})

//-----mongoose
//setup
const mongoose = require('mongoose');
var url_cloud = process.env.MONGOOSE_URI;
var url_localhost = 'mongodb://localhost/exercise-track';
mongoose.connect(url_cloud || url_localhost, { useNewUrlParser: true })
//create User model
var logSchema = new mongoose.Schema({
  description: {type: String, required: true},
  duration: {type: Number, required: true},
  date: {type: Date, required: true}
})
var userSchema = new mongoose.Schema({
  username: {type: String, required: true},
  _id: {type: String, 'default': shortid.generate, required: true},
  log: [logSchema]
});
var User = mongoose.model('User', userSchema);
