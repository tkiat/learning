#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <utility>

//Result: Max time used: 0.34/1.00, max memory used: 52113408/536870912
using std::string;
using std::vector;

vector<int> BuildSuffixArray(const string& text) {
  vector<int> result;

  int len = text.length();

  vector<std::pair<string,int>> vec(len);

  for(int i = 0; i < len; i++)
    vec[i] = std::make_pair(text.substr(i), i);

  std::sort(vec.begin(), vec.end());

  for(int i = 0; i < len; i++)
    result.push_back(vec[i].second);

  return result; //return index of each suffix
}

int main() {

  string text;
  std::cin >> text;
  vector<int> suffix_array = BuildSuffixArray(text);
  for (int i = 0; i < suffix_array.size(); i++)
    std::cout << suffix_array[i] << ' ';
  std::cout << std::endl;

  return 0;
}
/*
Input
AACGATAGCGGTAGA$
Output
15 14 0 1 12 6 4 2 8 13 3 7 9 10 11 5
Sorted suffixes:
15 $
14 A$
0 AACGATAGCGGTAGA$
1 ACGATAGCGGTAGA$
12 AGA$
6 AGCGGTAGA$
4 ATAGCGGTAGA$
2 CGATAGCGGTAGA$
8 CGGTAGA$
13 GA$
3 GATAGCGGTAGA$
7 GCGGTAGA$
9 GGTAGA$
10 GTAGA$
11 TAGA$
5 TAGCGGTAGA$
*/