#include <algorithm>
#include <iostream>
#include <climits>
#include <vector>

using std::vector;

//Result: Max time used: 0.23/1.00, max memory used: 9900032/536870912
struct Segment {
  int start, end;
};

bool sort_endfirstcomefirst(Segment a, Segment b){
  return a.end < b.end;
}

vector<int> optimal_points(vector<Segment> &segments) {
  vector<int> points;
  //write your code here
  sort(segments.begin(), segments.end(), sort_endfirstcomefirst);
  points.push_back(segments[0].end);

  for (size_t i = 1; i < segments.size(); i++) {
    if(segments[i].start > points.back()){
      points.push_back(segments[i].end);
    }
  }
/*
  for (size_t i = 1; i < segments.size(); ++i) {
    for(int j = 0; j< points.size(); j++){
      if(segments[i].start <= points[j] && points[j] <= segments[i].end){
        break;
      }
      else{
        if(j == points.size()-1){
          points.push_back(segments[i].end);
        }
      }
    }
  }
*/
  return points;
}

int main() {
  int n;
  std::cin >> n;
  vector<Segment> segments(n);
  for (size_t i = 0; i < segments.size(); ++i) {
    std::cin >> segments[i].start >> segments[i].end;
  }
  vector<int> points = optimal_points(segments);
  std::cout << points.size() << "\n";
  for (size_t i = 0; i < points.size(); ++i) {
    std::cout << points[i] << " ";
  }
}
