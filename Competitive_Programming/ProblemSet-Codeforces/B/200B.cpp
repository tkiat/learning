#include <iostream>

//Result: 30 ms	3400 KB
int main(){

    std::ios::sync_with_stdio(false);

    int n;
    std::cin >> n;

    double sum = 0;
    for(int i = 0; i < n; i++){
        int drink;
        std::cin >> drink;
        sum += drink;
    }
    printf("%.12lf", sum/(n*100)*100);
    //std::cout << std::fixed << std::setprecision(12) << sum/(n*100)*100;

    return 0;
}