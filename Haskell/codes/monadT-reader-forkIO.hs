import Control.Concurrent
import Control.Exception
import Control.Monad
import Control.Monad.Reader
import Data.IORef
import System.Posix.Signals

startTimer :: Int -> IO Int
startTimer sec = do
  mv <- newEmptyMVar
  installHandler sigINT (CatchOnce $ putMVar mv ()) Nothing
  t <- newIORef sec

  tid <- forkIO $ threadDelay 5000000
  killThread tid
  readIORef t

noob :: Int -> ReaderT Noob IO ()
noob sec = do
  mv <- asks mVar
  r <- asks ioRef
  liftIO $ putStrLn $ show sec
  liftIO $ threadDelay 1000000
  if sec > 0
    then (liftIO $ modifyIORef' r (subtract 1)) >> noob (sec - 1)
    else liftIO $ putMVar mv ()

data Noob = Noob {mVar :: MVar (), ioRef :: IORef Int}

main :: IO ()
main = do
  let sec = 5
  t <- newIORef sec

  mv <- newEmptyMVar
  _ <- installHandler sigINT (CatchOnce $ putMVar mv ()) Nothing

  let n = Noob {mVar = mv, ioRef = t}

  bracket (forkIO $ runReaderT (noob sec) n) killThread (\_ -> takeMVar mv)
  r <- readIORef t
  putStr $ show r
