#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stack>

using std::vector;
using std::string;
//Result: Max time used: 0.01/1.00, max memory used: 53637120/536870912
//--------------------------------------------------
vector<vector<int>> getDirectedGraph(int numVertices, int numEdges);
bool isBalancedGraph(vector<vector<int>> &adj);
vector<int> hierholzer(vector< vector<int> > adj);

int main() {

  std::ios_base::sync_with_stdio(false);
  //get input
  int V, E;
  std::cin >> V >> E;
  vector<vector<int>> graph = getDirectedGraph(V, E);
  //check if balanced
  if(!isBalancedGraph(graph)){
    std::cout << "0";
    return 0;
  }
  else 
    std::cout << "1\n";
  //find path
  vector<int> path = hierholzer(graph);
  //print result
  for(auto it = path.begin(); it != path.end() - 1; it++)
    std::cout << *it + 1 << " ";
  
  return 0;
}
//--------------------------------------------------
//Hierholzer algorihtm: find Eulerian path
vector<int> hierholzer(vector< vector<int> > adj){

  int n = adj.size();
  if (adj.size() == 0)
    return vector<int>();
  //outdegree of each node
  int outdegree[n] = {0};
  for (int i = 0; i < adj.size(); i++)
    outdegree[i] = adj[i].size();

  std::stack<int> st; //store travered nodes
  vector<int> path;
  //Hierholzer Step 1: Choose any vertex and begin traversing
  int u = 0;
  st.push(u);
  while (!st.empty()){
    //Hierholzer Step 2: keep traversing while there is still unexplored edge
    if (outdegree[u] > 0){
      st.push(u);
      int next = adj[u].back();
      adj[u].pop_back();
      outdegree[u]--; //remove explored edge
      
      u = next;
    }
    //Hierholzer Step 2: backtrack if there is no unexplored edge
    else{
      path.push_back(u);
      u = st.top();
      st.pop();
    }
  }
  //Hierholzer Step 3: reverse path
  std::reverse(path.begin(), path.end());
  return path;
}
//A graph is balanced if indegree = outdegree for each node
bool isBalancedGraph(vector<vector<int>> &adj){

  int n = adj.size();
  int indegree[n] = {0};
  int outdegree[n] = {0};

  for(int i = 0; i < n; i++){
    outdegree[i] = adj[i].size();
    for(int j = 0; j < adj[i].size(); j++)
      indegree[adj[i][j]]++;
  }
  for(int i = 0; i < n; i++)
    if(indegree[i] != outdegree[i])
      return false;
  return true;
}
//Read input
vector<vector<int>> getDirectedGraph(int numVertices, int numEdges){

  vector<vector<int>> adj(numVertices);
  for(int i = 0; i < numEdges; i++){
    int u, v;
    std::cin >> u >> v;
    adj[u-1].push_back(v-1);
  }
  return adj;
}
