#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;
typedef unsigned long long ull;

//Result: Max time used: 0.10/1.00, max memory used: 20713472/536870912
struct Data {
    string pattern, text;
};

Data read_input() {
    Data data;
    std::cin >> data.pattern >> data.text;
    return data;
}

void print_occurrences(const std::vector<int>& output) {
    for (size_t i = 0; i < output.size(); ++i)
        std::cout << output[i] << " ";
    std::cout << "\n";
}
//my code
//straightforward hash
long long hashFunc(const string& s) {   //map value to key

    static const int p = 1000000007;
    static const int x = 263;
    long long h_S = 0;
    //feed last char first
    for (int i = static_cast<int> (s.size()) - 1; i >= 0 ; i--)
        h_S = (h_S * x + s[i]) % p;
    return h_S;
}
//precompute hash to make rabinKarp faster
vector<long long> precomputeHashFunc(const string& t, const string& p) {   //map value to key

    static const int prime = 1000000007;
    static const int x = 263;

    int t_len = t.length();
    int p_len = p.length();

    vector<long long> H(t_len - p_len + 1);
    H[t_len - p_len] = hashFunc(t.substr(t_len - p_len));

    long long y = 1;
    for(int i = 1; i <= p_len; i++)
        y = (y * x) % prime;
    for(int i = t_len - p_len - 1; i >= 0; i--){
        //ex. text abcd
        //H(abc) = H(bcd shift right)
        //       = H(_bcd) + H(a) - H(___d)
        //       = x * H[i+1] + t[i] - y * t[i + p_len]
        H[i] = (x * H[i+1] + t[i] - y * t[i + p_len]) % prime;
        H[i] = (H[i] + prime) % prime; //guard against neg num
    }
    return H;
}
std::vector<int> rabinKarp(const Data& input) {

    const string& p = input.pattern, t = input.text;
    int p_len = p.length();

    vector<int> ans;
    int pHash = hashFunc(p);
    vector<long long> H = precomputeHashFunc(t, p);

    for(int i = 0; i < t.length() - p.length() + 1; i++){
        //long long tHash = hashFunc(t.substr(i, p.size())); //worked but slow
        //if(pHash != tHash)
        if(pHash != H[i])
            continue;
        if(t.substr(i, p_len) == p)
            ans.push_back(i);
    }
    return ans;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    print_occurrences(rabinKarp(read_input()));
    return 0;
}
/*
std::vector<int> get_occurrences_naive(const Data& input) {
    const string& s = input.pattern, t = input.text;
    std::vector<int> ans;
    for (size_t i = 0; i + s.size() <= t.size(); ++i)
        if (t.substr(i, s.size()) == s)
            ans.push_back(i);
    return ans;
}
*/