#include <iostream>
#include <vector>

using std::vector;
using std::cin;
using std::cout;

//Result: 0.07/1.00, max memory used: 22843392/536870912

//O(2^n) comparisons
int MaxPairwiseProduct_Naive(const std::vector<int>& numbers) {
    int max_product = 0;
    int n = numbers.size();

    for (int first = 0; first < n; ++first) {
        for (int second = first + 1; second < n; ++second) {
            max_product = std::max(max_product,
                numbers[first] * numbers[second]);
        }
    }

    return max_product;
}
//O(2n) comparisons
long long MaxPairwiseProduct_Fast(const vector<int>& numbers) {

  int index_max_1 = 0;
  int n = numbers.size();
  for (int i = 1; i < n; ++i)
    if(numbers[i] > numbers[index_max_1])
      index_max_1 = i;

  int index_max_2 = 0;
  if(index_max_1 == 0) //avoid these two maxes referring to the same index
    index_max_2 = 1;

  for (int i = 1; i < n; ++i) 
    if(i != index_max_1 && numbers[i] > numbers[index_max_2])
      index_max_2 = i;

  return (long long) numbers[index_max_1] * numbers[index_max_2];
}
/*
//O(1.5n) comparisons
long long MaxPairwiseProduct_Faster(const vector<int>& numbers) {

  int n = numbers.size();
  int index_max_1 = 0;
  int index_max_2 = 1;

  //value at index_max_1 >= value at index_max_2
  if(numbers[1] > numbers[0]){
    index_max_1 = 1;
    index_max_2 = 0;
  }
  for (int i = 2; i < n; i++) {
    if(numbers[i] > numbers[index_max_1]){
      index_max_2 = index_max_1;
      index_max_1 = i;
    }
    else if(numbers[i] < numbers[index_max_1] && numbers[i] > numbers[index_max_2]){
      index_max_2 = i;
    }
  }

  return (long long) numbers[index_max_1] * numbers[index_max_2];
}
//O(n + log(n)) comparisons
int MaxPairwiseProduct_Fastest(const vector<int>& numbers) {
  //compair each pair then after max result traverse back to compaisons of that max value
  //the max of those comparisons is second largest element
}
*/
int main() {
    int n;
    cin >> n;
    vector<int> numbers(n);
    for (int i = 0; i < n; ++i) {
        cin >> numbers[i];
    }

    long long result = MaxPairwiseProduct_Fast(numbers);
    cout << result << "\n";
    return 0;
}
