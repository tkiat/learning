-- src: https://stackoverflow.com/questions/5929377/format-list-output-in-haskell
import Data.List
import Text.PrettyPrint
import Prelude hiding ((<>))

-- a type for records
data T = T
  { make :: String,
    model :: String,
    years :: [Int]
  }
  deriving (Show)

-- test data
test =
  [ T "foo" "avenger" [1990, 1992],
    T "bar" "eagle" [1980, 1982]
  ]

-- print lists of records: a header, then each row
draw :: [T] -> Doc
draw xs =
  text "Make\t|\tModel\t|\tYear"
    $+$ vcat (map row xs)
  where
    -- print a row
    row t =
      foldl1
        (<|>)
        [ text (make t),
          text (model t),
          foldl1 (<^>) (map int (years t))
        ]

main = putStrLn (render (draw test))

-- helpers
x <|> y = x <> text "\t|\t" <> y

x <^> y = x <> text "," <+> y
