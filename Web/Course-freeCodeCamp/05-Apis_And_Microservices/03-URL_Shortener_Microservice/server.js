//-----require
const express = require('express'); var app = express();
//var mongo = require('mongodb');
const mongoose = require('mongoose');
const cors = require('cors');
const dns = require('dns');

//-----dotenv
const dotenv = require('dotenv');
dotenv.config();

//-----body-parser
const bodyParser = require('body-parser');
app.use("/", bodyParser.urlencoded({extended: false}));

//-----express

app.use(cors());
app.use('/public', express.static(process.cwd() + '/public'));

app.get('/', function(req, res){
  res.sendFile(process.cwd() + '/views/index.html');
});

app.post("/api/shorturl/new", function (req, res) {

  const url = req.body.url;
  //const re_hostName = /(.+:\/\/)?([^\/]+)\.([^\/]+)\.([^\/]+)(\/.*)*/i;
  const re_hostName = /(.+:\/\/)?([^\/]+)(\/.*)*/i;
  
  if(!re_hostName.test(url)){
    res.json({"error": "invalid URL"});
  }
  else{
    const hostName = url.match(re_hostName)[2]; //fetch host name
    dns.lookup(hostName, (err, address, family) => {
      if(err){
        res.json({"error": "invalid URL"});
      }
      else{
        //countDocuments is async
        Url.countDocuments({}, (err, count) => {
          if(err) return console.error(err);
          let shortUrl = count + 1;
          let newEntry = new Url({shortUrl: shortUrl, url: url});
          newEntry.save(function (err, data) {
            if (err) return console.error(err);
          });
          res.json({"original_url": url,"short_url": shortUrl});
        });
      }
    });
  }
});

app.get("/api/shorturl/:shortUrl", function (req, res) {
  Url.findOne({shortUrl: req.params.shortUrl}, (err, data) => {
    if(err) return console.error(err);
    data ? res.redirect(data.url) : res.send("short_url not found");
  })
});

var port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('Node.js listening ...');
});

//-----mongoose
//setup
var url_cloud = process.env.MONGOOSE_URI;
var url_localhost = 'mongodb://localhost/url_shortener';
console.log(url_cloud)
mongoose.connect(url_cloud || url_localhost, { useNewUrlParser: true })
//create Url model
var urlSchema = new mongoose.Schema({
  shortUrl: {type: Number, required: true},
  url: {type: String, required: true}
});
var Url = mongoose.model('Url', urlSchema);
