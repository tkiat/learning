#include <vector>
#include <iostream>
#include <algorithm>

using std::vector;

//Result: Max time used: 0.01/2.00, max memory used: 9928704/536870912
struct ConvertHampathToSat {
  int numVertices;
  vector<vector<int>> edges;

  ConvertHampathToSat(int n): numVertices(n), edges(n){}

  void printEquisatisfiableSatFormula() {

    int numEdges = 0;
    for(int i = 1; i <= numVertices; i++)
      numEdges += edges[i-1].size();
    int n = numVertices;
    int m = numEdges;
    //output # clauses and # variables
    int numClauses = n /*Cond. 1*/ + n*(n*n-n) /*Cond. 2*/ 
      + n /*Cond. 3*/ /*+ n*(n*n-n) Cond. 4*/ + (n-1)*(n*n-m) /*Cond. 5*/;
    int numVariables = n*n;
    std::cout << numClauses << " " << numVariables << "\n";
    //there are 5 conditions here, 
    //sometimes they are redundant but they can help SAT solver solves faster
    //sometimes # clauses is too many so I commented Cond. 4

    //assign x_ij = true if vertex i is at position j of Hamiltonian path
    //with number (i-1) * numVertices + (j-1) + 1
    //+ 1 since number zero does not work since 0 = -0

    //Cond. 1: each vertex i belongs to a path
    //add x_1j or x_2j ... or x_nj for each j
    for(int j = 1; j <= n; j++){
      for(int i = 1; i <= n; i++){
        int x_ij = (i-1) * n + (j-1) + 1;
        std::cout << x_ij << " ";
      }
      std::cout << "0\n";
    }
    //Cond. 2: vertex appears just once in a path
    //add ~x_ij or ~x_kj for all i, j, k with i != k
    for(int j = 1; j <= n; j++){
      for(int i = 1; i <= n; i++){
        for(int k = 1; k <= n; k++){
          if(i != k){
            int x_ij = (i-1)*n + (j-1) + 1;
            int x_kj = (k-1)*n + (j-1) + 1;
            std::cout << -x_ij << " " << -x_kj << " 0\n";
          }
        }
      }
    }
    //Cond. 3: Each position j in a path must be occupied
    //add x_i1 or x_i2 ... or x_in for each i
    for(int i = 1; i <= n; i++){
      for(int j = 1; j <= n; j++){
        int x_ij = (i-1) * n + (j-1) + 1;
        std::cout << x_ij << " ";
      }
      std::cout << "0\n";
    }
    /*
    //Cond. 4: No two vertices occupy the same position of a path
    //add ~x_ij or ~x_ik for all i, j, k with j != k
    for(int i = 1; i <= n; i++){
      for(int j = 1; j <= n; j++){
        for(int k = 1; k <= n; k++){
          if(j != k){
            int x_ij = (i-1) * n + (j-1) + 1;
            int x_ik = (i-1) * n + (k-1) + 1;
            std::cout << -x_ij << " " << -x_ik << " 0\n";
          }
        }
      }
    }
    */
    //Cond. 5: Nonadjacent nodes i and j cannot be adjacent in the path
    //add ~x_ki or ~x_(k+1)j for all i, j not in Graph
    for(int i = 1; i <= n; i++){
      for(int j = 1; j <= n; j++){
        if(std::find(edges[i-1].begin(), edges[i-1].end(), j-1) == edges[i-1].end()){ //if not in graph
          for(int k = 1; k <= n-1; k++){
            int x_ki = (k-1) * numVertices + (i-1) + 1;
            int x_k_plus1_j = k * numVertices + (j-1) + 1;
            std::cout << -x_ki << " " << -x_k_plus1_j << " 0\n";
          }
        }
      }
    }
  }
};

int main() {

  std::ios::sync_with_stdio(false);

  int n, m;
  std::cin >> n >> m;
  ConvertHampathToSat converter(n);
  for (int i = 0; i < m; ++i){
    int u, v;
    std::cin >> u >> v;
    //undirectional
    if(std::find(converter.edges[u-1].begin(), converter.edges[u-1].end(), v-1) == converter.edges[u-1].end()){ //if not yet added
      converter.edges[u-1].push_back(v-1);
      converter.edges[v-1].push_back(u-1);
    }
  }

  converter.printEquisatisfiableSatFormula();

  return 0;
}
