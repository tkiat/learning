#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <unordered_set>

using std::vector;
using std::string;

#define numReads 1618 //default 1618
//Result: Max time used: 0.32/3.00, max memory used: 9850880/536870912
//Assumption in real test: there is only one longest overlap from any node
//so this will pass test but not the testcases
//--------------------------------------------------
string reads_concat(vector<string> &reads);
int num_overlapped(const string &str1, const string &str2);
vector<string> getReads();

int main() {

  std::ios_base::sync_with_stdio(false);

  vector<string> reads = getReads();
  std::cout << reads_concat(reads);

  return 0;
}
//--------------------------------------------------
//Compute result
string reads_concat(vector<string> &reads){

  std::unordered_set<int> visited;
  
  int u = 0;
  string result = reads[u];

  int i = 0;
  while(i < numReads){
    int next = 0;
    int num_overlap = 0;
    for(int v = 0; v < numReads; v++){
      if(visited.find(v) != visited.end())
        continue;
      int num = num_overlapped(reads[u], reads[v]);
      if(num > num_overlap){
        num_overlap = num;
        next = v;
      }
    }
    result += reads[next].substr(num_overlap);
    visited.insert(next);
    u = next;
    i++;
  }
  //cut overlapped final read of result with its first read
  //assume that all reads have equal length (use read[0])
  int num = num_overlapped(result.substr(result.length() - reads[0].length()), result);
  return result.substr(0, result.length() - num);
}
//Compute number of overlapped chars (ex str1: AAC, str2: ACA, output = 2)
int num_overlapped(const string &str1, const string &str2){

  int max = std::min(str1.length(), str2.length());
  int k_mer = max;
  
  while(k_mer > 0){
    int num = 0;
    for(int i = 0; i < k_mer; i++){
      if(str1[str1.length() - k_mer + i] == str2[i])
        num++;
      else
        break;
    }
    if(num == k_mer)
      return k_mer;
    k_mer--;
  }
  return k_mer;
}
//Read input
vector<string> getReads(){

  vector<string> reads;
  for(int i = 0; i < numReads; i++){
    string read;
    std::cin >> read;
    reads.push_back(read);
  }
  return reads;
}
