#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stack>

using std::vector;
using std::string;
//Result: Max time used: 0.01/1.00, max memory used: 11042816/536870912
//--------------------------------------------------
vector<vector<int>> buildDirectedGraph(int k);
bool isBalancedGraph(vector<vector<int>> &adj);
vector<int> hierholzer(vector< vector<int> > adj);

int main() {

  std::ios_base::sync_with_stdio(false);
  //get input
  int k; //k-mer
  std::cin >> k;
  //build graph
  vector<vector<int>> graph = buildDirectedGraph(k);
  //no need check if balanced based on input given
//   if(!isBalancedGraph(graph))
//    return 0;
  //find path
  vector<int> path = hierholzer(graph);
  //print result
  for(auto it = path.begin(); it != path.end() - 1; it++)
    std::cout << *it % 2;
  
  return 0;
}
//--------------------------------------------------
//Hierholzer algorihtm: find Eulerian path
vector<int> hierholzer(vector< vector<int> > adj){

  int n = adj.size();
  if (adj.size() == 0)
    return vector<int>();
  //outdegree of each node
  int outdegree[n] = {0};
  for (int i = 0; i < adj.size(); i++)
    outdegree[i] = adj[i].size();

  std::stack<int> st; //store travered nodes
  vector<int> path;
  //Hierholzer Step 1: Choose any vertex and begin traversing
  int u = 0;
  st.push(u);
  while (!st.empty()){
    //Hierholzer Step 2: keep traversing while there is still unexplored edge
    if (outdegree[u] > 0){
      st.push(u);
      int next = adj[u].back();
      adj[u].pop_back();
      outdegree[u]--; //remove explored edge
      
      u = next;
    }
    //Hierholzer Step 2: backtrack if there is no unexplored edge
    else{
      path.push_back(u);
      u = st.top();
      st.pop();
    }
  }
  //Hierholzer Step 3: reverse path
  std::reverse(path.begin(), path.end());
  return path;
}
//A graph is balanced if indegree = outdegree for each node
bool isBalancedGraph(vector<vector<int>> &adj){

  int n = adj.size();
  int indegree[n] = {0};
  int outdegree[n] = {0};

  for(int i = 0; i < n; i++){
    outdegree[i] = adj[i].size();
    for(int j = 0; j < adj[i].size(); j++)
      indegree[adj[i][j]]++;
  }
  for(int i = 0; i < n; i++)
    if(indegree[i] != outdegree[i])
      return false;
  return true;
}
//Build graph
vector<vector<int>> buildDirectedGraph(int k){

  int m = pow(2, k);
  vector<vector<int>> adj(m);
  
  for(int i = 0; i < m; i++){
    adj[i >> 1].push_back(i % (m >> 1));
    //ex in lecture: TAA becomes TA -> AA
    //so for k-mer = 3 we have 8 edges and 4 vertices:
    //000 becomes 00->00
    //001:  00->01
    //010:  01->10
    //011:  01->01
    //100:  10->00
    //101:  10->01
    //110:  11->10
    //111:  11->11 
  }
  return adj;
}