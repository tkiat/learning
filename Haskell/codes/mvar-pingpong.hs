-- src: cs240h 2016
import Control.Concurrent
import Control.Exception

pingpong :: Int -> IO ()
pingpong n = do
  mvc <- newEmptyMVar -- MVar read by child
  mvp <- newEmptyMVar -- MVar read by parent
  let parent n
        | n > 0 = do
          putStr $ " " ++ show n
          putMVar mvc n
          takeMVar mvp >>= parent
        | otherwise = return ()
      child = do
        n <- takeMVar mvc
        putMVar mvp (n - 1)
        child
  tid <- forkIO child
  parent n `finally` killThread tid
  putStrLn ""

main :: IO ()
main = pingpong 10
