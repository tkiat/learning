-- src: https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/guide-to-ghc-extensions/pattern-and-guard-extensions
{-# LANGUAGE ViewPatterns #-}

eitherEndIsZero :: Int -> [Int] -> Bool
eitherEndIsZero a (head -> 0) = True
eitherEndIsZero a (last -> 0) = True
eitherEndIsZero a _ = False

main = print $ eitherEndIsZero 4 [0, 1, 8, 9]
