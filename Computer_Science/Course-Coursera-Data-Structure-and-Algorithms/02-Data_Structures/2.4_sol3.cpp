#include <algorithm>
#include <iostream>
#include <vector>
#include <climits>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

//Result: Max time used: 0.15/2.00, max memory used: 48336896/536870912
struct Node {
  int key;
  int left;
  int right;

  Node() : key(0), left(-1), right(-1) {}
  Node(int key_, int left_, int right_) : key(key_), left(left_), right(right_) {}
};


//two conditions for BST (w/ repeated node keys)
//1. any node in the left subtree must be < root
//2. any node in the right subtree must be >= root
//we can split this into 2 sub-functions but I merge into one
bool flag = true;
std::pair<long long, long long> isBST(Node root, const vector<Node> &tree){

  if(flag == false)//be idle to save time
    return std::make_pair(0, 0);
  long long min_val = root.key;
  long long min_left = LLONG_MAX;
  long long min_right = LLONG_MAX;

  long long max_val = min_val;
  long long max_left = LLONG_MIN;
  long long max_right = LLONG_MIN;

  if(root.left != -1){
    std::pair<long long, long long> temp = isBST(tree[root.left], tree);
    min_left = temp.first;
    max_left = temp.second;
  }
  
  if(root.right != -1){
    std::pair<long long, long long> temp = isBST(tree[root.right], tree);
    min_right = temp.first;
    max_right = temp.second;
  }
  //check for two conditions explained above
  //currently min_val = root key
  if(min_right < min_val || max_left >= min_val) 
    flag = false;

  if (min_left < min_val)
    min_val = min_left;
  if (min_right < min_val)
    min_val = min_right;

  if (max_left > max_val)
    max_val = max_left;
  if (max_right > max_val)
    max_val = max_right;

  return std::make_pair(min_val, max_val);
}
bool IsBinarySearchTree(const vector<Node>& tree) {

  if(tree.size() == 0)
    return true;
  std::pair<long long, long long> unused = isBST(tree[0], tree);
  if(!flag)
    return false;
  return true;
}

int main() {
  int nodes;
  cin >> nodes;
  vector<Node> tree;
  for (int i = 0; i < nodes; ++i) {
    int key, left, right;
    cin >> key >> left >> right;
    tree.push_back(Node(key, left, right));
  }
  if (IsBinarySearchTree(tree)) {
    cout << "CORRECT" << endl;
  } else {
    cout << "INCORRECT" << endl;
  }
  return 0;
}

/*old working code

long long findMax(Node root, const vector<Node> &tree){
  if(test == false)//no need to do anything anymore
    return 0;
  long long val = root.key;
  long long left = LLONG_MIN;
  long long right = LLONG_MIN;
  if(root.left != -1)
    left = findMax(tree[root.left], tree);
  if(root.right != -1)
    right = findMax(tree[root.right], tree);

  if(left >= val)
    test = false;

  if (left > val)
    val = left;
  if (right > val)
    val = right;
  return val;
}

long long findMin(Node root, const vector<Node> &tree){
  if(test == false)//no need to do anything anymore
    return 0;
  long long val = root.key;
  long long left = LLONG_MAX;
  long long right = LLONG_MAX;
  if(root.left != -1)
    left = findMin(tree[root.left], tree);
  if(root.right != -1)
    right = findMin(tree[root.right], tree);

  if(right < val)
    test = false;

  if (left < val)
    val = left;
  if (right < val)
    val = right;
  return val;
}
*/