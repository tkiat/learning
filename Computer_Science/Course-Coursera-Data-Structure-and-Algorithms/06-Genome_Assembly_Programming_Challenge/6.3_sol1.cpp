#include <iostream>
#include <vector>
#include <climits>
#include <queue>
#include <tuple>
#include <climits>

using std::vector;
//Result: Max time used: 0.01/3.00, max memory used: 9957376/1073741824
//Search online: Maximum Flows with Edge Demands
//----------------------------------------
/* This class implements a bit unusual scheme for storing edges of the graph,
 * in order to retrieve the backward edge for a given edge quickly. */
class FlowGraph {

public:

  struct Edge {
    int from, to, capacity, flow;
  };

private:
    
  vector<Edge> edges; // List of all forward and backward edges
  vector<vector<size_t> > graph; //adjacency lists, store only indices of edges in the edges list

public:

  explicit FlowGraph(size_t n): graph(n) {} //explicit forgoes implicit conv

  void add_edge(int from, int to, int capacity) {

    Edge forward_edge = {from, to, capacity, 0};
    Edge backward_edge = {to, from, 0, 0};

    graph[from].push_back(edges.size());
    edges.push_back(forward_edge);

    graph[to].push_back(edges.size());
    edges.push_back(backward_edge);

  }

  size_t size() const {
    return graph.size();
  }

  const vector<size_t>& get_ids(int from) const {
    return graph[from];
  }

  const Edge& get_edge(size_t id) const {
    return edges[id];
  }

  void add_capacity(size_t id, int capacity) {
    edges[id].capacity += capacity;
  }

  void add_flow(size_t id, int flow) {

    edges[id].flow += flow;
    edges[id ^ 1].flow -= flow;
  }
};

std::pair<int, vector<int>> bfs(FlowGraph &graph, int from, int to){

  int n = graph.size();

  vector<int> parent(n);
  std::fill(parent.begin(), parent.end(), -1);
  parent[from] = -2;//make sure src is not rediscovered

  int M[n]; //current path capacity
  M[from] = INT_MAX;

  std::queue<int> q;
  q.push(from);

  while(!q.empty()){
    //u = from, v = to
    int u = q.front();
    q.pop();
    for(auto it = graph.get_ids(u).begin(); it != graph.get_ids(u).end(); it++){

      int v = graph.get_edge(*it).to;

      if(graph.get_edge(*it).capacity - graph.get_edge(*it).flow  > 0 
        && parent[v] == -1){

        parent[v] = u;
        M[v] = std::min(M[u], graph.get_edge(*it).capacity - graph.get_edge(*it).flow);
        
        if(v != to)
          q.push(v);
        else
          return std::make_pair(M[to], parent);
      }
    }
  }
  return std::make_pair(0, parent);
}

int edmondsKarp(FlowGraph& graph, int from, int to) {

  int maxFlow = 0;
  int n = graph.size();

  while(1){
    
    int flow;
    vector<int> parent;
    std::tie(flow, parent) = bfs(graph, from, to);

    if (flow == 0)
      break;

    maxFlow += flow;

    int cur = to;

    while(cur != from){

      int prev = parent[cur];

      for(auto it = graph.get_ids(prev).begin(); it != graph.get_ids(prev).end(); it++){
        if(graph.get_edge(*it).to == cur 
          && graph.get_edge(*it).capacity - graph.get_edge(*it).flow >= flow){
          graph.add_flow(*it, flow);
          break;
        }
      }
      cur = prev;
    }
  }
  return maxFlow;
}
//Main
FlowGraph read_data();
int main() {

  std::ios_base::sync_with_stdio(false);

  int vertex_count, edge_count;
  std::cin >> vertex_count >> edge_count;
  FlowGraph graph(vertex_count + 2);
  int in[vertex_count] = {0};
  int out[vertex_count] = {0};
  int lowerBound[edge_count];
  //read input
  for(int i = 0; i < edge_count; i++){
    int u, v, lower_bound, capacity;
    std::cin >> u >> v >> lower_bound >> capacity;
    graph.add_edge(u - 1, v - 1, capacity - lower_bound);
    out[u-1] += lower_bound;
    in[v-1] += lower_bound;
    lowerBound[i] = lower_bound;
  }
  //show result
  int D = 0; //sum of all edges' demands
  int s = vertex_count + 1;
  int t = vertex_count + 2;
  for(int v = 1; v <= vertex_count; v++){
    graph.add_edge(s - 1, v - 1, in[v-1]);
    graph.add_edge(v - 1, t - 1, out[v-1]);
    D += in[v-1];
  }
  int maxFlow = edmondsKarp(graph, s-1, t-1);
  //the flow from s via residual (capacity - lower_bound) graph to t must 
  //be equal to D since s is supplying all edges with lower_bound capacity
  if(maxFlow != D)
    std::cout << "NO";
  else{
    std::cout << "YES\n";
    //add lower_bound back to each edge weight
    for(int u = 0; u < edge_count; u++)
      std::cout << graph.get_edge(2*u).flow + lowerBound[u] << "\n";
  }
  return 0;
}
