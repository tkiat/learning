-- src: modify form http://learnyouahaskell.com/for-a-few-monads-more
import Control.Monad.Writer

gcd' :: Int -> Int -> Writer [String] Int
gcd' a b
  | b == 0 = do
    tell ["Finished with " ++ show a]
    return a
  | otherwise = do
    tell [show a ++ " mod " ++ show b ++ " = " ++ show (a `mod` b)]
    gcd' b (a `mod` b)

main :: IO ()
main = do
  print $ fst $ runWriter (gcd' 8 3) -- final result
  putStrLn "----------------------------------------"
  mapM_ putStrLn $ execWriter (gcd' 8 3) -- acc
