#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

using std::vector;

//Result: Max time used: 0.02/1.50, max memory used: 9625600/536870912
vector<int> optimal_sequence_DP(int n) { //incorrect e.g. n = 11

  std::vector<int> minOps_seq;
  
  //Step 1: compute minOps_seq[i] = min # ops to get a number i starting from 1
  minOps_seq.push_back(0); 
  minOps_seq.push_back(0);
  for(int i = 2; i <= n; i++){

    //select op either (*3, *2, +1) that yields min # ops
    int minOps = INT_MAX;
    if(i % 3 == 0)
      minOps = minOps_seq[i/3] + 1;

    if(i % 2 == 0 && minOps_seq[i/2] + 1 < minOps)
      minOps = minOps_seq[i/2] + 1;
    
    if(minOps_seq[i-1] + 1 < minOps)
      minOps = minOps_seq[i-1] + 1;
    
    minOps_seq.push_back(minOps);
  }
  //Step 2: compute integer sequence using min # ops
  //to get number 1 starting from n, then reverse sequence
  std::vector<int> sequence;
  while (n >= 1) {

    sequence.push_back(n);
    //select op either (/3, /2, -1) that yields min # ops
    int temp;
    int op;
    if(n % 3 == 0){
      temp = minOps_seq[n/3];
      op = 3;
    }
    if(n % 2 == 0 && minOps_seq[n/2] < temp){
      temp = minOps_seq[n/2];
      op = 2;
    }
    if(minOps_seq[n-1] < temp)
      op = 1;

    switch(op){
      case 3: n /= 3; break;
      case 2: n /= 2; break;
      case 1: n--; break;
    }
  }
  reverse(sequence.begin(), sequence.end());
  return sequence;
}
int main() {
  int n;
  std::cin >> n;
  vector<int> sequence_DP = optimal_sequence_DP(n);

  std::cout << sequence_DP.size() - 1 << std::endl;
  for (size_t i = 0; i < sequence_DP.size(); ++i) {
    std::cout << sequence_DP[i] << " ";
  }
}
/*
vector<int> optimal_sequence(int n) { //greedy alg, incorrect e.g. n = 11
  std::vector<int> sequence;
  while (n >= 1) {
    sequence.push_back(n);
    if (n % 3 == 0) {
      n /= 3;
    } else if (n % 2 == 0) {
      n /= 2;
    } else {
      n = n - 1;
    }
  }
  reverse(sequence.begin(), sequence.end());
  return sequence;
}
*/