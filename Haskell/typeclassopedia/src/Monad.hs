{-# LANGUAGE InstanceSigs #-}

module Monad where

class Applicative m => Monad' m where
  return' :: a -> m a
  (>>@) :: m a -> (a -> m b) -> m b
  (>@) :: m a -> m b -> m b
  m >@ n = m >>@ \_ -> n

-------------------------------------------------------------------------------
-- Instances

-- ex 1

instance Monad' [] where
  return' a = [a]
  m >>@ f = concatMap f m

--   (x : _) >>@ g = g x
--   [] >>@ _ = []

myList1 :: [Int]
myList1 = [5] >>@ (\s -> [s + 1])

myList2 :: [Int]
myList2 = [] >>@ (\s -> [s + 1])

-- ex 2

instance Monad' ((->) e) where
  return' = const
  x >>@ y = \e -> y (x e) e

-- x is e -> a
-- y is a -> e -> b
-- want to get e -> b

-- ex 3: TODO
data Free f a = Var a | Node (f (Free f a)) -- where f has a Functor instance

instance Functor f => Functor (Free f) where
  fmap :: (a -> b) -> Free f a -> Free f b
  fmap g (Var a) = Var (g a)
  fmap g (Node a) = Node (fmap (fmap g) a)

instance Functor f => Applicative (Free f) where
  pure = Var

  (<*>) :: Free f (a -> b) -> Free f a -> Free f b
  --   g <*> a = ($ a) <$> g
  (Var g) <*> a = fmap g a
  (Node g) <*> (Var a) = fmap ($ a) (Node g) -- interchange law: u <*> pure y = pure ($ y) <*> u = fmap ($ y) u
  --   (Node g) <*> (Node a) = TODO

-- source: https://stackoverflow.com/a/13357359
concatFree :: Functor f => Free f (Free f a) -> Free f a
concatFree (Var x) = x
concatFree (Node y) = Node (fmap concatFree y)

instance Functor f => Monad (Free f) where
  return = Var
  (>>=) :: Free f a -> (a -> Free f b) -> Free f b
  x >>= g = concatFree (fmap g x)

-------------------------------------------------------------------------------
-- Intuition

class Applicative m => Monad'' m where
  return'' :: a -> m a
  join' :: m (m a) -> m a

  -- ex 1: Implement (>>=) in terms of fmap (or liftM) and join
  (>>?) :: m a -> (a -> m b) -> m b
  x >>? y = join' (fmap y x)

  -- ex 2: Now implement join and fmap (liftM) in terms of (>>=) and return
  joinEx2 :: m (m a) -> m a
  joinEx2 x = x >>? (\s -> s >>? return'')

  fmapEx2 :: (a -> b) -> m a -> m b
  fmapEx2 g x = x >>? (return'' . g)

-------------------------------------------------------------------------------
main :: IO ()
main = do
  putStr "myList1: " >> print myList1
  putStr "myList2: " >> print myList2
