#include <iostream>
#include <vector>
#include <algorithm>
#if defined(__unix__) || defined(__APPLE__)
#include <sys/resource.h>
#endif

using std::vector;
using std::ios_base;
using std::cin;
using std::cout;

//Result: Max time used: 0.09/1.00, max memory used: 30121984/536870912
class TreeOrders {
  int n;
  vector <int> key;
  vector <int> left;
  vector <int> right;

public:

  void read() {
    cin >> n;
    key.resize(n);
    left.resize(n);
    right.resize(n);
    for (int i = 0; i < n; i++) {
      cin >> key[i] >> left[i] >> right[i];
    }
  }
  //my code
  void inOrder(int root) {
    if(root == -1)
      return;
    inOrder(left[root]);
    std::cout << key[root] << " ";
    inOrder(right[root]);
  }
  void preOrder(int root) {  
    if(root == -1)
      return;
    std::cout << key[root] << " ";
    preOrder(left[root]);
    preOrder(right[root]);
  }
  void postOrder(int root) {  
    if(root == -1)
      return;
    postOrder(left[root]);
    postOrder(right[root]);
    std::cout << key[root] << " ";
  }
};

int main_with_large_stack_space() {

  ios_base::sync_with_stdio(0);
  TreeOrders t;
  t.read();
  int root = 0;
  
  t.inOrder(root);
  std::cout << "\n";

  t.preOrder(root);
  std::cout << "\n";
  
  t.postOrder(root);
  return 0;
}

int main (int argc, char **argv)
{
#if defined(__unix__) || defined(__APPLE__)
  // Allow larger stack space
  const rlim_t kStackSize = 16 * 1024 * 1024;   // min stack size = 16 MB
  struct rlimit rl;
  int result;

  result = getrlimit(RLIMIT_STACK, &rl);
  if (result == 0)
  {
      if (rl.rlim_cur < kStackSize)
      {
          rl.rlim_cur = kStackSize;
          result = setrlimit(RLIMIT_STACK, &rl);
          if (result != 0)
          {
              std::cerr << "setrlimit returned result = " << result << std::endl;
          }
      }
  }
#endif

  return main_with_large_stack_space();
}

