
var chai = require("chai");
var assert = chai.assert;

var chaiHttp = require('chai-http');
chai.use(chaiHttp); 

var app = require("../server");
var server;

var portNum = process.env.PORT || 3000;
describe('Timestamp API Tests', function() {

  before(function(done) {
    //asnyc func
    server = app.listen(portNum, () => {
      console.log("now listening to port " + portNum + "...")
      done();
    });
  });

  after(function(done) {
    server.close(() => {
      console.log("successfully shutting down");
      done();
    });
  });

  //specific test
  it("GET [project url]/api/timestamp/", function(done){ 
    chai.request(server)
        .get("/api/timestamp/")
        .end(function(err, res){
          let expect = new Date().getTime();
          let result = JSON.parse(res.text).unix;
          assert.equal(res.status, 200, "response status should be 200");
          assert.isBelow(expect - result, 500, "maximum delay should be 0.5sec");
          done();
        })
  });
  
  //general test
  let testUrls = [
    {url: "/api/timestamp/2015-12-25", result: '{"unix":1451001600000,"utc":"Fri, 25 Dec 2015 00:00:00 GMT"}'},
    {url: "/api/timestamp/1450137600000", result: '{"unix":1450137600000,"utc":"Tue, 15 Dec 2015 00:00:00 GMT"}'},
    {url: "/api/timestamp/a562f", result: '{"unix":null,"utc":"Invalid Date"}'}
  ]
  
  for(let testCase of testUrls){
    it("GET [project url]" + testCase.url, function(done){ 
      chai.request(server)
          .get(testCase.url)
          .end(function(err, res){
            assert.equal(res.status, 200, "response status should be 200");
            assert.equal(res.text, testCase.result);
            done();
          })
    });
  }

});


