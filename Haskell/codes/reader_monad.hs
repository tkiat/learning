-- source: https://passy.svbtle.com/dont-fear-the-reader
--         https://blog.ssanj.net/posts/2014-09-23-A-Simple-Reader-Monad-Example.html

import Control.Monad.Reader

tom :: Reader String Int
tom = do
  env <- ask
  return 5

jerry :: Reader String String
jerry = do
  env <- ask
  return (env ++ " This is Jerry.")

tomAndJerry :: Reader String String
tomAndJerry = do
  t <- tom
  j <- jerry
  return (show t ++ "\n" ++ j)

computation1 :: Reader String String
computation1 = do
  greeting <- ask
  return $ greeting ++ ", Haskell"

computation2 :: String -> Reader String String
computation2 name = do
  greeting <- ask
  return $ greeting ++ ", " ++ name

example2 :: String -> String
example2 context = runReader (greet "James" >>= end) context
  where
    greet :: String -> Reader String String
    greet name = do
      greeting <- ask
      return $ greeting ++ ", " ++ name

    end :: String -> Reader String String
    end input = do
      isHello <- asks (== "Hello")
      return $ input ++ if isHello then "!" else "."

main :: IO ()
main = do
  putStrLn $ runReader tomAndJerry "Who is this?"
  putStrLn $ runReader computation1 "Hello"
  putStrLn $ runReader (computation2 "Tom") "Hello"
  putStrLn $ example2 "Hello"

-- source: https://stackoverflow.com/questions/49495838/reader-monad-explanation-of-trivial-case
-- As part of a monad transformer it is an excellent way to pass configuration parameters through your application. Usually this is done by adding a MonadReader constraint to any function that needs access to configuration.
-- data Config = Config
--   { databaseConnection :: Connection
--   , ... other configuration stuff
--   }
--
-- getUser :: (MonadReader Config m, MonadIO m) => UserKey -> m User
-- getUser x = do
--    db <- asks databaseConnection
--    .... fetch user from database using the connection
--
-- main :: IO ()
-- main = do
--   config <- .... create the configuration
--   user <- runReaderT (getUser (UserKey 42)) config
--   print user-
