-- src: http://slides.com/fp-ctd/lecture-7#/13/0/3

import Control.Applicative (Alternative, empty, (<|>))
import Control.Monad (guard)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Maybe

emailIsValid :: String -> Bool
emailIsValid email = '@' `elem` email

askEmail :: MaybeT IO String
askEmail = do
  lift $ putStrLn "Input your email, please:"
  email <- lift getLine
  guard $ emailIsValid email
  return email

main :: IO ()
main = do
  Just email <- runMaybeT $ untilSuccess askEmail
  putStrLn $ "OK, your email is " ++ email

untilSuccess :: Alternative f => f a -> f a
untilSuccess = foldr (<|>) empty . repeat

-- -- Defined in Control.Monad.Trans.Maybe
-- instance (Functor m, Monad m) => Alternative (MaybeT m) where
--   empty = MaybeT (return Nothing)
--   x <|> y = MaybeT $ maybe (runMaybeT y) pure $ runMaybeT x
