import Test.HUnit

main :: IO ()
main = do
  results <- runTestTT $ TestList [testRecord, testRecord]
  if errors results + failures results == 0
    then exitSuccess
    else exitWith (ExitFailure 1)

testRecord :: Test
testRecord = TestLabel "Time.Record" $ TestList $ map TestCase tests
  where
    tests =
      [ assertBool "test #1" (result1 == 5),
        assertBool "test #2" (result2 == 6)
      ]

    result1 = 5
    result2 = 7
