#include <iostream>
#include <string>

//Result: 30 ms	3400 KB
int main(){

    int inp;
    std::cin >> inp;
    std::string res[2] = {"YES", "NO"};
    std::cout << ((inp > 2) ? res[inp % 2] : res[1]);

    return 0;
}