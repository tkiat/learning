#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

//Result: Max time used: 0.39/2.00, max memory used: 158937088/536870912

struct DisjointSetsElement {
	int size, parent, rank;
	
	DisjointSetsElement(int size = 0, int parent = -1, int rank = 0):
	    size(size), parent(parent), rank(rank) {}
};

struct DisjointSets {
	int size;
	int max_table_size;
	vector <DisjointSetsElement> sets;

	DisjointSets(int size): size(size), max_table_size(0), sets(size) {
		for (int i = 0; i < size; i++){
			sets[i].parent = i;
			//sets[i].rank = 0 by default
		}
	}

	int findParent(int i) {
		//find parent and compress path
		if(i != sets[i].parent)
			sets[i].parent = findParent(sets[i].parent);
		return sets[i].parent;
	}

	void Union(int dest, int src) {
		int dest_parent = findParent(dest);
		int src_parent = findParent(src);
		//merge two components using union by rank heuristic
		if(dest_parent == src_parent)
			return;
		else if(sets[dest_parent].rank > sets[src_parent].rank){
			sets[src_parent].parent = dest_parent;
			sets[dest_parent].size += sets[src_parent].size;
			max_table_size = std::max(max_table_size, sets[dest_parent].size);
		}
		else{
			sets[dest_parent].parent = src_parent;
			sets[src_parent].size += sets[dest_parent].size;
			max_table_size = std::max(max_table_size, sets[src_parent].size);
			if(sets[dest_parent].rank == sets[src_parent].rank)
				sets[src_parent].rank++;
		}
	}
};

int main() {

	int n, m;
	cin >> n >> m;

	DisjointSets tables(n);
	for (auto &table : tables.sets) {
		cin >> table.size;
		tables.max_table_size = std::max(tables.max_table_size, table.size);
	}

	for (int i = 0; i < m; i++) {
		int destination, source;
		cin >> destination >> source;
		destination--;
		source--;
		
		tables.Union(destination, source);
	        cout << tables.max_table_size << endl;
	}

	return 0;
}
