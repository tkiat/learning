#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <stack>

using std::vector;
using std::string;

//Result: Max time used: 0.10/4.00, max memory used: 26308608/536870912
//see prof's hint below
struct custom{
  int val;
  char typ;
  int pos;
  custom(int a, char b, int c){
    val = a;
    typ = b;
    pos = c;
  }
};
bool customSort(custom lhs, custom rhs) { 
  if(lhs.val == rhs.val)
    return lhs.typ < rhs.typ; //priority: 'l' to 'p' to 'r'
  return lhs.val < rhs.val; 
}
vector<int> fast_count_segments(vector<int> &starts, vector<int> &ends, vector<int> &points) {

  vector<int> cnt(points.size());
  vector<custom> hint;
  //Step 1: push
  for(int i = 0; i < starts.size(); i++)
    hint.push_back(custom(starts[i], 'l', 0));

  for(int i = 0; i < ends.size(); i++)
    hint.push_back(custom(ends[i], 'r', 0));

  for(int i = 0; i < points.size(); i++)
    hint.push_back(custom(points[i], 'p', i));
  //Step 2: sort
  std::sort(hint.begin(), hint.end(), customSort);
  //Step 3: # segments which contain each point = # 'l' - # 'r' before its pos
  int count = 0;
  for(int i = 0; i < hint.size(); i++){

    if(hint[i].typ == 'l')
      count++;
    else if(hint[i].typ == 'r')
      count--;
    else if(hint[i].typ == 'p')
      cnt[hint[i].pos] = count;
  }
  return cnt;
}
int main() {

  int n, m;
  std::cin >> n >> m;
  vector<int> starts(n), ends(n);
  for (size_t i = 0; i < starts.size(); i++) {
    std::cin >> starts[i] >> ends[i];
  }
  vector<int> points(m);
  for (size_t i = 0; i < points.size(); i++) {
    std::cin >> points[i];
  }
  vector<int> cnt = fast_count_segments(starts, ends, points);
  for (size_t i = 0; i < cnt.size(); i++) {
    std::cout << cnt[i] << ' ';
  }
}
/*
vector<int> naive_count_segments(vector<int> starts, vector<int> ends, vector<int> points) {
  vector<int> cnt(points.size());
  for (size_t i = 0; i < points.size(); i++) {
    for (size_t j = 0; j < starts.size(); j++) {
      cnt[i] += starts[j] <= points[i] && points[i] <= ends[j];
    }
  }
  return cnt;
}
*/
/*
Hint from professor Kulikov
---
Assume that we are given three points x_1 = 5,x_2 = 8,x_3 = 3x 

and two segments [a_1,b_1]=[4,10], [a_2,b_2]=[2,6]

We then create the following list:

(5,p), (8,p), (3,p), (4,l), (10,r), (2,l), (6,r)

Let's now sort it:

(2,l), (3,p), (4,l), (5,p), (6,r), (8,p), (10,r)

Now, let's scan it from left to right. 
*/
/*
Test case (additional)
---
4 6

1 3
3 6
5 9
9 9

1 2 3 4 5 6
*/