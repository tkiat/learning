#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <utility>

using std::cin;
using std::cout;
using std::endl;
using std::make_pair;
using std::pair;
using std::string;
using std::vector;

//Result: Max time used: 0.23/1.00, max memory used: 15671296/536870912
vector<int> updateClasses(vector<int> &order, int L, vector<int> &classes){

  int n = order.size();
  vector<int> newClasses;
  newClasses.resize(n);
  newClasses[order[0]] = 0;

  for(int i = 1; i < n; i++){

    int cur = order[i];
    int prev = order[i - 1];
    int mid = cur + L;
    int midPrev = (prev + L) % n;

    if(classes[cur] != classes[prev] || classes[mid] != classes[midPrev])
      newClasses[cur] = newClasses[prev] + 1;
    else
      newClasses[cur] = newClasses[prev];
  }
  return newClasses;
}
vector<int> sortDoubled(const string& text, int L, vector<int> &classes, vector<int> &order){

  int t_len = text.length();
  int count[t_len];
  for(size_t i = 0; i < t_len; i++)
    count[i] = 0;
  vector<int> newOrder;
  newOrder.resize(t_len);

  for(int i = 0; i < t_len; i++)
    count[classes[i]] = count[classes[i]] + 1;
  for(int i = 1; i < t_len; i++)
    count[i] = count[i] + count[i-1];
  for(int i = t_len - 1; i >= 0; i--){

    int start = (order[i] - L + t_len) % t_len;
    int cl = classes[start];
    count[cl]--;
    newOrder[count[cl]] = start;
  }
  return newOrder;
}
void countSort(const string& text, vector<int> &order){

  int count[256];
  for(int i = 0; i < 256; i++)
    count[i] = 0;
  
  size_t ind = 0;
  for(char c : text){
    count[c + 128]++;
  }
  for(int i = 1; i < 256; i++){
    count[i] = count[i] + count[i-1];
  }
  for(int i = text.length() - 1; i >= 0; i--){
    char c = text[i];
    count[c + 128]--;
    order[count[c + 128]] = i;
  }
}
void computeClasses(const string &text, vector<int> &order, vector<int> &classes){
  
  classes[order[0]] = 0;
  for(int i = 1; i < text.length(); i++){

    if(text[order[i]] != text[order[i - 1]])
      classes[order[i]] = classes[order[i-1]] + 1;
    else
      classes[order[i]] = classes[order[i-1]];
  }
}
// Build suffix array of the string text and
// return a vector result of the same length as the text
// such that the value result[i] is the index (0-based)
// in text where the i-th lexicographically smallest
// suffix of text starts.
vector<int> BuildSuffixArray(const string& text) {

  vector<int> result;
  vector<int> order;
  vector<int> classes;

  int t_len = text.length();

  order.resize(t_len);
  classes.resize(t_len);

  countSort(text, order);

  computeClasses(text, order, classes);

  int L = 1;
  //std::cout << t_len <<"fsdfsfsdfsdfs";
  while(L < t_len){

    order = sortDoubled(text, L, classes, order);
    classes = updateClasses(order, L, classes);
    L = 2 * L;
  }
  return order;
}

int main() {

  string text;
  cin >> text;
  vector<int> suffix_array = BuildSuffixArray(text);
  for (int i = 0; i < suffix_array.size(); ++i) {
    cout << suffix_array[i] << ' ';
  }
  cout << endl;
  return 0;
}
/*Example
Input: AACGATAGCGGTAGA$
Output: 15 14 0 1 12 6 4 2 8 13 3 7 9 10 11 5

Sorted suffixes:
15 $
14 A$
0 AACGATAGCGGTAGA$
1 ACGATAGCGGTAGA$
12 AGA$
6 AGCGGTAGA$
4 ATAGCGGTAGA$
2 CGATAGCGGTAGA$
8 CGGTAGA$
13 GA$
3 GATAGCGGTAGA$
7 GCGGTAGA$
9 GGTAGA$
10 GTAGA$
11 TAGA$
5 TAGCGGTAGA$
*/