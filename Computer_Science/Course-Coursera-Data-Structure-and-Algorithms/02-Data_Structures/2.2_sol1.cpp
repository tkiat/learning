#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::cin;
using std::cout;
using std::swap;
using std::pair;
using std::make_pair;

//Result: Max time used: 0.04/1.00, max memory used: 26271744/536870912
//my code
void swap(int *x, int *y){
    int temp = *x;
    *x = *y;
    *y = temp;
}
int leftChild(int i){
    return 2*i + 1;
}
int rightChild(int i){
    return 2*i + 2;
}
void siftDown(vector<int> &arr, int n, int i, vector< pair<int, int> > &swaps_){

    int maxIndex = i;
    int l = leftChild(i);
    if (l < n && arr[l] < arr[maxIndex]){
        maxIndex = l;
    }
    int r = rightChild(i);
    if (r < n && arr[r] < arr[maxIndex]){
        maxIndex = r;
    }
    if (i != maxIndex){
        swap(&arr[i], &arr[maxIndex]);
        swaps_.push_back(make_pair(i, maxIndex));
        siftDown(arr, n, maxIndex, swaps_);
    }
}
class HeapBuilder {
 private:
  vector<int> data_;
  vector< pair<int, int> > swaps_;

  void WriteResponse() const {
    cout << swaps_.size() << "\n";
    for (int i = 0; i < swaps_.size(); ++i) {
      cout << swaps_[i].first << " " << swaps_[i].second << "\n";
    }
  }

  void ReadData() {
    int n;
    cin >> n;
    data_.resize(n);
    for(int i = 0; i < n; ++i)
      cin >> data_[i];
  }

  void GenerateSwaps() {
    swaps_.clear();
    // The following naive implementation just sorts 
    // the given sequence using selection sort algorithm
    // and saves the resulting sequence of swaps.
    // This turns the given array into a heap, 
    // but in the worst case gives a quadratic number of swaps.
    // DO: replace by a more efficient implementation
    /*
    for (int i = 0; i < data_.size(); ++i)
        for (int j = i + 1; j < data_.size(); ++j) {
            if (data_[i] > data_[j]) {
            swap(data_[i], data_[j]);
            swaps_.push_back(make_pair(i, j));
            }
        }
    */
    //my code
    int n = data_.size();
    for(int i = n/2; i >= 0; i--){
      siftDown(data_, n, i, swaps_);
    }
  }

 public:
  void Solve() {
    ReadData();
    GenerateSwaps();
    WriteResponse();
  }
};

int main() {
  std::ios_base::sync_with_stdio(false);
  HeapBuilder heap_builder;
  heap_builder.Solve();
  return 0;
}
