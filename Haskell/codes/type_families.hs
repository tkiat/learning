{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeFamilies #-}

import Data.Functor.Identity (Identity)
import Data.Proxy (Proxy)

-- ex 1: without type family
-- source: https://github.com/freckle/guides/blob/main/haskell-best-practices.md
--
-- type Never = Proxy
-- type Always = Identity
--
-- data FooBar f
--   = FooBar
--    { a :: Int
--    , b :: Int
--    , c :: Int
--    , d :: f Int
--    }
--
--  type Foo = FooBar Never
--  type Bar = FooBar Always

-- ex 2: with type family
-- source: https://github.com/freckle/guides/blob/main/haskell-best-practices.md
--
-- data FooBarType = Foo | Bar
--
-- type family OnlyIfBar (x :: FooBarType) (a :: *) :: * where
--   OnlyIfBar Foo a = ()
--   OnlyIfBar Bar a = a
--
-- data FooBar (x :: FooBarType) = FooBar
--   { a :: Int,
--     b :: Int,
--     c :: Int,
--     d :: OnlyIfBar x Int
--   }
