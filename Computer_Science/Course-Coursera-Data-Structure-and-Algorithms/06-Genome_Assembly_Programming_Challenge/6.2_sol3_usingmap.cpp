#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stack>
#include <unordered_map>

using std::vector;
using std::string;
//Result: Max time used: 0.01/1.00, max memory used: 11038720/536870912
//--------------------------------------------------
std::unordered_multimap<int,int> getDirectedGraph(int k);
bool isBalancedGraph(std::unordered_multimap<int,int> &graph);
vector<int> hierholzer(std::unordered_multimap<int,int> graph);

int main() {

  std::ios_base::sync_with_stdio(false);
  //get input
  int k; //k-mer
  std::cin >> k;
  std::unordered_multimap<int,int> graph = getDirectedGraph(k);
  //find path
  vector<int> path = hierholzer(graph);
  //print result
  for(auto it = path.begin(); it != path.end() - 1; it++)
    std::cout << *it%2;

  return 0;
}
//--------------------------------------------------
//Hierholzer algorihtm: find Eulerian path
vector<int> hierholzer(std::unordered_multimap<int,int> graph){

  vector<int> path;
  if(graph.size() == 0)
    return vector<int>();
    
  //outdegree of each node
  std::unordered_map<int,int> outdegree;
  for(std::pair<int,int> e : graph)
    outdegree[e.first] = graph.count(e.first);

  std::stack<int> st; //store travered nodes
  
  //Hierholzer Step 1: Choose any vertex and begin traversing
  int u = graph.begin()->first;
  st.push(u);
  
  while (!st.empty()){
    //Hierholzer Step 2: keep traversing while there is still unexplored edge
    if (outdegree[u] > 0){
      st.push(u);
        //return pair iterator: first/second = lower/upper bound of the range
      auto ret = graph.equal_range(u);
      int next = ret.first->second;
      graph.erase(ret.first);
      outdegree[u]--; //remove explored edge
      u = next;
    }
    //Hierholzer Step 2: backtrack if there is no unexplored edge
    else{
      path.push_back(u);
      u = st.top();
      st.pop();
    }
  }
  //Hierholzer Step 3: reverse path
  std::reverse(path.begin(), path.end());
  
  return path;
}
//read input
std::unordered_multimap<int,int> getDirectedGraph(int k){

  std::unordered_multimap<int,int> adj;

  int m = pow(2, k);
  for(int i = 0; i < m; i++){
    adj.insert(std::make_pair(i >> 1, i % (m >> 1)));
    //ex in lecture: TAA becomes TA -> AA
    //so for k-mer = 3 we have 8 edges and 4 vertices:
    //000 becomes 00->00
    //001:  00->01
    //010:  01->10
    //011:  01->01
    //100:  10->00
    //101:  10->01
    //110:  11->10
    //111:  11->11 
  }
  return adj;
}