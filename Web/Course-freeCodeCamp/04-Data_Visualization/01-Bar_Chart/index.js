console.clear();

document.addEventListener('DOMContentLoaded', function(){
  let req = new XMLHttpRequest();
  req.open("GET", 'https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/GDP-data.json', true);
  req.send();
  req.onload = function(){
    let json = JSON.parse(req.responseText);
    var dataset = json.data;

    //d3
    d3.select("body")
    .append("h1")
    .text("The United States GDP")
    .attr("id", "title");
    
    const w = 800, h = 300, padding = 40;
    const xScale = d3.scaleTime().domain([new Date(d3.min(dataset, d => d[0])), new Date(d3.max(dataset, d => d[0]))])
                              .range([padding, w - padding]);
    const yScale = d3.scaleLinear().domain([0, d3.max(dataset, d => d[1])])
                                   .range([h - padding, padding]);
    
    const svg = d3.select("body")
                  .append("svg")
                  .attr("width", w)
                  .attr("height", h)
  
    svg.selectAll("rect")
       .data(dataset)
       .enter()
       .append("rect")
       .attr("x", (d, i) => xScale(new Date(d[0])))
       .attr("y", (d, i) => yScale(d[1]))
       .attr("width", 2)
       .attr("height", (d, i) => h - yScale(d[1]) - padding)
       .attr("class", "bar")
       .attr("data-date", (d, i) => d[0])
       .attr("data-gdp", (d, i) => d[1])

    //axis
    const xAxis = d3.axisBottom(xScale), yAxis = d3.axisLeft(yScale);
    svg.append("g")
       .attr("id", "x-axis")
       .attr("transform", "translate\(0," + (h - padding) + "\)")
       .call(xAxis)
    svg.append("g")
       .attr("id", "y-axis")
       .attr("transform", "translate\(" + padding + ", 0\)")
       .call(yAxis)
    
    //tooltip
    var tooltip = d3.select("body")
                    .append("div")
                    .attr("id", "tooltip")
                    .style("position", "absolute")
                    .style("visibility", "hidden")
    
    d3.selectAll("rect")
      .on("mouseover", function(d){
        tooltip.attr("data-date", d[0])
               .text(d[0] + ": " + d[1] + " billions")
               .style("visibility", "visible")
               .style("top", (event.pageY - 30) + "px")
               .style("left",  (event.pageX > document.body.clientWidth - 200 ? event.pageX - 200 : event.pageX) + "px")
      })
      .on("mouseout", function(){
        return tooltip.style("visibility", "hidden");
      });
  }
})

